/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: GameObjectExtention.cs
* Script Author: MinhLe 
* Created On: 7/7/2020 12:14:50 AM*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GameObjectExtention
{
    public static IList<Calculator> Clone<Calculator>(this IList<Calculator> listToClone) where Calculator : ICloneable
    {
        return listToClone.Select(item => (Calculator)item.Clone()).ToList();
    }
    public static List<Cell> GetAllCell(this GameObject gameObject)
    {
        var list = gameObject.GetComponentsInChildren<Cell>().ToList();
        return list;
    }
    public static List<GameObject> GetAllChild(this GameObject gameObject)
    {
        List<GameObject> returnList = new List<GameObject>();
        foreach (Transform child in gameObject.GetComponent<Transform>())
        {
            returnList.Add(child.gameObject);
        }
        return returnList;
    }
    internal static void Swap(this IList source, int firstIndex, int secondIndex)
    {
        var temp = source[firstIndex];
        source[firstIndex] = source[secondIndex];
        source[secondIndex] = temp;
    }
    public static void Shuffle(this IList source, System.Random random)
    {
        var length = source.Count;

        for (var currentIndex = 0; currentIndex < length; currentIndex++)
        {
            var swapIndex = random.Next(currentIndex, length);
            source.Swap(currentIndex, swapIndex);
        }
    }
}
