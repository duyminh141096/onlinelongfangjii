﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class Menu : MonoBehaviour
{

    public Animator flashScreenClose;
    public static int sceneLoad;
    public void ClickClose(int scene)
    {
        flashScreenClose.SetBool("Close", true);
        sceneLoad = scene;
        //Debug.Log("Scene : " + sceneLoad);
    }

    public void LoadScene()
    {
        //Debug.Log("Scene Load : " + sceneLoad);
        SceneManager.LoadScene(sceneLoad);
    }

    
}
