/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: ChangingBackground.cs
* Script Author: MinhLe 
* Created On: 10/19/2020 11:05:36 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangingBackground : MonoBehaviour
{
    public Image background;
    public List<Sprite> listback;

    private void Start()
    {
        background.sprite = listback[Random.Range(0, listback.Count)];
    }
}
