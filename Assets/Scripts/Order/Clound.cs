﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Clound : MonoBehaviour
{
    public Image cloundImage;

    public Sprite[] clound;

    public RectTransform rect;

    public float minX, maxX;
    public float minY, maxY;

    public float minSpeed, maxSpeed;

    public float speed;
    //public bool moveLeft;
    public int MoveLeft;
    // Start is called before the first frame update

 


    void OnEnable()
    {
        SpawnPosition();
    }



    Vector3 randomPositionY(float a)
    {
        Vector3 temp = new Vector3(a, Random.Range(minY, maxY), 0);

        return temp;
        
    }

    void SetImageClound()
    {
        speed = Random.Range(minSpeed, maxSpeed);

        int ran = Random.Range(0, clound.Length);

        cloundImage.sprite = clound[ran];

        cloundImage.SetNativeSize();

        rect.localScale = new Vector3(1, 1, 1);
    }

    public void SpawnPosition()
    {
        SetImageClound();

        MoveLeft = Random.Range(0, 2);
        if (MoveLeft == 0)
        {
            rect.anchoredPosition = randomPositionY(minX);

            //transform.position = randomPositionY(minX);

        }
        else
        {
            rect.anchoredPosition = randomPositionY(maxX);
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
    }
    // Update is called once per frame
    void Update()
    {
  
        //neu Moveleft == 0
        if (MoveLeft == 0)
        {
            if (rect.anchoredPosition.x < maxX)
            {
                Vector3 temp = transform.position;
                temp.x += Time.deltaTime * speed;
                transform.position = temp;
            }
            else
            {
                ScaleCharacter();
                rect.anchoredPosition = randomPositionY(rect.anchoredPosition.x);
                MoveLeft = 1;

            }
        }
        else
        {
            if (rect.anchoredPosition.x > minX)
            {
                Vector3 temp = transform.position;
                temp.x -= Time.deltaTime * speed;
                transform.position = temp;
            }
            else
            {
                ScaleCharacter();
                rect.anchoredPosition = randomPositionY(rect.anchoredPosition.x);
                MoveLeft = 0;
            }
        }
    }
    void ScaleCharacter()
    {
        SetImageClound();
        Vector3 temp = transform.localScale;
        temp.x *= -1;
        transform.localScale = temp;
    }

}
