/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: DefineClass.cs
* Script Author: MinhLe 
* Created On: 7/10/2020 12:43:49 AM*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public enum Kind
{
    Normal = 0, UI = 1
}

public enum CanRotHard
{
    None = 0 , //Reset
    Can = 1, 
    Cannot = 2
}

public enum ClickSiteRot
{
    None = 2,
    Left = 0, 
    Right = 1
}

public enum PlayMode
{
    SinglePlay = 0,
    MultiPlay = 1
}

[System.Serializable]
public class SetupClass
{
    public Cell cells;
    public bool used = false;
    public string objectName = "";
}
[System.Serializable]
public class ObjectSetup
{
    public BasePieces piece;
    public string objectName = "";
    public bool used = false;
    public int index = 9999;
    public bool detected = false;
}

public class SetUpbase
{
    public List<SetupClass> userSetup = new List<SetupClass>();
    public List<SetupClass> baseSetup = new List<SetupClass>();

    public List<ObjectSetup> ObjectUser = new List<ObjectSetup>();
    public List<ObjectSetup> ObjectBase = new List<ObjectSetup>();

    bool baseColor = false; //green

    public void BaseSetup()
    {
        if (baseColor)
        {
            //move first
            //Check move P => if used
            //Check user L => used => place
            //if not L index + or - my P.
            ObjectSetup objectP = ObjectBase.Find(x => x.objectName == "Phuong");
            if (!objectP.used)
            {
                //place 1,3,5,7
                objectP.index = 1;
                objectP.used = true;
                return;
            }
            ObjectSetup objectL = ObjectBase.Find(x => x.objectName == "Long");
            if (!objectL.used)
            {
                ObjectSetup objectuserL = ObjectUser.Find(x => x.objectName == "Long");
                if (objectuserL.used)
                {
                    //place opposite user L
                    objectL.used = true;
                }
                else
                {
                    //place next P
                    //index bang + or - objectP.index
                }
            }

        }
        else
        {
            //move second
            //check user Move => if used P == true and detected != true
            //Place P too => random index( not random => need courter user P) => detected = true
            //if used move L == true and detected != true
            //Place L too => opposite
            ObjectSetup objectP = ObjectUser.Find(x => x.objectName == "Phuong");
            if (objectP.used && !objectP.detected)
            {
                ObjectSetup objectbaseP = ObjectBase.Find(x => x.objectName == "Phuong");
                if (!objectbaseP.used)
                {
                    //place

                    objectP.detected = true;
                }
            }
            ObjectSetup objectL = ObjectUser.Find(x => x.objectName == "Long");
            if (objectL.used && !objectL.detected)
            {
                ObjectSetup objectbaseL = ObjectBase.Find(x => x.objectName == "Long");
                if (!objectbaseL.used)
                {
                    //place

                    objectL.detected = true;
                }
            }
        }
    }
}
[System.Serializable]
public class Calculator
{
    public BasePieces myPiece;
    public List<DataCell> redCells;
    public List<DataCell> yellowCells;
    public List<Cell> listCells;
    public List<DataCell> greenCells;
    public List<DataCell> bestCells;
    public void SetDefaultValue(BasePieces basePieces)
    {
        myPiece = basePieces;
    }
    public void ClearData()
    {
        redCells.Clear();
        yellowCells.Clear();
        listCells.Clear();
        bestCells.Clear();
        greenCells.Clear();
    }
    public Calculator Clone()
    {
        return (Calculator)this.MemberwiseClone();
    }
    private int[] GetDirRotate(int dir)
    {
        int[] dirs = new int[2];

        switch (dir)
        {
            case 1:
                dirs[0] = 4;
                dirs[1] = 2;
                break;
            case 2:
                dirs[0] = 1;
                dirs[1] = 3;
                break;
            case 3:
                dirs[0] = 2;
                dirs[1] = 4;
                break;
            case 4:
                dirs[0] = 3;
                dirs[1] = 1;
                break;
        }
        return dirs;
    }
    private int[] GetDir(int dir)
    {
        int[] dirs = new int[3];

        switch (dir)
        {
            case 1:
                dirs[0] = 4;
                dirs[1] = 2;
                dirs[2] = 1;
                break;
            case 2:
                dirs[0] = 1;
                dirs[1] = 3;
                dirs[2] = 2;
                break;
            case 3:
                dirs[0] = 2;
                dirs[1] = 4;
                dirs[2] = 3;
                break;
            case 4:
                dirs[0] = 3;
                dirs[1] = 1;
                dirs[2] = 4;
                break;
        }
        return dirs;
    }

    public List<DataPiece> Calculatev2()
    {
        if (!myPiece) return null;
        BasePieces clone = myPiece.Clone();

        Cell currentCell = clone.mCurrentCell;
        int curentState = clone.MStateRotation;
        List<DataPiece> returnData = new List<DataPiece>();
        switch (myPiece.kind)
        {
            case Kind.Normal:
                if (clone.mNamePiece != "Long" && clone.mNamePiece != "Phuong" && clone.mNamePiece != "Vuong")
                {
                    #region Rotate not L-P-V
                   
                    int[] dirs = GetDirRotate(clone.MStateRotation);
                    for (int j = 0; j < dirs.Length; j++)
                    {
                        //check this pieces contain move
                        clone.MStateRotation = dirs[j];
                        List<Cell> cell = clone.CheckPathingBot();
                        if (cell.Count > 0)
                        {
                            returnData.Add(new DataPiece(myPiece, currentCell, dirs[j], type.none, clone.mListPiece.score));
                        }
                    }
                    clone.MStateRotation = curentState;
                    clone.mCurrentCell = currentCell;
                    #endregion
                    #region Move not L-P-V
                    //get list cell
                    //check cell xem co ton tai con piece nao k 
                    //neu co add => next
                    //neu khong co gan current cell and current dir
                    List<Cell> listCellsNormal = clone.CheckPathingBot();
                    if (listCellsNormal.Count == 0) break;

                    for (int i = 0; i < listCellsNormal.Count; i++)
                    {
                        if (listCellsNormal[i].ContainPiece())
                        {
                            int score = listCellsNormal[i].mCurrentPiece.mColor == true ?  listCellsNormal[i].mCurrentPiece.mListPiece.score: -listCellsNormal[i].mCurrentPiece.mListPiece.score;
                            int[] dirss = GetDir(clone.MStateRotation);
                            for (int j = 0; j < dirss.Length; j++)
                            {
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], j, type.none, clone.mListPiece.score + score));
                            }
                        }
                        else
                        {
                            int bestscore = -9999;
                        
                            clone.mCurrentCell = listCellsNormal[i];
                            int[] dirss = GetDir(clone.MStateRotation);
                            int index = dirss[2];
                            for (int j = 0; j < dirss.Length; j++)
                            {
                                clone.MStateRotation = dirss[j];
                                List<Cell> cells = clone.CheckPathingBot();
                                if (cells.Count > 0)
                                {
                                    int score = GetScoreListCells(cells);
                                    if(bestscore <= score)
                                    {
                                        index = j;
                                        bestscore = score;
                                    }
                                }
                            }
                            if (clone.mListPiece.GetListNameCount() > 1)
                            {
                                //team
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], dirss[index], type.single, clone.mListPiece.score + bestscore));
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], dirss[index], type.team, clone.mListPiece.score + bestscore));
                            }
                            else
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], dirss[index], type.none, clone.mListPiece.score + bestscore));
                        }
                    }
                    returnData = returnData.OrderBy(x => x.score).ToList();
                    #endregion
                }
                else
                {
                    #region Rotate L-P-V
                    //Co cl ma rotate
                    #endregion
                    #region Move L-P-V
                    List<Cell> listCellsNormal = clone.CheckPathingBot();
                    if (listCellsNormal.Count == 0) break;

                    for (int i = 0; i < listCellsNormal.Count; i++)
                    {
                        if (listCellsNormal[i].ContainPiece())
                        {
                            int score = listCellsNormal[i].mCurrentPiece.mColor == true ? listCellsNormal[i].mCurrentPiece.mListPiece.score : -listCellsNormal[i].mCurrentPiece.mListPiece.score;
                            returnData.Add(new DataPiece(myPiece, listCellsNormal[i], myPiece.MStateRotation, type.none, clone.mListPiece.score + score));
                        }
                        else
                        {
                            clone.mCurrentCell = listCellsNormal[i];
                            List<Cell> cells = clone.CheckPathingBot();
                            if (cells.Count > 0)
                            {
                                int score = GetScoreListCells(cells);
                                if (clone.mListPiece.GetListNameCount() > 0)
                                {
                                    //team
                                    returnData.Add(new DataPiece(myPiece, listCellsNormal[i], myPiece.MStateRotation, type.single, clone.mListPiece.score + score));
                                    returnData.Add(new DataPiece(myPiece, listCellsNormal[i], myPiece.MStateRotation, type.team, clone.mListPiece.score + score));
                                }
                                else
                                    returnData.Add(new DataPiece(myPiece, listCellsNormal[i], myPiece.MStateRotation, type.none, clone.mListPiece.score + score));
                            }
                        }
                    }
                    returnData = returnData.OrderBy(x => x.score).ToList();
                    #endregion
                }
                #region ...
                /*
                if (myPiece.mNamePiece != "Long" && myPiece.mNamePiece != "Phuong" && myPiece.mNamePiece != "Vuong")
                {
                    Cell currentCell = myPiece.mCurrentCell;
                    int curentState = myPiece.MStateRotation;
                    int[] dirs = GetDir(clone.MStateRotation);
                    for (int j = 0; j < dirs.Length; j++)
                    {
                        //current cell
                        //rotate check move if contain piece return;
                        myPiece.MStateRotation = dirs[j];
                        List<Cell> cellPiece = clone.CheckPathingBot();
                        if (cellPiece.Count == 0 ) continue;
                        for (int i = 0; i < cellPiece.Count; i++)
                        {
                            if (cellPiece[i].ContainPiece())
                            {
                                returnData.Add(new DataPiece(myPiece, myPiece.mCurrentCell, dirs[j], type.none, cellPiece[i].mCurrentPiece.mListPiece.score));
                                break;
                            }
                        }


                      //  returnData.Add(new DataPiece(myPiece, myPiece.mCurrentCell, dirs[j], type.none)); //Rotate
                    }
                    myPiece.MStateRotation = curentState;
                    myPiece.mCurrentCell = currentCell;
                }
                    List<Cell> listCellsNormal = clone.CheckPathingBot();
                    if (listCellsNormal.Count == 0) break;

                    for (int i = 0; i < listCellsNormal.Count; i++)
                    {
                        Cell currentCell = myPiece.mCurrentCell;
                        int curentState = myPiece.MStateRotation;


                        
                        if (clone.mListPiece.GetListNameCount() > 1)
                        {
                            if (myPiece.mNamePiece != "Long" && myPiece.mNamePiece != "Phuong" && myPiece.mNamePiece != "Vuong")
                            {
                                //team
                                int[] dirsss = GetDir(clone.MStateRotation);
                                for (int j = 0; j < dirsss.Length; j++)
                                {
                                    //check contain piece



                                    returnData.Add(new DataPiece(myPiece, listCellsNormal[i], dirsss[j], type.single));
                                    returnData.Add(new DataPiece(myPiece, listCellsNormal[i], dirsss[j], type.team));
                                }
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], clone.MStateRotation, type.single));
                            }
                            else
                            {
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], myPiece.MStateRotation, type.single));
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], myPiece.MStateRotation, type.team));
                            }
                        }
                        else
                        {
                            if (myPiece.mNamePiece != "Long" && myPiece.mNamePiece != "Phuong" && myPiece.mNamePiece != "Vuong")
                            {
                                //team
                                int[] dirsss = GetDir(clone.MStateRotation);
                                for (int j = 0; j < dirsss.Length; j++)
                                {
                                    returnData.Add(new DataPiece(myPiece, listCellsNormal[i], dirsss[j], type.single));
                                }
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], clone.MStateRotation, type.single));
                            }
                            else
                            {
                                returnData.Add(new DataPiece(myPiece, listCellsNormal[i], myPiece.MStateRotation, type.single));
                            }
                }
    */
                #endregion
                break;
            case Kind.UI:
                #region piece ui
                List<Cell> listCellsUI = clone.CheckPathingBot();
                clone.kind = Kind.Normal;
                clone.mIsPlace = true;
                if (listCellsUI.Count == 0) break;
                for (int i = 0; i < listCellsUI.Count; i++)
                {
                    clone.mCurrentCell = listCellsUI[i];
                    if (myPiece.mNamePiece != "Long" && myPiece.mNamePiece != "Phuong" && myPiece.mNamePiece != "Vuong")
                    {
                        for (int j = 1; j < 5; j++)
                        {
                            clone.MStateRotation = j;
                            List<Cell> cells = clone.CheckPathingBot();
                            int score = 0;
                           if (CheckContainPiece(cells, out score))
                            {
                                returnData.Add(new DataPiece(myPiece, listCellsUI[i], j, type.none, clone.mListPiece.score + score));
                            }
                        }
                    }
                    else
                    {
                        int score = 0;
                        if (CheckContainPiece(clone.CheckPathingBot(),out score))
                        {
                            returnData.Add(new DataPiece(myPiece, listCellsUI[i], clone.MStateRotation, type.none, clone.mListPiece.score + score));
                        }
                    }
                   
                }
                //sort theo score

                returnData = returnData.OrderBy(x => x.score).ToList();
                if (returnData.Count > 20)
                    returnData.RemoveRange(20, returnData.Count - 20);
                #endregion
                break;
        }
        clone.DestroyYourself();
        if (returnData.Count > 0 || returnData != null)
            return returnData;
        return null;
    }

    private int GetScoreListCells(List<Cell> cells)
    {
        int score = 999;
        foreach (var item in cells)
        {
            if (item.ContainPiece())
            {
                int indexScore = true == item.mCurrentPiece.mColor ? item.mCurrentPiece.mListPiece.score : -item.mCurrentPiece.mListPiece.score;
                if (score > indexScore)
                {
                    score = indexScore;
                }
            }
        }
        return score == 999 ? 0 : score;
    }

    bool CheckContainPiece(List<Cell> cells,  out int score)
    {
        bool found = false;
        int inScore = 0;
        if (cells.Count == 0)
        {
            score = 0;
            return found;
        }
        foreach (var item in cells)
        {
            if (item.ContainPiece())
            {
                inScore = true == item.mCurrentPiece.mColor ?  item.mCurrentPiece.mListPiece.score : - item.mCurrentPiece.mListPiece.score;
                found = true;
                break;
            }
        }
        score = inScore;
        return found;
    }

    public void Calculate()
    {
        redCells = new List<DataCell>();
        yellowCells = new List<DataCell>();
        greenCells = new List<DataCell>();
        listCells = new List<Cell>();
        bestCells = new List<DataCell>();
        BasePieces clone = (BasePieces)myPiece.Clone();

        List<Cell> newListCell = new List<Cell>();
        newListCell = clone.CheckPathingBot();
        if (clone.kind == Kind.Normal)
        {
            //string namePiece = myPiece.mNamePiece;
            //if (namePiece == "Long" || namePiece == "Phuong" || namePiece == "Xa")
            //    listCells = clone.GetListCellLongPhuongXa(); 
            //else

            listCells = clone.CheckPathingBot(true);
        }
        if (newListCell.Count > 0)
        {
            for (int i = 0; i < newListCell.Count; i++)
            {
                if (newListCell[i].mCurrentPiece != null)//O DAY HIEN TAI CO CON PIECE
                {
                    CheckFriendOrEnemy(newListCell[i], clone);
                }
                else // O HIEN TAI DELL CO' CON CO` NAO
                {
                    CheckCellNullPiece(newListCell[i], clone);
                }
            }
        }
        int score = myPiece.mListPiece.score;
        // get best greenCells 
        for (int i = 0; i < greenCells.Count; i++)
        {
            int newScore = greenCells[i].score - score;
            DataCell dataCell = new DataCell(greenCells[i].cell, newScore, greenCells[i].dir, myPiece);
            bestCells.Add(dataCell);
        }

        redCells = redCells.OrderByDescending(x => x.score).ToList();
        yellowCells = yellowCells.OrderByDescending(x => x.score).ToList();
        greenCells = greenCells.OrderByDescending(x => x.score).ToList();
        bestCells = bestCells.OrderByDescending(x => x.score).ToList();
    }
    void CheckFriendOrEnemy(Cell item, BasePieces basePieces)
    {
        if (item.mCurrentPiece.mColor == myPiece.mColor) //FRIEND
        {
            DataCell dataCell = myPiece.CheckPathingAtCell(item);
            if (dataCell.dir == 0)
            {
                //red list
                redCells.Add(new DataCell(item, 0, 0, myPiece));
            }
            else
            {
                yellowCells.Add(new DataCell(item, dataCell.score, dataCell.dir, myPiece));
                //yellow list
            }
        }
        else//ENEMY
        {
            greenCells.Add(new DataCell(item, item.mCurrentPiece.mListPiece.score, myPiece.MStateRotation, myPiece));
        }
    }

    void CheckCellNullPiece(Cell item, BasePieces basePieces)
    {
        DataCell dataCell = basePieces.CheckPathingAtCell(item);
        if (dataCell.dir == 0)
        {
            //red list
            redCells.Add(new DataCell(item, 0, 0, myPiece));
        }
        else
        {
            yellowCells.Add(new DataCell(item, dataCell.score, dataCell.dir, myPiece));
            //yellow list
        }
    }

}
public enum type
{
    none,
    single,
    team
}

[System.Serializable]
public class DataPiece
{
    public BasePieces targetPiece;
    public string namePiece;
    public Cell targetCell;
    public int targetDir;
    public type type;
    public int score = -9999;
    public mode mode;
    public DataPiece(BasePieces pieces, Cell cell, int dir, type type, int score = -9999)
    {
        targetPiece = pieces;
        namePiece = pieces.mNamePiece;
        targetCell = cell;
        targetDir = dir;
        this.type = type;
        this.score = score;
    }
    public DataPiece()
    {

    }
    public void SetValue(BasePieces pieces, Cell cell, int dir, type type)
    {
        targetPiece = pieces;
        namePiece = pieces.mNamePiece;
        targetCell = cell;
        targetDir = dir;
        this.type = type;
    }
}
[System.Serializable]
public class DataCell
{
    public Cell cell;
    public int score;
    public int dir;
    public BasePieces piece;
    public bool canUse = true;
    public void Clear()
    {
        cell = null;
        score = 0;
    }
    public DataCell(Cell cell, int score, int dir, BasePieces piece)
    {
        this.cell = cell;
        this.score = score;
        this.dir = dir;
        this.piece = piece;
    }
}
public enum mode
{
    MoveSingle,
    MoveSingleinTeam,
    MoveTeam,
    MoveSingleandKill,//
    MoveTeamandKill,//
    RotationPiece,
   // RotationAllPieceLeft,
   // RotationAllPieceRight,
    TeamUp,//
    MoveFromStoreToCell,
    TeamUpFormStoreToCell,//
    Setupincell,
    SetupTeamUp//
}
[System.Serializable]
public class DataNeeded
{
    public BasePieces piece;
    public Cell cell;
    public mode mode;
    public int dir;

    public class Builder
    {
        public BasePieces piece;
        public Cell cell;
        public mode mode;
        public int dir;
        public Builder addPiece(BasePieces piece)
        {
            this.piece = piece;
            return this;
        }
        public Builder addCell(Cell cell)
        {
            this.cell = cell;
            return this;
        }
        public Builder addMode(mode mode)
        {
            this.mode = mode;
            return this;
        }
        public Builder addDir(int dir)
        {
            this.dir = dir;
            return this;
        }
        public DataNeeded build()
        {
            DataNeeded data = new DataNeeded();
            data.piece = piece;
            data.cell = cell;
            data.dir = dir;
            data.mode = mode;
            return data;
        }
    }
}

[System.Serializable]
public class ReturnValueCell
{
    public int dir;//return 0 mean not 
}
