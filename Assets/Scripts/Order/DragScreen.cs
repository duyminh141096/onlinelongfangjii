﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class DragScreen : MonoBehaviour, IDragHandler, IEndDragHandler
{

    private Vector3 panelLocation;

    public float easing;

    public Vector3 minPostion, maxPositon;

    public RectTransform Bound;

    public Camera cam;

    public float offset = 684;
    private void Start()
    {
        Application.targetFrameRate = 60;

        QualitySettings.vSyncCount = 0;

        panelLocation = GetComponent<RectTransform>().anchoredPosition3D;

        minPostion = GetComponent<RectTransform>().anchoredPosition3D;

        //Debug.Log("Min Position " + minPostion);

       
       // SetFOVCam();
       
        maxPositon = minPostion - new Vector3(offset, 0,0);
    }


    void SetFOVCam()
    {
        float ratio = cam.aspect;
        if (ratio >= 0.75) // 3:4
        {
            offset = 0;
        }
        else
        if (ratio >= 0.56) // 9:16
        {
            offset = 0;
        }
        else
        if (ratio >= 1.76) // 1280x720
        {
            offset = 0;
        }
    }

    //public Vector3 MaxPostion()
    //{

    //    int i = -1;
    //    foreach (Transform child in transform)
    //    {
    //        i++;
    //    }
    //    float distance = i <= 0 ? -Screen.width : i * -Screen.width;

    //    distance += minPostion.x;

    //    Vector3 maxPos = new Vector3(distance, minPostion.y, minPostion.z);

    //    return maxPos;
    //}

    //lay duoc mixPosion va maxPostion
    //minposition bang tranform.postion
    //maxPOsition = 


    public void OnDrag(PointerEventData eventData)
    {
        float difference = eventData.pressPosition.x - eventData.position.x;

        transform.position = panelLocation - new Vector3(difference, 0f, 0f);
    }


    public void OnEndDrag(PointerEventData eventData)
    {
        //phai >> trai : duong
        //trai >> phai : am

        float percentage = eventData.pressPosition.x - eventData.position.x;

        if(transform.position.x > minPostion.x)
        {
            StartCoroutine(SmoothMove(transform.position, minPostion, easing));
        }
        if(transform.position.x < maxPositon.x)
        {
            StartCoroutine(SmoothMove(transform.position, maxPositon, easing));
        }

       
        //if (Mathf.Abs(percentage) >= minPercentPressHold)
        //{
        //    Vector3 newLocation = panelLocation;

        //    if (percentage > 0)// di chuyen sang trai
        //    {
        //        newLocation += new Vector3(-Screen.width, 0, 0);
        //    }
        //    else //di chuyen sang phai
        //    {
        //        newLocation += new Vector3(Screen.width, 0, 0);
        //    }
        //    if (newLocation.x > minPostion.x)
        //    {
        //        StartCoroutine(SmoothMove(transform.position, minPostion, easing));

        //        panelLocation = minPostion;
        //    }
        //    else if (newLocation.x < maxPositon.x)
        //    {
        //        StartCoroutine(SmoothMove(transform.position, maxPositon, easing));

        //        panelLocation = maxPositon;
        //    }
        //    else
        //    {
        //        StartCoroutine(SmoothMove(transform.position, newLocation, easing));

        //        panelLocation = newLocation;

        //    }
        //}
        //else
        //{
        //    StartCoroutine(SmoothMove(transform.position, panelLocation, easing));
        //}

    }

    IEnumerator SmoothMove(Vector3 startPos, Vector3 endPos, float seconds)
    {
        float t = 0;

        while(t < 1f)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

}