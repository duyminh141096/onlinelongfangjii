/*Project Name : Long Ki
* Script Name: ScorePiece.cs
* Script Author: MinhLe 
* Created On: 4/22/2020 10:06:20 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InfoScorePiece {
    public string name;

    public int score;
}


public class ScorePiece : MonoBehaviour
{
    public List<InfoScorePiece> Score;

    public int GetScorePiece(string piece)
    {
        for (int i = 0; i < Score.Count; i++)
        {
            if(piece == Score[i].name)
            {
                return Score[i].score;
            }
        }
        return 0;
    }


    public static ScorePiece instance;

    private void Awake()
    {
        instance = this;
    }
}
