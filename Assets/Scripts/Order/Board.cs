﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public enum CellState
{
    None,
    Friendly,
    Enemy,
    Free,
    OutofBounds,

}

public class Board : MonoBehaviour
{
    public static Board instance;

    public GameObject CellPrefab;

    public Cell[,] AllCells = new Cell[9, 9];

    public Transform mWhiteBoardParent;
    public Transform mBlackBoardParent;

    int mainCellID = 0;

    //int whiteCellID = 0;

    //int blackCellID = 0;

    public List<Text> textBlack, textWhite;
    public StorePieces storePieces;
    /*
    public void UpdateCountList(bool Orange)
    {
        Debug.Log("Green team = : " + Orange);
        kiem tra ten con piece nao trung khop voi list piece
        if (Orange)
        {
            for (int i = 0; i < mWhiteBoardList.Count; i++)
            {
                if (mWhiteBoardList[i].mCurrentPiece != null)
                {
                    Debug.Log("Name : " + mWhiteBoardList[i].mCurrentPiece.mNamePiece);
                    Debug.Log("Text : " + storePieces.GetCountStorePiece(mWhiteBoardList[i].mCurrentPiece.mNamePiece, Orange));
                    textWhite[i].text = storePieces.GetCountStorePiece(mWhiteBoardList[i].mCurrentPiece.mNamePiece, Orange);
                }
                else
                {
                    textWhite[i].text = "";
                }
            }
        }
        else
        {
            for (int i = 0; i < mBlackBoardList.Count; i++)
            {
                if (mBlackBoardList[i].mCurrentPiece != null)
                {

                    textBlack[i].text = storePieces.GetCountStorePiece(mBlackBoardList[i].mCurrentPiece.mNamePiece, Orange);
                }
                else
                {
                    textBlack[i].text = "";
                }
            }
        }

    }
    */

    private void Awake()
    {
        instance = this;

    }

    public Transform whiteBoard;
    public Transform blackBoard;

    //Create Board
    public void Create()
    {
        //create Board
        for (int y = 0; y < 9; y++)
        {
            for (int x = 0; x < 9; x++)
            {
                GameObject newCell = Instantiate(CellPrefab, transform);
                if (SetupLongPhuong.Instance.typeSetup == TypeSetUp.beginnerRule)
                {
                    if ((x == 7 || x == 1))
                    {
                        if (y == 1)
                            SetupLongPhuong.Instance.AddCell(newCell.GetComponent<Cell>(), true);
                        if (y == 7)
                            SetupLongPhuong.Instance.AddCell(newCell.GetComponent<Cell>(), false);
                    }
                }
                else
                {
                    if (y == 1)
                        SetupLongPhuong.Instance.AddCell(newCell.GetComponent<Cell>(), true);
                    if (y == 7)
                        SetupLongPhuong.Instance.AddCell(newCell.GetComponent<Cell>(), false);
                }
                newCell.name = x.ToString() + ":" + y.ToString();
                mainCellID++;

                //position
                RectTransform rectTransform = newCell.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector2((x * 90) + 45, (y * 90) + 45);

                //setup
                AllCells[x, y] = newCell.GetComponent<Cell>();
                AllCells[x, y].Setup(new Vector2Int(x, y), this, mainCellID);

            }
        }


    }
    /*
    public void CreateBlackBoard(int xCloumn, int yColumn, GameObject mBoard)
    {
        for (int y = 0; y < yColumn; y++)
        {
            for (int x = 0; x < xCloumn; x++)
            {
                GameObject newBackCell = Instantiate(mBlackCell, mBoard.transform);
                newBackCell.name = x.ToString() + ":" + y.ToString();

                //set postion
                RectTransform rectTransform = newBackCell.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector2((x * 90 * -1) - 45, (y * 90) + 45);
                blackCellID = 100 + ++blackCellID;
                //setup
                BlackCell[x, y] = newBackCell.GetComponent<Cell>();
                BlackCell[x, y].Setup(new Vector2Int(x, y), this, blackCellID);
                mBlackBoardList.Add(newBackCell.GetComponent<Cell>());

            }
        }

    }

    public void CreateWhiteBoard(int xCloumn, int yColumn, GameObject mBoard)
    {
        for (int y = 0; y < yColumn; y++)
        {
            for (int x = 0; x < xCloumn; x++)
            {
                GameObject newWhiteCell = Instantiate(mWhiteCell, mBoard.transform);
                newWhiteCell.name = x.ToString() + ":" + y.ToString();
                whiteCellID = 200 + ++whiteCellID;
                //set postion
                RectTransform rectTransform = newWhiteCell.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector2((x * 90) + 45, (y * 90 * -1) - 45);
                //setup
                WhiteCell[x, y] = newWhiteCell.GetComponent<Cell>();
                WhiteCell[x, y].Setup(new Vector2Int(x, y), this, whiteCellID);
                mWhiteBoardList.Add(newWhiteCell.GetComponent<Cell>());
            }
        }

    }
    */
    public CellState ValidateCell(int targetX, int targetY, BasePieces currentPiece)
    {
        if (targetX < 0 || targetX > 8)//Out of Bounds
        {
            return CellState.OutofBounds;
        }
        if (targetY < 0 || targetY > 8)
            return CellState.OutofBounds;

        //get cell 
        Cell targetCell = AllCells[targetX, targetY];

        if (targetCell.mCurrentPiece != null)
        {
            if (currentPiece.mColor == targetCell.mCurrentPiece.mColor)
            {
                //friendly
                if (targetCell.mCurrentPiece.mNamePiece == "Vuong")
                    return CellState.OutofBounds;
                else
                    return CellState.Friendly;
            }
            if (currentPiece.mColor != targetCell.mCurrentPiece.mColor)
            {
                return CellState.Enemy;
            }
        }
        return CellState.Free;

    }
}
