/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: EventButtonPlayMode.cs
* Script Author: MinhLe 
* Created On: 7/20/2020 9:20:38 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class EventButtonPlayMode : MonoBehaviour
{
    public Button singlePlayBtn;
    public Button multiPlayBtn;

    public Button setUpFree;
    public Button setUpFixed;
    public Button setupHard;
    public Button historyMatch;
    public GameObject panelChoose;

    private void Awake()
    {
        singlePlayBtn.onClick.AddListener(() => SetPlayMode(1,2));
        multiPlayBtn.onClick.AddListener(() => SetPlayMode(0,2));
        setUpFree.onClick.AddListener(() => SetTypeSetUp(1));
        setUpFixed.onClick.AddListener(() => SetTypeSetUp(0));
        setupHard.onClick.AddListener(() => SetTypeSetUp(2));
        historyMatch.onClick.AddListener(() => { sceneLoad = 3; LoadScene(); });
    }
    int sceneLoad;
    public void SetPlayMode(int playMode, int loadScene)
    {
        PlayerPrefs.SetInt("PlayMode", playMode);
        sceneLoad = loadScene;
        panelChoose.SetActive(true);
    }
    public void SetTypeSetUp(int type)
    {
        PlayerPrefs.SetInt("TypeSetUp", type);
        LoadScene();
    }
    public void LoadScene()
    {
        //Debug.Log("Scene Load : " + sceneLoad);
        SceneManager.LoadScene(sceneLoad);
    }

}
