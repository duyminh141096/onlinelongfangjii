﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUndo 
{
    public Stack<ICommand> undoCommand = new Stack<ICommand>();
    /// <summary>
    /// Lam sao de undo 2 phat bang 1 bien bool
    /// </summary>
    public void Undo()
    {
        if(undoCommand.Count > 0)
        {
            //pop
            if (GameController.playMode == PlayMode.MultiPlay)
            {
                ICommand command = undoCommand.Pop();
                command.Undo();
                return;
            }
            if(GameController.playMode == PlayMode.SinglePlay)
            {
                ICommand command1 = undoCommand.Pop();
                command1.Undo();
                if (undoCommand.Count == 0)  return; 
                ICommand command = undoCommand.Pop();
                command.Undo();
            }
            //Debug.Log("_____Undo_________");
        }
    }

    public void Clear()
    {
        undoCommand.Clear();
    }

    public void InsertCommand(ICommand command)
    {
        if(undoCommand.Count >= 0)
        {
            undoCommand.Push(command);
        }
    }
}
