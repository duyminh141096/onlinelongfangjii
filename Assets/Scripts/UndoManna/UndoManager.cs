﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoManager : MonoBehaviour
{
    public BaseUndo undo = new BaseUndo();

    public static UndoManager instance;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Undo();
        }
    }

    public void Undo()
    {
        undo.Undo();

        GameController.instance.CreateRecord.RemoveRecord();
        //GameController.instance.TurnOffButtonRotate(false);

        //GameController.instance.mCurentBasePiece.ClearCells();

        //GameController.instance.mTargetCell = null;

        //GameController.instance.ClearListHightLightCell();
    }
    public void InsertCommand(ICommand command)
    {
        Debug.Log("insert command");
        undo.InsertCommand(command);
    }
}
