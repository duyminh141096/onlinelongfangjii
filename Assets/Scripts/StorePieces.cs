/*Project Name : Long Ki
* Script Name: StorePieces.cs
* Script Author: MinhLe 
* Created On: 5/6/2020 9:02:19 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class Store
{
    public string pieceName;
    public Transform pos;
    public bool teamColor;
    public int count;
    public Text textCount;
    public Store() { }
    public Store(string name , Transform pos,int count , bool teamcolor , Text text)
    {
        pieceName = name;
        this.pos = pos;
        this.count = count;
        teamColor = teamcolor;
        textCount = text;
    }
}

public class StorePieces : MonoBehaviour
{
    #region singleton
    public static StorePieces Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    public List<Store> storesPiece;
    public List<GameObject> selectedCell;

    public void ShowSelectCell(Transform pos ,bool enable)
    {
        for (int i = 0; i < storesPiece.Count; i++)
        {
            if(storesPiece[i].pos == pos)
            {
                Debug.Log("Dieu kien dung : " + enable);
                selectedCell[i].SetActive(enable);
            }else selectedCell[i].SetActive(false);
        }
    }

    public void UpdateText()
    {
        foreach (var item in storesPiece)
        {
            string a = item.count > 0 ? "" + item.count : "" ;
            item.textCount.text = a;
        }
    }

    public void Add(string piece, bool teamColor, bool show = true)
    {
        Store store = storesPiece.Find(x => x.pieceName == piece && x.teamColor == teamColor);
        if (store != null)
        {
            store.count++;
            if(show) store.textCount.text = store.count.ToString();
        }
    }

    public void Remove(string name , bool teamColor, bool show = true)
    {
        Store store = storesPiece.Find(x => x.pieceName == name && x.teamColor == teamColor);
        if(store != null)
        {
            store.count--;
            if (show)
            {
                string a = store.count > 0 ? "" + store.count : "";
                store.textCount.text = a;
            }
        }
    }

    public Store GetInfo(string name,bool teamColor)
    {
        Store value = storesPiece.Find(x => x.pieceName == name && x.teamColor == teamColor);
        if(value != null)
        return value;
        return null;
    }

}

