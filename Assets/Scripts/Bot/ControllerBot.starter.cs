/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: ControllerBot.starter.cs
* Script Author: MinhLe 
* Created On: 7/17/2020 10:10:36 AM*/
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class ControllerBot
{
    /* real action bot */
    public IEnumerator MoveSingleinteamBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        if (targetCell.mCurrentPiece == null)
        {
            MoveSingleinTeam command = new MoveSingleinTeam();
            GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

            PiecesManager.rotation = false;

            if (newPiece.GetComponent<BasePieces>() != null)
            {
                newPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                //newPiece.GetComponent<BasePieces>().Move(targetCell, false, false);
                MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, false);
                yield return new WaitForSeconds(0.3f);
                GameController.instance.SelectedPiece = newPiece.GetComponent<BasePieces>();
                //newPiece.GetComponent<BasePieces>().rotationCS.StartCoroutine("ActiveButton");
                command.GetMovePiece(newPiece.GetComponent<BasePieces>());
            }
            basePieces.mListPiece.RemovePieceList();

            if (basePieces.mListPiece.GetListNameCount() > 1)
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
                basePieces.mListPiece.RemovePieceList();
                newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(basePieces.mListPiece.GetPieceNamesList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            else
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }

            UndoManager.instance.InsertCommand(command);
            //GameController.instance.mCurentBasePiece.ClearCells();
            //GameController.instance.SelectedCellColor(false);
            //GameController.instance.mTargetCell = null;
            //GameController.instance.ClearListHightLightCell();
            GameController.instance.SetState(State.None);

            //check Rotation
            if (basePieces.checkNamePiece(basePieces)) PiecesManager.SwitchSides(!basePieces.mColor);
            else
            {
                if (basePieces.MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, basePieces);
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                PiecesManager.SwitchSides(!basePieces.mColor);
                //else
                //{
                //    DataCell dataCell = newPiece.GetComponent<BasePieces>().CheckPathingAtCell(basePieces.mCurrentCell);
                //    if (newPiece.GetComponent<BasePieces>().MStateRotation != dataCell.dir)
                //    {
                //        //rotation
                //        CheckRotation(dataCell.dir, newPiece.GetComponent<BasePieces>());
                //        PiecesManager.SwitchSides(!basePieces.mColor);
                //    }
                //    PiecesManager.SwitchSides(!basePieces.mColor);
                //}
            }
            Destroy(basePieces.gameObject, 0.4f);
        }
    }
    public IEnumerator MoveTeamBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        if (targetCell.mCurrentPiece == null)
        {
            // GameController.instance.Shield.SetActive(true);
            MoveTeam command = new MoveTeam(basePieces, targetCell);
            UndoManager.instance.InsertCommand(command);
            PiecesManager.rotation = false;

            basePieces.mCurrentCell.mCurrentPiece = null;
            basePieces.mCurrentCell = targetCell;
            basePieces.mCurrentCell.mCurrentPiece = basePieces;

            basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
            yield return new WaitForSeconds(0.3f);
            //GameController.instance.ShieldGame();

            basePieces.mTargetCell = null;
            basePieces.mIsPlace = true;
            basePieces.ClearCells();

            //GameController.instance.mCurentBasePiece.ClearCells();

            //GameController.instance.mTargetCell = null;
            //GameController.instance.ClearListHightLightCell();
            //GameController.instance.SelectedCellColor(false);
            GameController.instance.SetState(State.None);

            if (basePieces.checkNamePiece(basePieces)) PiecesManager.SwitchSides(!basePieces.mColor);
            else
            {
                if (basePieces.MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, basePieces);
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                PiecesManager.SwitchSides(!basePieces.mColor);
                //else
                //{
                //    DataCell dataCell = basePieces.CheckPathingAtCell(basePieces.mCurrentCell);
                //    if (basePieces.MStateRotation != dataCell.dir)
                //    {
                //        //rotation
                //        CheckRotation(dataCell.dir, basePieces);
                //        PiecesManager.SwitchSides(!basePieces.mColor);
                //    }
                //    PiecesManager.SwitchSides(!basePieces.mColor);
                //}
            }
        }
    }
    public IEnumerator MoveOnlyAndKillBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        if (targetCell.mCurrentPiece != null)
        {
            BasePieces targetPiece = targetCell.mCurrentPiece;
            MoveSingleandKill command = new MoveSingleandKill();
            GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
            PiecesManager.rotation = false;

            if (newPiece.GetComponent<BasePieces>() != null)
            {
                newPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, true);
                yield return new WaitForSeconds(0.3f);
                //newPiece.GetComponent<BasePieces>().Move(targetCell, false, true);
                GameController.instance.SelectedPiece = newPiece.GetComponent<BasePieces>();
                //newPiece.GetComponent<BasePieces>().rotationCS.StartCoroutine("ActiveButton");
                command.GetMovePiece(newPiece.GetComponent<BasePieces>());
            }

            basePieces.mListPiece.RemovePieceList();

            if (basePieces.mListPiece.GetListNameCount() > 1)
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
                basePieces.mListPiece.RemovePieceList();
                newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(basePieces.mListPiece.GetPieceNamesList());
                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            else
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            // GameController.instance.mCurentBasePiece.ClearCells();
            //GameController.instance.ClearListHightLightCell();
            GameController.instance.SetState(State.None);
            //GameController.instance.SelectedCellColor(false);
            command.GetKilledPiece(GameController.instance.mTargetBasePiece);
            targetPiece.Kill(targetPiece.mListPiece.GetPieceNamesList(), command);
            UndoManager.instance.InsertCommand(command);
            GameController.instance.mTargetCell = null;
            if (basePieces.mNamePiece == "Vuong" || basePieces.mNamePiece == "Long" || basePieces.mNamePiece == "Phuong") PiecesManager.SwitchSides(!basePieces.mColor);
            else
            {
                if (newPiece.GetComponent<BasePieces>().MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, newPiece.GetComponent<BasePieces>());
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                else
                //{
                //    DataCell dataCell = newPiece.GetComponent<BasePieces>().CheckPathingAtCell(basePieces.mCurrentCell);
                //    if (newPiece.GetComponent<BasePieces>().MStateRotation != dataCell.dir)
                //    {
                //        //rotation
                //        CheckRotation(dataCell.dir, newPiece.GetComponent<BasePieces>());
                //        PiecesManager.SwitchSides(!basePieces.mColor);
                //    }
                //    PiecesManager.SwitchSides(!basePieces.mColor);
                //}
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
            Destroy(basePieces.gameObject, 0.4f);
        }
    }
    public IEnumerator MoveTeamAndKillBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        if (targetCell.mCurrentPiece != null)
        {
            BasePieces targetPiece = targetCell.mCurrentPiece;
            //GameController.instance.Shield.SetActive(true);
            MoveTeamandKill command = new MoveTeamandKill(basePieces, targetCell);
            command.GetKilledPiece(GameController.instance.mTargetBasePiece);
            targetPiece.Kill(targetPiece.mListPiece.GetPieceNamesList(), command);
            UndoManager.instance.InsertCommand(command);

            PiecesManager.rotation = false;
            basePieces.mCurrentCell.mCurrentPiece = null;
            basePieces.mCurrentCell = targetCell;
            basePieces.mCurrentCell.mCurrentPiece = basePieces;
            basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
            yield return new WaitForSeconds(0.3f);
            //GameController.instance.ShieldGame();
            basePieces.mTargetCell = null;
            basePieces.ClearCells();
            // GameController.instance.SelectedCellColor(false);
            //GameController.instance.mTargetCell = null;
            // GameController.instance.ClearListHightLightCell();
            GameController.instance.SetState(State.None);
            if (basePieces.mNamePiece == "Vuong" || basePieces.mNamePiece == "Long" || basePieces.mNamePiece == "Phuong") PiecesManager.SwitchSides(!basePieces.mColor);
            else
            {
                if (basePieces.MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, basePieces);
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                //else
                //{
                //    DataCell dataCell = basePieces.GetComponent<BasePieces>().CheckPathingAtCell(basePieces.mCurrentCell);
                //    if (basePieces.GetComponent<BasePieces>().MStateRotation != dataCell.dir)
                //    {
                //        //rotation
                //        CheckRotation(dataCell.dir, basePieces.GetComponent<BasePieces>());
                //        PiecesManager.SwitchSides(!basePieces.mColor);
                //    }
                //    PiecesManager.SwitchSides(!basePieces.mColor);
                //}
                PiecesManager.SwitchSides(!basePieces.mColor);
            }

        }
    }
    public IEnumerator TeamUpBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        TeamUp commandTeamUp = new TeamUp();
        commandTeamUp.GetStandPiece(targetCell.mCurrentPiece);//target
        commandTeamUp.GetMovePiece(basePieces);
        PiecesManager.rotation = false;
        UndoManager.instance.InsertCommand(commandTeamUp);

        basePieces.TeamUp(targetCell.mCurrentPiece.mListPiece.GetPieceNamesList());//teamup
        if (basePieces.mCurrentCell != null)
            basePieces.mCurrentCell.mCurrentPiece = null;
        //switch cells
        Destroy(targetCell.mCurrentPiece.gameObject);
        basePieces.mCurrentCell = targetCell;
        basePieces.mCurrentCell.mCurrentPiece = basePieces;
        basePieces.transform.SetParent(PiecesManager.transform);
        basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
        yield return new WaitForSeconds(0.3f);
        //GameController.instance.ShieldGame();
        //targetCell = null;
        basePieces.mIsPlace = true;
        basePieces.ClearCells();
        // GameController.instance.SelectedCellColor(false);
        gameController.PieceState = State.None;
        // gameController.SelectedPiece = basePieces;
        string NamePiece = basePieces.mNamePiece;
        if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
        {
            PiecesManager.instance.SwitchSides(!basePieces.mColor);
        }
        else
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
            //else
            //{
            //    DataCell dataCell = basePieces.CheckPathingAtCell(basePieces.mCurrentCell);
            //    if (basePieces.MStateRotation != dataCell.dir)
            //    {
            //        //rotation
            //        CheckRotation(dataCell.dir, basePieces);
            //        PiecesManager.SwitchSides(!basePieces.mColor);
            //    }
            //    PiecesManager.SwitchSides(!basePieces.mColor);
            //}
            PiecesManager.SwitchSides(!basePieces.mColor);
        }
    }
    private IEnumerator RotationPieceBot(BasePieces piece, Cell cell, int dir)
    {
        yield return new WaitForSeconds(0.1f);
        string NamePiece = piece.mNamePiece;
        if (NamePiece != "Long" && NamePiece != "Phuong" && NamePiece != "Vuong")
        {
            if (piece.MStateRotation != dir)
            {
                //rotation
                CheckRotation(dir, piece);
            }
        }
        PiecesManager.SwitchSides(!piece.mColor);
    }
    public IEnumerator MoveFromStoretoCellBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        Store store = StorePieces.Instance.GetInfo(basePieces.mNamePiece, basePieces.mColor);
        if (store != null)
        {
            MoveFromStoreToCell command = new MoveFromStoreToCell(store.pos.localPosition, targetCell, store.teamColor);
            UndoManager.instance.InsertCommand(command);
        }
        PiecesManager.rotation = false;
        StorePieces.Instance.ShowSelectCell(basePieces.transform, false);
        GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.MStateRotation, basePieces.mNamePiece);
        newPiece.GetComponent<BasePieces>().isDropPiece = true;
        //gameController.SelectedPiece = newPiece.GetComponent<BasePieces>();
        newPiece.transform.position = basePieces.transform.position;
        basePieces.ClearCells();
        //gameController.SelectedCellColor(false);
        basePieces = newPiece.GetComponent<BasePieces>();
        // newPiece.GetComponent<BasePieces>().Move(targetCell, false, false);
        MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, false);
        yield return new WaitForSeconds(0.3f);
        StorePieces.Instance.Remove(basePieces.mNamePiece, basePieces.mColor);
        gameController.SetState(State.None);
        // gameController.ClearListHightLightCell();
        string NamePiece = basePieces.mNamePiece;
        if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
        {
            PiecesManager.instance.SwitchSides(!basePieces.mColor);
        }
        else
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
            else PiecesManager.SwitchSides(!basePieces.mColor);
        }
    }
    public IEnumerator TeamupFromStoretoCellBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        TeamUpFormStoreToCell command = new TeamUpFormStoreToCell();
        command.GetInfoOldPieces(targetCell.mCurrentPiece, basePieces.transform.position, targetCell.mCurrentPiece.mColor);

        //create new piece
        StorePieces.Instance.ShowSelectCell(basePieces.transform, false);
        GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.MStateRotation, basePieces.mNamePiece);
        newPiece.GetComponent<BasePieces>().isDropPiece = true;

        command.GetInfoNewPiece(newPiece.GetComponent<BasePieces>().mNamePiece);
        UndoManager.instance.InsertCommand(command);//done command

        //gameController.SelectedPiece = newPiece.GetComponent<BasePieces>();
        newPiece.transform.position = basePieces.transform.position;//done create

        newPiece.GetComponent<BasePieces>().TeamUp(targetCell.mCurrentPiece.mListPiece.GetPieceNamesList());//teamup
        //newPiece.GetComponent<BasePieces>().Move(targetCell.mCurrentPiece.mCurrentCell, true, false);//move
        MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, true, false);
        yield return new WaitForSeconds(0.3f);
        // gameController.SelectedPiece = newPiece.GetComponent<BasePieces>();
        Destroy(targetCell.mCurrentPiece.gameObject);

        StorePieces.Instance.Remove(basePieces.mNamePiece, basePieces.mColor);//remove from store
        gameController.SetState(State.None);
        basePieces.ClearCells();
        //gameController.ClearListHightLightCell();
        // gameController.SelectedCellColor(false);

        basePieces = newPiece.GetComponent<BasePieces>();

        string NamePiece = basePieces.mNamePiece;
        if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
        {
            PiecesManager.instance.SwitchSides(!basePieces.mColor);
        }
        else
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
            else PiecesManager.SwitchSides(!basePieces.mColor);
        }
    }
    public IEnumerator MoveSingleBot(BasePieces basePieces, Cell targetcell, int newDir)
    {
        MoveSingleCommand command = new MoveSingleCommand(basePieces, targetcell);
        UndoManager.instance.InsertCommand(command);
        MoveBot(basePieces, targetcell, false, false);
        yield return new WaitForSeconds(0.3f);
        string NamePiece = basePieces.mNamePiece;
        if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
        {
            PiecesManager.instance.SwitchSides(!basePieces.mColor);
        }
        else
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
            //else
            //{
            //    DataCell dataCell = basePieces.CheckPathingAtCell(basePieces.mCurrentCell);
            //    if (basePieces.MStateRotation != dataCell.dir)
            //    {
            //        //rotation
            //        CheckRotation(dataCell.dir, basePieces);
            //        PiecesManager.SwitchSides(!basePieces.mColor);
            //    }
            //    PiecesManager.SwitchSides(!basePieces.mColor);
            //}
            PiecesManager.SwitchSides(!basePieces.mColor);
        }
    }
    public bool CheckRotation(int targetDir, BasePieces basePieces)
    {
        switch (basePieces.MStateRotation)
        {
            case 1:
                if (basePieces.kind == Kind.UI)
                {
                    basePieces.MStateRotation = targetDir;
                    basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                    return true;
                }
                else
                {
                    if (targetDir == 4 || targetDir == 2)
                    {
                        basePieces.MStateRotation = targetDir;
                        basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                        return true;
                    }
                }
                break;
            case 2:
                if (basePieces.kind == Kind.UI)
                {
                    basePieces.MStateRotation = targetDir;
                    basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                    return true;
                }
                else
                {
                    if (targetDir == 1 || targetDir == 3)
                    {
                        basePieces.MStateRotation = targetDir;
                        basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                        return true;
                    }
                }
                break;
            case 3:
                if (basePieces.kind == Kind.UI)
                {
                    basePieces.MStateRotation = targetDir;
                    basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                    return true;
                }
                else
                {
                    if (targetDir == 2 || targetDir == 4)
                    {
                        basePieces.MStateRotation = targetDir;
                        basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                        return true;
                    }
                }
                break;
            case 4:
                if (basePieces.kind == Kind.UI)
                {
                    basePieces.MStateRotation = targetDir;
                    basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                    return true;
                }
                else
                {
                    if (targetDir == 1 || targetDir == 3)
                    {
                        basePieces.MStateRotation = targetDir;
                        basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                        return true;
                    }
                }
                break;
        }
        return false;
    }
    public void MoveBot(BasePieces basePieces, Cell targetCell, bool isTeamup, bool isKill)
    {
        SoundController.Instance.PlayVfxClip(2);

        //   GameController.dropPiece = basePieces.isDropPiece;
        if (basePieces.mCurrentCell != null)
            basePieces.mCurrentCell.mCurrentPiece = null;
        //switch cells
        basePieces.mCurrentCell = targetCell;
        basePieces.mCurrentCell.mCurrentPiece = basePieces;
        basePieces.transform.SetParent(PiecesManager.transform);
        basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
        //  GameController.instance.ShieldGame();
        //targetCell = null;
        basePieces.mIsPlace = true;
        basePieces.ClearCells();
        // GameController.instance.SelectedCellColor(false);

        //  PiecesManager.SwitchSides(!basePieces.mColor);
    }

    /* action for minimax */
    public Stack<ICommandMini> undoCommand = new Stack<ICommandMini>();
    public void InsertCommandMini(ICommandMini command)
    {
        if (undoCommand.Count >= 0)
        {
            undoCommand.Push(command);
        }
    }
    public void UndoMini()
    {
        if (undoCommand.Count > 0)
        {
            ICommandMini command = undoCommand.Pop();
            command.Undo();
        }
    }


    #region Action bot not contain animation

    public void MoveSingle(BasePieces basePieces, Cell targetcell, int newDir)
    {
        /*
         save current cell
         clear current piece in current cell
         => set current piece to target cell

        undo
        clear target cell
        set current piece to current cell
         */
        MoveSingleMini command = new MoveSingleMini();
        command.currentPiece = basePieces;
        command.currentCell = basePieces.mCurrentCell;
        command.targetCell = targetcell;
        command.dir = basePieces.MStateRotation;
        InsertCommandMini(command);

        Cell currentCell = basePieces.mCurrentCell;
        currentCell.mCurrentPiece = null;

        targetcell.mCurrentPiece = basePieces;
        basePieces.mCurrentCell = targetcell;

        basePieces.MStateRotation = newDir;
    }

    public void MoveSingleinteam(BasePieces basePieces, Cell targetCell, int newDir)
    {
        MovesingleinteamMini command = new MovesingleinteamMini();
        command.mainPiece = basePieces;
        command.currentCell = basePieces.mCurrentCell;
        command.targetCell = targetCell;
        command.dir = newDir;

        InsertCommandMini(command);

        Cell currentCell = basePieces.mCurrentCell;

        List<PieceInfo> clone = basePieces.mListPiece.Clone();
        PieceInfo pieceInfo = new PieceInfo(clone[0].nPieceName, clone[0].mPieceRotation);
        clone.RemoveAt(0);

        GameObject newPiece1 = PiecesManager.instance.CreateBasePiece(basePieces.mColor, pieceInfo.mPieceRotation, pieceInfo.nPieceName);
        BasePieces piece1 = newPiece1.GetComponent<BasePieces>();

        GameObject newPiece2 = PiecesManager.instance.CreateBasePiece(basePieces.mColor, clone[0].mPieceRotation, clone[0].nPieceName);
        BasePieces piece2 = newPiece2.GetComponent<BasePieces>();
        piece2.mListPiece.RemovePieceList();
        piece2.mListPiece.AddPiecesList(clone);

        piece1.mCurrentCell = currentCell;
        currentCell.mCurrentPiece = piece1;
        piece1.MStateRotation = newDir;

        piece2.mCurrentCell = targetCell;
        targetCell.mCurrentPiece = piece2;


        basePieces.GetComponent<calTest>().Unsubcribe();
        /*
         Create 2 new item => 1 set in current cell , 1 set to target cell => rotate piece in target cell neu co


         //Unsubcribe => this piece

        Undo => delete 2 item => Subcribe this piece 
         */


        /*
        if (targetCell.mCurrentPiece == null)
        {
            MoveSingleinTeam command = new MoveSingleinTeam();
            GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

            PiecesManager.rotation = false;

            if (newPiece.GetComponent<BasePieces>() != null)
            {
                newPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                //newPiece.GetComponent<BasePieces>().Move(targetCell, false, false);
                MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, false);
                yield return new WaitForSeconds(0.3f);
                GameController.instance.SelectedPiece = newPiece.GetComponent<BasePieces>();
                //newPiece.GetComponent<BasePieces>().rotationCS.StartCoroutine("ActiveButton");
                command.GetMovePiece(newPiece.GetComponent<BasePieces>());
            }
            basePieces.mListPiece.RemovePieceList();

            if (basePieces.mListPiece.GetListNameCount() > 1)
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
                basePieces.mListPiece.RemovePieceList();
                newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(basePieces.mListPiece.GetPieceNamesList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            else
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }

            UndoManager.instance.InsertCommand(command);
            //GameController.instance.mCurentBasePiece.ClearCells();
            //GameController.instance.SelectedCellColor(false);
            //GameController.instance.mTargetCell = null;
            //GameController.instance.ClearListHightLightCell();
            GameController.instance.SetState(State.None);

            //check Rotation
            if (basePieces.checkNamePiece(basePieces)) PiecesManager.SwitchSides(!basePieces.mColor);
            else
            {
                if (basePieces.MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, basePieces);
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                else
                {
                    DataCell dataCell = newPiece.GetComponent<BasePieces>().CheckPathingAtCell(basePieces.mCurrentCell);
                    if (newPiece.GetComponent<BasePieces>().MStateRotation != dataCell.dir)
                    {
                        //rotation
                        CheckRotation(dataCell.dir, newPiece.GetComponent<BasePieces>());
                        PiecesManager.SwitchSides(!basePieces.mColor);
                    }
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
            }
            Destroy(basePieces.gameObject, 0.4f);
        }
        */
    }

    public void MoveTeam(BasePieces basePieces, Cell targetCell, int newDir)
    {
        MoveTeamMini command = new MoveTeamMini();
        command.currentPiece = basePieces;
        command.currentCell = basePieces.mCurrentCell;
        command.targetCell = targetCell;
        command.dir = basePieces.MStateRotation;
        InsertCommandMini(command);

        Cell currentCell = basePieces.mCurrentCell;
        currentCell.RemovePiece();

        basePieces.mCurrentCell = targetCell;
        basePieces.MStateRotation = newDir;

        /*
         Change current cell cua piece hien tai

        Undo
        clear target cell
        assign lai current cell
         */



        /*
        if (targetCell.mCurrentPiece == null)
        {
            // GameController.instance.Shield.SetActive(true);
            MoveTeam command = new MoveTeam(basePieces, targetCell);
            UndoManager.instance.InsertCommand(command);
            PiecesManager.rotation = false;

            basePieces.mCurrentCell.mCurrentPiece = null;
            basePieces.mCurrentCell = targetCell;
            basePieces.mCurrentCell.mCurrentPiece = basePieces;

            basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
            yield return new WaitForSeconds(0.3f);
            //GameController.instance.ShieldGame();

            basePieces.mTargetCell = null;
            basePieces.mIsPlace = true;
            basePieces.ClearCells();

            //GameController.instance.mCurentBasePiece.ClearCells();

            //GameController.instance.mTargetCell = null;
            //GameController.instance.ClearListHightLightCell();
            //GameController.instance.SelectedCellColor(false);
            GameController.instance.SetState(State.None);

            if (basePieces.checkNamePiece(basePieces)) PiecesManager.SwitchSides(!basePieces.mColor);
            else
            {
                if (basePieces.MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, basePieces);
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                else
                {
                    DataCell dataCell = basePieces.CheckPathingAtCell(basePieces.mCurrentCell);
                    if (basePieces.MStateRotation != dataCell.dir)
                    {
                        //rotation
                        CheckRotation(dataCell.dir, basePieces);
                        PiecesManager.SwitchSides(!basePieces.mColor);
                    }
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
            }
        }
        */
    }

    public void MoveOnlyAndKill(BasePieces basePieces, Cell targetCell, int newDir)
    {
        MoveOnlyAndKillMini command = new MoveOnlyAndKillMini();
        command.mainPieceCurrent = basePieces;
        command.mainPieceTarget = targetCell.mCurrentPiece;
        command.currentCell = basePieces.mCurrentCell;
        command.targetCell = targetCell;
        command.dir = basePieces.MStateRotation;

        InsertCommandMini(command);

        BasePieces currentPiece = basePieces;
        BasePieces targetPiece = targetCell.mCurrentPiece;
        Cell currentCell = basePieces.mCurrentCell;

        List<PieceInfo> cloneTarget = targetPiece.mListPiece.Clone();

        currentPiece.GetComponent<calTest>().Unsubcribe();
        targetPiece.GetComponent<calTest>().Unsubcribe();

        List<PieceInfo> clone = basePieces.mListPiece.Clone();
        PieceInfo pieceInfo = new PieceInfo(clone[0].nPieceName, clone[0].mPieceRotation);
        clone.RemoveAt(0);

        GameObject newPiece1 = PiecesManager.instance.CreateBasePiece(basePieces.mColor, pieceInfo.mPieceRotation, pieceInfo.nPieceName);
        BasePieces piece1 = newPiece1.GetComponent<BasePieces>();

        GameObject newPiece2 = PiecesManager.instance.CreateBasePiece(basePieces.mColor, clone[0].mPieceRotation, clone[0].nPieceName);
        BasePieces piece2 = newPiece2.GetComponent<BasePieces>();
        piece2.mListPiece.RemovePieceList();
        piece2.mListPiece.AddPiecesList(clone);

        piece1.mCurrentCell = targetCell;
        targetCell.mCurrentPiece = piece1;
        piece1.MStateRotation = newDir;

        piece2.mCurrentCell = currentCell;
        currentCell.mCurrentPiece = piece2;

        foreach (PieceInfo item in cloneTarget)
        {
            StorePieces.Instance.Add(item.nPieceName, targetCell.mCurrentPiece.mColor,false);
        }
        /*
        unsubcribe piece in target cell
        unsubcribe piece in current cell
        create 2 piece => 1 piece current cell , 1 piece target cell 

        undo => subcribe piece in target cell
        undo => subcribe piece in current cell
        delete 2 piece k can thiet
         */



        /*
        if (targetCell.mCurrentPiece != null)
        {
            BasePieces targetPiece = targetCell.mCurrentPiece;
            MoveSingleandKill command = new MoveSingleandKill();
            GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
            PiecesManager.rotation = false;

            if (newPiece.GetComponent<BasePieces>() != null)
            {
                newPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, true);
                yield return new WaitForSeconds(0.3f);
                //newPiece.GetComponent<BasePieces>().Move(targetCell, false, true);
                GameController.instance.SelectedPiece = newPiece.GetComponent<BasePieces>();
                //newPiece.GetComponent<BasePieces>().rotationCS.StartCoroutine("ActiveButton");
                command.GetMovePiece(newPiece.GetComponent<BasePieces>());
            }

            basePieces.mListPiece.RemovePieceList();

            if (basePieces.mListPiece.GetListNameCount() > 1)
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
                basePieces.mListPiece.RemovePieceList();
                newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(basePieces.mListPiece.GetPieceNamesList());
                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            else
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            // GameController.instance.mCurentBasePiece.ClearCells();
            //GameController.instance.ClearListHightLightCell();
            GameController.instance.SetState(State.None);
            //GameController.instance.SelectedCellColor(false);
            command.GetKilledPiece(GameController.instance.mTargetBasePiece);
            targetPiece.Kill(targetPiece.mListPiece.GetPieceNamesList(), command);
            UndoManager.instance.InsertCommand(command);
            GameController.instance.mTargetCell = null;
            if (basePieces.mNamePiece == "Vuong" || basePieces.mNamePiece == "Long" || basePieces.mNamePiece == "Phuong") PiecesManager.SwitchSides(!basePieces.mColor);
            else
            {
                if (newPiece.GetComponent<BasePieces>().MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, newPiece.GetComponent<BasePieces>());
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                else
                {
                    DataCell dataCell = newPiece.GetComponent<BasePieces>().CheckPathingAtCell(basePieces.mCurrentCell);
                    if (newPiece.GetComponent<BasePieces>().MStateRotation != dataCell.dir)
                    {
                        //rotation
                        CheckRotation(dataCell.dir, newPiece.GetComponent<BasePieces>());
                        PiecesManager.SwitchSides(!basePieces.mColor);
                    }
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
            }
            Destroy(basePieces.gameObject, 0.4f);
        }*/
    }

    public void MoveTeamAndKill(BasePieces basePieces, Cell targetCell, int newDir)
    {
        MoveTeamAndKillMini command = new MoveTeamAndKillMini();
        command.currentPiece = basePieces;
        command.targetPiece = targetCell.mCurrentPiece;
        command.currentCell = basePieces.mCurrentCell;
        command.targetCell = targetCell;
        command.dir = basePieces.MStateRotation;

        InsertCommandMini(command);

        BasePieces targetPiece = targetCell.mCurrentPiece;
        List<PieceInfo> clone = targetPiece.mListPiece.Clone();
        targetPiece.GetComponent<calTest>().Unsubcribe();

        basePieces.mCurrentCell = targetCell;
        targetCell.mCurrentPiece = basePieces;
        basePieces.MStateRotation = newDir;

        foreach (var item in clone)
        {
            StorePieces.Instance.Add(item.nPieceName, targetCell.mCurrentPiece.mColor,false);
        }
        /*
        unsubcribe piece in target cell
        assign this piece to target cell
        save current cell, and piece in target cell lai

        undo
        subcribe piece in target cell
        assign this piece to current cell
        assign piece in target cell => target cell
         */


        /*
        if (targetCell.mCurrentPiece != null)
        {
            BasePieces targetPiece = targetCell.mCurrentPiece;
            //GameController.instance.Shield.SetActive(true);
            MoveTeamandKill command = new MoveTeamandKill(basePieces, targetCell);
            command.GetKilledPiece(GameController.instance.mTargetBasePiece);
            targetPiece.Kill(targetPiece.mListPiece.GetPieceNamesList(), command);
            UndoManager.instance.InsertCommand(command);

            PiecesManager.rotation = false;
            basePieces.mCurrentCell.mCurrentPiece = null;
            basePieces.mCurrentCell = targetCell;
            basePieces.mCurrentCell.mCurrentPiece = basePieces;
            basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
            yield return new WaitForSeconds(0.3f);
            //GameController.instance.ShieldGame();
            basePieces.mTargetCell = null;
            basePieces.ClearCells();
            // GameController.instance.SelectedCellColor(false);
            //GameController.instance.mTargetCell = null;
            // GameController.instance.ClearListHightLightCell();
            GameController.instance.SetState(State.None);
            if (basePieces.mNamePiece == "Vuong" || basePieces.mNamePiece == "Long" || basePieces.mNamePiece == "Phuong") PiecesManager.SwitchSides(!basePieces.mColor);
            else
            {
                if (basePieces.MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, basePieces);
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                else
                {
                    DataCell dataCell = basePieces.GetComponent<BasePieces>().CheckPathingAtCell(basePieces.mCurrentCell);
                    if (basePieces.GetComponent<BasePieces>().MStateRotation != dataCell.dir)
                    {
                        //rotation
                        CheckRotation(dataCell.dir, basePieces.GetComponent<BasePieces>());
                        PiecesManager.SwitchSides(!basePieces.mColor);
                    }
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
            }

        }*/
    }

    public void TeamUp(BasePieces basePieces, Cell targetCell, int newDir)
    {
        /*
         create new piece and set in target cell
         current piece unsubcribe
         target piece unsubcribe
         */
        TeamUpMini command = new TeamUpMini();
        command.currentPiece = basePieces;
        command.targetPiece = targetCell.mCurrentPiece;
        command.currentCell = basePieces.mCurrentCell;
        command.targetCell = targetCell;
        command.dir = basePieces.MStateRotation;
 

        BasePieces targetPiece = targetCell.mCurrentPiece;
        Cell currentCell = basePieces.mCurrentCell;
        List<PieceInfo> clone = targetPiece.mListPiece.Clone();

        BasePieces piece = basePieces.Clone(true);
        command.deletePiece = piece;
        InsertCommandMini(command);
        piece.mListPiece.AddPiecesList(clone);

        piece.mCurrentCell = targetCell;
        targetCell.mCurrentPiece = piece;
        piece.MStateRotation = newDir;

        currentCell.mCurrentPiece = null;

        basePieces.GetComponent<calTest>().Unsubcribe();
        targetPiece.GetComponent<calTest>().Unsubcribe();


        /*
        TeamUp commandTeamUp = new TeamUp();
        commandTeamUp.GetStandPiece(targetCell.mCurrentPiece);//target
        commandTeamUp.GetMovePiece(basePieces);
        PiecesManager.rotation = false;
        UndoManager.instance.InsertCommand(commandTeamUp);

        basePieces.TeamUp(targetCell.mCurrentPiece.mListPiece.GetPieceNamesList());//teamup
        if (basePieces.mCurrentCell != null)
            basePieces.mCurrentCell.mCurrentPiece = null;
        //switch cells
        Destroy(targetCell.mCurrentPiece.gameObject);
        basePieces.mCurrentCell = targetCell;
        basePieces.mCurrentCell.mCurrentPiece = basePieces;
        basePieces.transform.SetParent(PiecesManager.transform);
        basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
        yield return new WaitForSeconds(0.3f);
        //GameController.instance.ShieldGame();
        //targetCell = null;
        basePieces.mIsPlace = true;
        basePieces.ClearCells();
        // GameController.instance.SelectedCellColor(false);
        gameController.PieceState = State.None;
        // gameController.SelectedPiece = basePieces;
        string NamePiece = basePieces.mNamePiece;
        if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
        {
            PiecesManager.instance.SwitchSides(!basePieces.mColor);
        }
        else
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
            else
            {
                DataCell dataCell = basePieces.CheckPathingAtCell(basePieces.mCurrentCell);
                if (basePieces.MStateRotation != dataCell.dir)
                {
                    //rotation
                    CheckRotation(dataCell.dir, basePieces);
                    PiecesManager.SwitchSides(!basePieces.mColor);
                }
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
        }
        */
    }

    public void MoveFromStoretoCell(BasePieces basePieces, Cell targetCell, int newDir)
    {
        /*
         Create piece 
         Set in target cell
        - di trong storePiece
         */
        MoveFromStoretoCell command = new MoveFromStoretoCell();
        command.currentCell = targetCell;


        StorePieces.Instance.Remove(basePieces.mNamePiece, basePieces.mColor, false);
        GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.MStateRotation, basePieces.mNamePiece);
        BasePieces piece = newPiece.GetComponent<BasePieces>();

        command.piece = piece;
        InsertCommandMini(command);

        piece.mIsPlace = true;
        piece.isDropPiece = false;
        piece.mCurrentCell = targetCell;
        targetCell.mCurrentPiece = piece;
        piece.MStateRotation = newDir;
        /*
        Store store = StorePieces.Instance.GetInfo(basePieces.mNamePiece, basePieces.mColor);
        if (store != null)
        {
            MoveFromStoreToCell command = new MoveFromStoreToCell(store.pos.localPosition, targetCell, store.teamColor);
            UndoManager.instance.InsertCommand(command);
        }
        PiecesManager.rotation = false;
        StorePieces.Instance.ShowSelectCell(basePieces.transform, false);
        GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.MStateRotation, basePieces.mNamePiece);
        newPiece.GetComponent<BasePieces>().isDropPiece = true;
        //gameController.SelectedPiece = newPiece.GetComponent<BasePieces>();
        newPiece.transform.position = basePieces.transform.position;
        basePieces.ClearCells();
        //gameController.SelectedCellColor(false);
        basePieces = newPiece.GetComponent<BasePieces>();
        // newPiece.GetComponent<BasePieces>().Move(targetCell, false, false);
        MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, false);
        yield return new WaitForSeconds(0.3f);
        StorePieces.Instance.Remove(basePieces.mNamePiece, basePieces.mColor);
        gameController.SetState(State.None);
        // gameController.ClearListHightLightCell();
        string NamePiece = basePieces.mNamePiece;
        if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
        {
            PiecesManager.instance.SwitchSides(!basePieces.mColor);
        }
        else
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
            else PiecesManager.SwitchSides(!basePieces.mColor);
        }*/
    }

    public void TeamupFromStoretoCell(BasePieces basePieces, Cell targetCell, int newDir)
    {

        /*
         Create piece
        teamup => set current cell = target cell
        unsub piece in targetCell

         public BasePieces mainPiece;
    public BasePieces destroyPiece;

    public Cell currentCell;
         */

        TeamupFromStoretoCell command = new TeamupFromStoretoCell();
        command.mainPiece = targetCell.mCurrentPiece;
        command.currentCell = targetCell;
 

        StorePieces.Instance.Remove(basePieces.mNamePiece, basePieces.mColor, false);
        BasePieces targetPiece = targetCell.mCurrentPiece;
        List<PieceInfo> clone = targetPiece.mListPiece.Clone();

        GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.MStateRotation, basePieces.mNamePiece);
        BasePieces piece = newPiece.GetComponent<BasePieces>();

        command.deletePiece = piece;
        InsertCommandMini(command);

        piece.isDropPiece = false;
        piece.mListPiece.AddPiecesList(clone);
        piece.mCurrentCell = targetCell;
        targetCell.mCurrentPiece = piece;

        targetPiece.mCurrentCell = null;
        targetPiece.GetComponent<calTest>().Unsubcribe();

        /*
        TeamUpFormStoreToCell command = new TeamUpFormStoreToCell();
        command.GetInfoOldPieces(targetCell.mCurrentPiece, basePieces.transform.position, targetCell.mCurrentPiece.mColor);

        //create new piece
        StorePieces.Instance.ShowSelectCell(basePieces.transform, false);
        GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.MStateRotation, basePieces.mNamePiece);
        newPiece.GetComponent<BasePieces>().isDropPiece = true;

        command.GetInfoNewPiece(newPiece.GetComponent<BasePieces>().mNamePiece);
        UndoManager.instance.InsertCommand(command);//done command

        //gameController.SelectedPiece = newPiece.GetComponent<BasePieces>();
        newPiece.transform.position = basePieces.transform.position;//done create

        newPiece.GetComponent<BasePieces>().TeamUp(targetCell.mCurrentPiece.mListPiece.GetPieceNamesList());//teamup
        //newPiece.GetComponent<BasePieces>().Move(targetCell.mCurrentPiece.mCurrentCell, true, false);//move
        MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, true, false);
        yield return new WaitForSeconds(0.3f);
        // gameController.SelectedPiece = newPiece.GetComponent<BasePieces>();
        Destroy(targetCell.mCurrentPiece.gameObject);

        StorePieces.Instance.Remove(basePieces.mNamePiece, basePieces.mColor);//remove from store
        gameController.SetState(State.None);
        basePieces.ClearCells();
        //gameController.ClearListHightLightCell();
        // gameController.SelectedCellColor(false);

        basePieces = newPiece.GetComponent<BasePieces>();

        string NamePiece = basePieces.mNamePiece;
        if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
        {
            PiecesManager.instance.SwitchSides(!basePieces.mColor);
        }
        else
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
                PiecesManager.SwitchSides(!basePieces.mColor);
            }
            else PiecesManager.SwitchSides(!basePieces.mColor);
        }*/
    }

    public void Rotate(BasePieces basePieces, Cell targetCell, int newDir)
    {
        RotateMini command = new RotateMini();
        command.piece = basePieces;
        command.cell = basePieces.mCurrentCell;
        command.dir = basePieces.MStateRotation;

        InsertCommandMini(command);

        basePieces.MStateRotation = newDir;
    }

    #endregion

    /*
     
     */
}
#region Old Command
/*
public class MoveSingleminiCommand : ICommandMini
{
    string mPieceName;
    bool mTeamColor;
    Cell mPieceCell;
    int mStateRotaion;
    bool isPlace;
    bool isDropPiece;
    List<PieceInfo> mListPieceInfo = new List<PieceInfo>();
    Cell currentCell;
    public MoveSingleminiCommand(BasePieces piece, Cell currentCell)
    {

        mPieceName = piece.mNamePiece;
        mTeamColor = piece.mColor;
        mPieceCell = piece.mCurrentCell;
        mStateRotaion = piece.MStateRotation;
        isPlace = piece.mIsPlace;
        isDropPiece = piece.isDropPiece;
        mListPieceInfo.AddRange(piece.mListPiece.GetPieceNamesList());
        this.currentCell = currentCell;
     
    }

    public void Undo()
    {
        if (mPieceCell.mCurrentPiece == null)
        {
            if (currentCell.mCurrentPiece.mNamePiece == mPieceName)
            {
                mPieceCell.mCurrentPiece = currentCell.mCurrentPiece;
                mPieceCell.mCurrentPiece.mCurrentCell = mPieceCell;
                mPieceCell.mCurrentPiece.GetandSetStateRotaionMini(mStateRotaion);
                mPieceCell.mCurrentPiece.isDropPiece = isDropPiece;
                mPieceCell.mCurrentPiece.mIsPlace = isPlace;
                currentCell.mCurrentPiece = null;
            }
        }
    }
}
public class MoveTeammini : ICommandMini
{
    string namePiece;
    int stateRotation;
    bool teamColor;
    Cell pastCell;
    Cell presentCell;

    public MoveTeammini(BasePieces piece, Cell targetCell)
    {
        namePiece = piece.mNamePiece;
        stateRotation = piece.MStateRotation;
        teamColor = piece.mColor;
        pastCell = piece.mCurrentCell;
        presentCell = targetCell;
    }

    public void Undo()
    {
        if (pastCell.mCurrentPiece == null)
        {
            pastCell.mCurrentPiece = presentCell.mCurrentPiece;
            pastCell.mCurrentPiece.mCurrentCell = pastCell;
            presentCell.mCurrentPiece = null;
            pastCell.mCurrentPiece.GetandSetStateRotaionMini(stateRotation);
        }

    }
}
public class TeamUpmini : ICommandMini
{
    string standPieceName;

    int standRotation;

    bool teamColor;

    Cell standCell;

    bool isPlace;

    bool isDropPiece;

    List<PieceInfo> standList = new List<PieceInfo>();

    List<PieceInfo> moveList = new List<PieceInfo>();

    int moveRotaion;

    Cell moveCell;

    public void GetMovePiece(BasePieces piece)
    {
        moveRotaion = piece.MStateRotation;

        moveCell = piece.mCurrentCell;

        isPlace = piece.mIsPlace;
        //Debug.Log("ISPLACE IN TEAMUP : " + isPlace);
        isDropPiece = piece.isDropPiece;
        //moveList = piece.mListPiece.mPiecesInfo;

        moveList.AddRange(piece.mListPiece.GetPieceNamesList());

    }

    public void GetStandPiece(BasePieces piece)
    {

        standPieceName = piece.mNamePiece;

        teamColor = piece.mColor;

        standCell = piece.mCurrentCell;

        // standList = piece.mListPiece.GetPieceNamesList();
        standList.AddRange(piece.mListPiece.GetPieceNamesList());

        standRotation = piece.MStateRotation;

    }
    public void Undo()
    {

        standCell.mCurrentPiece.mListPiece.mPiecesInfo.Clear();
        standCell.mCurrentPiece.mListPiece.AddPiecesList(moveList);
        standCell.mCurrentPiece.transform.position = moveCell.transform.position;

        if (!isPlace)
        {
            standCell.mCurrentPiece.transform.SetParent(moveCell.transform);
            standCell.mCurrentPiece.transform.localScale = Vector3.one;
        }

        standCell.mCurrentPiece.GetandSetStateRotaion(moveRotaion);
        moveCell.mCurrentPiece = standCell.mCurrentPiece;
        moveCell.mCurrentPiece.mCurrentCell = moveCell;
        moveCell.mCurrentPiece.isDropPiece = isDropPiece;
        moveCell.mCurrentPiece.mIsPlace = isPlace;
        standCell.mCurrentPiece = null;
        GameObject newstandPiece = PiecesManager.instance.CreateBasePiece(teamColor, standRotation, standPieceName);
        newstandPiece.GetComponent<BasePieces>().Place(standCell);

        if (standList.Count >= 2)
        {
            standList.RemoveAt(0);
            newstandPiece.GetComponent<BasePieces>().mListPiece.AddPiecesList(standList);
        }
        if (GameController.playMode == PlayMode.SinglePlay)
        {
            if (UndoManager.instance.undo.undoCommand.Count == 0)
            {
                PiecesManager.instance.SwitchSides(teamColor);
                return;
            }
            if (ControllerBot.teamColor != teamColor)
            {
                PiecesManager.instance.SwitchSides(teamColor);
            }
        }
        else
            PiecesManager.instance.SwitchSides(teamColor);
    }

}
*/
#endregion

