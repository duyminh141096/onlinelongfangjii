/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: ControllerBot.cs
* Script Author: MinhLe 
* Created On: 7/6/2020 10:16:07 PM*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public partial class ControllerBot : MonoBehaviour
{
    public List<Calculator> green;
    public List<Calculator> orange;

    public List<DataCell> greenListOrange = new List<DataCell>();
    public List<DataCell> greenListGreen = new List<DataCell>();

    public List<DataCell> yellowListOrange = new List<DataCell>();
    public List<DataCell> yellowListGreen = new List<DataCell>();

    public List<DataCell> redListOrange = new List<DataCell>();
    public List<DataCell> redListGreen = new List<DataCell>();

    public List<DataCell> bestListOrange = new List<DataCell>();
    public List<DataCell> bestListGreen = new List<DataCell>();

    public List<Cell> safecells;
    public GameObject parentCells;
    public static ControllerBot Instance;

    public GameObject parentItemNormal;
    public GameObject parentItemFake;

    /// <summary>
    /// tra ve true neu user pick green team
    /// false neu user pick orange team
    /// </summary>
    public static bool teamColor;

    [Header("Parent Pieces")]
    public GameObject parentAllNormalPiece;
    public GameObject parentGreenUIPiece;
    public GameObject parentOrangeUIPiece;
    [Header("RETURN DATA")]
    public DataNeeded ReturnData;//for testing
    [Header("CONTROL VARIABLE")]
    public GameController gameController;//for testing
    public PiecesManager PiecesManager;

    public Minimax Minimax;
    private void Awake()
    {
        Instance = this;
        green = new List<Calculator>();
        orange = new List<Calculator>();
        greenListOrange = new List<DataCell>();
        greenListGreen = new List<DataCell>();

        yellowListOrange = new List<DataCell>();
        yellowListGreen = new List<DataCell>();

        redListOrange = new List<DataCell>();
        redListGreen = new List<DataCell>();
        bestListOrange = new List<DataCell>();
        bestListGreen = new List<DataCell>();
    }

    public void GetPieces(GameObject parent)
    {
        #region Normal
        List<GameObject> allchild = parent.GetAllChild();
        for (int i = 0; i < allchild.Count; i++)
        {
            Calculator data = allchild[i].GetComponent<calTest>().calculator;
            BasePieces basePieces = allchild[i].GetComponent<BasePieces>();

            data.ClearData();
            basePieces.mListPiece.UpdateScore();
            basePieces.UpdateStatusPiece();
            if (basePieces.mColor == true)
                orange.Add(data);
            else
                green.Add(data);
        }
        #endregion
        #region UI 
        List<GameObject> childsGreen = parentGreenUIPiece.GetAllChild();
        List<GameObject> childsOrange = parentOrangeUIPiece.GetAllChild();
        GetChildUI(childsGreen, true);
        GetChildUI(childsOrange, false);
        //get count 
        #endregion
    }
    void GetChildUI(List<GameObject> GOs, bool green)
    {
        for (int i = 0; i < GOs.Count; i++)
        {
            BasePieces basePieces = GOs[i].GetComponent<BasePieces>();
            Calculator calculator = GOs[i].GetComponent<calTest>().calculator;
            calculator.ClearData();
            basePieces.mListPiece.UpdateScore();
            Store store = StorePieces.Instance.GetInfo(basePieces.mNamePiece, basePieces.mColor);
            if (store.count > 0)
            {
                //do something
                if (green) this.green.Add(calculator);
                else orange.Add(calculator);
            }
        }
    }
    public IEnumerator CallCaculate()
    {
        yield return new WaitForSeconds(0.5f);
        Minimax.Move1(1, teamColor, teamColor);
        /*
         ClearData();

        GetAllCell();
        GetPieces(parentAllNormalPiece);

        yield return Call(green);
        yield return Call(orange);
        Debug.Log("=> End Call");

        GetListDataCell(orange, greenListOrange, yellowListOrange, redListOrange, bestListOrange);
        GetListDataCell(green, greenListGreen, yellowListGreen, redListGreen, bestListGreen);

        //Sap xep
        SortListData();
        GetSafeCellList();

        BotAction(ChoisePieceandCell());

        //Debug.Log("Chon quan : " + ReturnData.piece.mNamePiece + " ID : " + ReturnData.piece.mID + "\nChon O : " + ReturnData.cell.ToString() + "\nTrang thai : " + ReturnData.mode.ToString());
        //ReturnData = ChoisePieceandCell();
        ClearData();
        
         */
    }
    private void GetListDataCell(List<Calculator> calculators, List<DataCell> greenData, List<DataCell> yellowData, List<DataCell> redData, List<DataCell> bestData)
    {
        for (int i = 0; i < calculators.Count; i++)
        {
            greenData.AddRange(calculators[i].greenCells);
            yellowData.AddRange(calculators[i].yellowCells);
            redData.AddRange(calculators[i].redCells);
            bestData.AddRange(calculators[i].bestCells);

            //for (int a = 0; a < calculators[i].greenCells.Count; a++)
            //{
            //    greenData.Add(calculators[i].greenCells[a]);
            //}
            //for (int b = 0; b < calculators[i].yellowCells.Count; b++)
            //{
            //    yellowData.Add(calculators[i].yellowCells[b]);
            //}
            //for (int c = 0; c < calculators[i].redCells.Count; c++)
            //{
            //    redData.Add(calculators[i].redCells[c]);
            //}
            //for (int d = 0; d < calculators[i].bestCells.Count; d++)
            //{
            //    bestData.Add(calculators[i].bestCells[d]);
            //}
        }
    }
    private void ClearData()
    {
        green.Clear();
        orange.Clear();

        greenListOrange.Clear();
        greenListGreen.Clear();

        yellowListOrange.Clear();
        yellowListGreen.Clear();

        redListOrange.Clear();
        redListGreen.Clear();

        bestListGreen.Clear();
        bestListOrange.Clear();
    }
    private void SortListData()
    {
        greenListGreen = greenListGreen.OrderByDescending(x => x.score).ToList() ;
        greenListOrange = greenListOrange.OrderByDescending(x => x.score).ToList();

        yellowListGreen= yellowListGreen.OrderByDescending(x => x.score).ToList();
        yellowListOrange= yellowListOrange.OrderByDescending(x => x.score).ToList();

        redListGreen= redListGreen.OrderByDescending(x => x.score).ToList();
        redListOrange= redListOrange.OrderByDescending(x => x.score).ToList();

        bestListOrange= bestListOrange.OrderByDescending(x => x.score).ToList();
        bestListGreen= bestListGreen.OrderByDescending(x => x.score).ToList();
    }
    public void BotActionForMinimax(DataNeeded data)
    {
        switch (data.mode)
        {
            case mode.MoveSingle:
                MoveSingle(data.piece, data.cell, data.dir);
                break;
            case mode.MoveSingleinTeam:
                MoveSingleinteam(data.piece, data.cell, data.dir);
                break;
            case mode.MoveTeam:
                MoveTeam(data.piece, data.cell, data.dir);
                break;
            case mode.MoveSingleandKill:
                // data.piece.MoveOnlyAndKill(data.cell);
                MoveOnlyAndKill(data.piece, data.cell, data.dir);
                break;
            case mode.MoveTeamandKill:
                MoveTeamAndKill(data.piece, data.cell, data.dir);
                break;
            case mode.RotationPiece:
                Rotate(data.piece, data.cell, data.dir);
                break;
            case mode.TeamUp:
                TeamUp(data.piece, data.cell, data.dir);
                break;
            case mode.MoveFromStoreToCell:
                MoveFromStoretoCell(data.piece, data.cell, data.dir);
                break;
            case mode.TeamUpFormStoreToCell:
                TeamupFromStoretoCell(data.piece, data.cell, data.dir);
                break;
        }
    }

    public void BotAction(DataNeeded data)
    {
        ReturnData = data;
        //gameController.mCurentBasePiece = data.piece;
        //gameController.mCurrentCell = data.piece.mCurrentCell;
        //gameController.mTargetCell = data.cell;
        //gameController.mTargetBasePiece = data.cell.mCurrentPiece;
        switch (data.mode)
        {
            case mode.MoveSingle:
                StartCoroutine(MoveSingleBot(data.piece, data.cell, data.dir));
                break;
            case mode.MoveSingleinTeam:
                StartCoroutine(MoveSingleinteamBot(data.piece, data.cell, data.dir));
                break;
            case mode.MoveTeam:
                StartCoroutine(MoveTeamBot(data.piece, data.cell, data.dir));
                break;
            case mode.MoveSingleandKill:
                // data.piece.MoveOnlyAndKill(data.cell);
                StartCoroutine(MoveOnlyAndKillBot(data.piece, data.cell, data.dir));
                break;
            case mode.MoveTeamandKill:
                StartCoroutine(MoveTeamAndKillBot(data.piece, data.cell, data.dir));
                break;
            case mode.RotationPiece:
                StartCoroutine(RotationPieceBot(data.piece, data.cell, data.dir));
                break;
            case mode.TeamUp:
                StartCoroutine(TeamUpBot(data.piece, data.cell, data.dir));
                break;
            case mode.MoveFromStoreToCell:
                StartCoroutine(MoveFromStoretoCellBot(data.piece, data.cell, data.dir));
                break;
            case mode.TeamUpFormStoreToCell:
                StartCoroutine(TeamupFromStoretoCellBot(data.piece, data.cell, data.dir));
                break;
        }
    }

  
    public List<Calculator> enemyList = new List<Calculator>();
    public List<Calculator> allyList = new List<Calculator>();
    private void GetSafeCellList() // chuan roi ne
    {
        if (teamColor) //true if user choose green => enemy team is green
        {
            for (int i = 0; i < green.Count; i++)
            {
                if (green[i].listCells.Count > 0)
                {
                    for (int y = 0; y < green[i].listCells.Count; y++)
                    {
                        if (safecells.Contains(green[i].listCells[y]))
                        {
                            safecells.Remove(green[i].listCells[y]);
                        }
                    }
                }

            }
        }
        else
        {
            for (int i = 0; i < orange.Count; i++)
            {
                if (orange[i].listCells.Count > 0)
                {
                    for (int y = 0; y < orange[i].listCells.Count; y++)
                    {
                        if (safecells.Contains(orange[i].listCells[y]))
                        {
                            safecells.Remove(orange[i].listCells[y]);
                        }
                    }
                }
            }
        }
    }



    public DataNeeded ChoisePieceandCell()
    {
        Debug.Log("Pick~~~~~~~~");
        List<DataCell> enemyGreenList = new List<DataCell>();
        List<DataCell> allyGreenList = new List<DataCell>();

        List<DataCell> enemyYellowList = new List<DataCell>();
        List<DataCell> allyYellowList = new List<DataCell>();

        List<DataCell> enemyRedList = new List<DataCell>();
        List<DataCell> allyRedList = new List<DataCell>();

        List<DataCell> enemybestList = new List<DataCell>();
        List<DataCell> allybestList = new List<DataCell>();
        enemyList = new List<Calculator>();
        allyList = new List<Calculator>();
        if (teamColor)
        {
            enemyList = green;
            allyList = orange;

            enemyGreenList = greenListGreen;
            allyGreenList = greenListOrange;

            enemyYellowList = yellowListGreen;
            allyYellowList = yellowListOrange;

            enemyRedList = redListGreen;
            allyRedList = redListOrange;

            enemybestList = bestListGreen;
            allybestList = bestListOrange;
        }
        else
        {
            enemyList = orange;
            allyList = green;

            enemyGreenList = greenListOrange;
            allyGreenList = greenListGreen;

            enemyYellowList = yellowListOrange;
            allyYellowList = yellowListGreen;

            enemyRedList = redListOrange;
            allyRedList = redListGreen;

            enemybestList = bestListOrange;
            allybestList = bestListGreen;
        }

        ////////////////////////////
        ///New Attack
        ////////////////////////////

        //check xem co an duoc Tuong ben kia khong
        if (allyGreenList.Count > 0)
        {
            if (allyGreenList[0].score == 100)// Trung me con Vuong roi . choi no thoi =)) 
            {
                DataNeeded dataNeeded = new DataNeeded();
                dataNeeded.mode = mode.MoveTeamandKill;
                dataNeeded.piece = allyGreenList[0].piece;
                dataNeeded.cell = allyGreenList[0].cell;
                dataNeeded.dir = allyGreenList[0].dir;
                return dataNeeded;
            }
        }
        if (enemyGreenList.Count > 0) //Quân địch có nước để ăn quân
        {
            if (enemyGreenList[0].score == 100)// Kiểm tra xem có đang bị chiếu hay không
            {
                //chieu tuong
                //Check xem có ăn được cái con đang chiếu không
                DataNeeded dataNeeded = new DataNeeded();
                dataNeeded = KillThisPiece(enemyGreenList[0].piece.mCurrentCell, allyGreenList);
                if (dataNeeded != null) //Có giá trị trả về là có thể giết được
                {
                    return dataNeeded;
                }
                else
                {
                    //Không có giá trị trả về , tìm cách thủ
                    DataNeeded movekingData = new DataNeeded();
                    movekingData = MoveKingData(enemyGreenList[0], enemyGreenList[0].cell);
                    if (movekingData != null)//Move được
                    {
                        return movekingData;
                    }
                    else
                    {
                        BasePieces targetPiece = (BasePieces)enemyGreenList[0].piece.Clone();
                        BasePieces kingPiece = (BasePieces)enemyGreenList[0].cell.mCurrentPiece.Clone();

                        List<Cell> targetCells = targetPiece.CheckPathingBot();
                        List<Cell> kingCells = kingPiece.CheckPathingBot();

                        List<Cell> canMoveCell = new List<Cell>();
                        List<Cell> containCells = new List<Cell>();
                        for (int i = 0; i < targetCells.Count; i++)
                        {
                            if (kingCells.Contains(targetCells[i]))
                            {
                                containCells.Add(targetCells[i]);
                            }
                        }
                        if (containCells.Contains(enemyGreenList[0].cell))//remove cell of king
                            containCells.Remove(enemyGreenList[0].cell);
                        List<DataNeeded> canMove = new List<DataNeeded>();
                        for (int i = 0; i < allyRedList.Count; i++)
                        {
                            for (int z = 0; z < containCells.Count; z++)
                            {
                                if (allyRedList[i].cell == containCells[z])
                                {
                                    if (allyRedList[i].piece.canTeamUp) // con piece hien tai chi co 1 minh
                                    {
                                        if (!containCells[z].ContainPiece())// team up here
                                        {
                                            DataNeeded data = new DataNeeded();
                                            data.mode = mode.MoveSingle;
                                            data.piece = allyRedList[i].piece;
                                            data.cell = containCells[z];
                                            data.dir = allyRedList[i].dir;
                                            canMove.Add(data);
                                        }
                                    }
                                    else// can't team up , check free cell thi move
                                    {
                                        if (!containCells[z].ContainPiece())
                                        {
                                            bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                                            if (randomValue) //move team
                                            {
                                                DataNeeded data = new DataNeeded();
                                                data.mode = mode.MoveTeam;
                                                data.piece = allyRedList[i].piece;
                                                data.cell = containCells[z];
                                                data.dir = allyRedList[i].dir;
                                                canMove.Add(data);
                                            }
                                            else //move only in team
                                            {
                                                DataNeeded data = new DataNeeded();
                                                data.mode = mode.MoveSingleinTeam;
                                                data.piece = allyRedList[i].piece;
                                                data.cell = containCells[z];
                                                data.dir = allyRedList[i].dir;
                                                canMove.Add(data);
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        if (canMove.Count > 0)
                        {
                            return canMove[UnityEngine.Random.Range(0, canMove.Count)];
                        }
                        List<DataNeeded> listData = new List<DataNeeded>();
                        for (int i = 0; i < allyYellowList.Count; i++)
                        {

                            for (int z = 0; z < targetCells.Count; z++)
                            {
                                if (allyYellowList[i].cell.mId == containCells[z].mId)
                                {
                                    DataNeeded data = new DataNeeded();
                                    if (allyYellowList[i].piece.canTeamUp)
                                    {
                                        data.mode = mode.MoveSingle;
                                        data.piece = allyYellowList[i].piece;
                                        data.cell = targetCells[z];
                                        data.dir = allyYellowList[i].dir;
                                        listData.Add(data);
                                    }
                                    else
                                    {
                                        data.mode = mode.MoveSingleinTeam;
                                        data.piece = allyYellowList[i].piece;
                                        data.cell = targetCells[z];
                                        data.dir = allyYellowList[i].dir;
                                        listData.Add(data);
                                    }
                                }
                            }
                        }
                        if (listData.Count > 0)
                        {
                            return listData[UnityEngine.Random.Range(0, listData.Count)];
                        }
                        List<DataNeeded> lastChange = new List<DataNeeded>();
                        for (int i = 0; i < allyRedList.Count; i++)
                        {
                            for (int z = 0; z < targetCells.Count; z++)
                            {
                                if (allyRedList[i].cell.mId == targetCells[z].mId)
                                {
                                    DataNeeded data = new DataNeeded();
                                    if (allyRedList[i].piece.canTeamUp)
                                    {
                                        data.mode = mode.MoveSingle;
                                        data.piece = allyRedList[i].piece;
                                        data.cell = targetCells[z];
                                        data.dir = allyRedList[i].dir;
                                        lastChange.Add(data);
                                    }
                                    else
                                    {
                                        data.mode = mode.MoveSingleinTeam;
                                        data.piece = allyRedList[i].piece;
                                        data.cell = targetCells[z];
                                        data.dir = allyRedList[i].dir;
                                        lastChange.Add(data);
                                    }
                                }
                            }
                        }
                        if (lastChange.Count > 0)
                            return lastChange[UnityEngine.Random.Range(0, lastChange.Count)];
                    }
                }
            }
        }

        //for (int j = 0; j < enemyList.Count; j++)
        //{
        //    for (int i = 0; i < allybestList.Count; i++)
        //    {

        //        if (enemyList[j].listCells.Contains(allybestList[i].cell))
        //        {
        //            allybestList.Remove(allybestList[i]);
        //        }

        //    }
        //}
        //for (int j = 0; j < allyList.Count; j++)
        //{
        //    for (int i = 0; i < enemybestList.Count; i++)
        //    {

        //        if (allyList[j].listCells.Contains(enemybestList[i].cell))
        //        {
        //            enemybestList.Remove(enemybestList[i]);
        //        }

        //    }
        //}

        if (enemybestList.Count > 0)// quan dich co quan de an quan ta
        {
            if (allybestList.Count > 0) //Kiểm tra xem quân ta có ăn được quân địch hay không
            {
                //Tình huống này là cả quân ta và quân địch đều có con để ăn nên sẽ ưu tiên best score bên nào cao hơn để tính tấn công hay phòng thủ

                for (int y = 0; y < enemybestList.Count; y++)
                {
                    for (int z = 0; z < allybestList.Count; z++)
                    {
                        if (enemybestList[y].score > allybestList[z].score)
                        {
                            ///////////////
                            ///enemy dang lon hon => nguy hiem => kiem tra xem con co` do' co an duoc hay khong
                            ///////////////
                            DataNeeded data = KillThisPiece(enemybestList[y].piece.mCurrentCell, allybestList);
                            if (data != null)
                                return data;
                            bool check = true;
                            for (int c = 0; c < allyList.Count; c++)
                            {
                                if (allyList[c].listCells.Contains(enemybestList[y].cell))
                                {
                                    check = false;
                                }
                            }
                            if (check)
                            {
                                List<DataNeeded> listData = new List<DataNeeded>();
                                BasePieces allyPiece = (BasePieces)enemybestList[z].cell.mCurrentPiece.Clone();
                                List<Cell> cells = allyPiece.CheckPathingBot();
                                for (int m = 0; m < cells.Count; m++)
                                {
                                    if (CheckCellContainSafeCells(cells[m]))
                                    {
                                        if (allyPiece.canTeamUp) // con piece hien tai chi co 1 minh
                                        {
                                            if (cells[m].ContainPiece())// team up here
                                            {
                                                DataNeeded dataPieces = new DataNeeded();
                                                dataPieces.mode = mode.TeamUp;
                                                dataPieces.piece = enemybestList[z].cell.mCurrentPiece;
                                                dataPieces.cell = cells[m];
                                                dataPieces.dir = enemybestList[z].cell.mCurrentPiece.MStateRotation;
                                                listData.Add(dataPieces);
                                            }
                                            else//cell check not contain piece
                                            {
                                                DataNeeded dataPieces = new DataNeeded();
                                                dataPieces.mode = mode.MoveSingle;
                                                dataPieces.piece = enemybestList[z].cell.mCurrentPiece;
                                                dataPieces.cell = cells[m];
                                                dataPieces.dir = enemybestList[z].cell.mCurrentPiece.MStateRotation;
                                                listData.Add(dataPieces);
                                            }
                                        }
                                        else// can't team up , check free cell thi move
                                        {
                                            if (!cells[m].ContainPiece())
                                            {
                                                DataNeeded dataPieces = new DataNeeded();
                                                dataPieces.mode = mode.MoveTeam;
                                                dataPieces.piece = enemybestList[z].cell.mCurrentPiece;
                                                dataPieces.cell = cells[m];
                                                dataPieces.dir = enemybestList[z].cell.mCurrentPiece.MStateRotation;
                                                listData.Add(dataPieces);
                                            }
                                        }
                                    }
                                }
                                if (listData.Count > 0)
                                    return listData[UnityEngine.Random.Range(0, listData.Count)];
                            }
                            continue;
                        }
                        else
                        {
                            //Tình huống này bên ta đang có lợi thế nên sẽ ăn quân.
                            bool check = true;
                            for (int m = 0; m < enemyList.Count; m++)
                            {
                                if (enemyList[m].listCells.Contains(allybestList[z].cell))
                                {
                                    check = false;
                                }
                            }
                            if (check)
                            {
                                if (allybestList[z].piece.canTeamUp) // kiem tra tiep con piece dang dung 1 minh hay dang team up
                                {
                                    DataNeeded data = new DataNeeded();
                                    data.mode = mode.MoveTeamandKill;
                                    data.piece = allybestList[z].piece;
                                    data.cell = allybestList[z].cell;
                                    data.dir = allybestList[z].dir;
                                    return data;
                                }
                                else // team up here , random moveteam kill or move only kill
                                {
                                    bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                                    if (randomValue) //move team
                                    {
                                        DataNeeded data = new DataNeeded();
                                        data.mode = mode.MoveTeamandKill;
                                        data.piece = allybestList[z].piece;
                                        data.cell = allybestList[z].cell;
                                        data.dir = allybestList[z].dir;
                                        return data;
                                    }
                                    else
                                    {
                                        DataNeeded data = new DataNeeded();
                                        data.mode = mode.MoveSingleandKill;
                                        data.piece = allybestList[z].piece;
                                        data.cell = allybestList[z].cell;
                                        data.dir = allybestList[z].dir;
                                        return data;
                                    }
                                }
                            }
                            else
                            {
                                int scoreAlly = allybestList[z].piece.mListPiece.score;
                                int scoreEnemy = allybestList[z].cell.mCurrentPiece.mListPiece.score;
                                if(scoreAlly < scoreEnemy)
                                {
                                    if (allybestList[z].piece.canTeamUp) // kiem tra tiep con piece dang dung 1 minh hay dang team up
                                    {
                                        DataNeeded data = new DataNeeded();
                                        data.mode = mode.MoveTeamandKill;
                                        data.piece = allybestList[z].piece;
                                        data.cell = allybestList[z].cell;
                                        data.dir = allybestList[z].dir;
                                        return data;
                                    }
                                    else // team up here , random moveteam kill or move only kill
                                    {
                                        bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                                        if (randomValue) //move team
                                        {
                                            DataNeeded data = new DataNeeded();
                                            data.mode = mode.MoveTeamandKill;
                                            data.piece = allybestList[z].piece;
                                            data.cell = allybestList[z].cell;
                                            data.dir = allybestList[z].dir;
                                            return data;
                                        }
                                        else
                                        {
                                            DataNeeded data = new DataNeeded();
                                            data.mode = mode.MoveSingleandKill;
                                            data.piece = allybestList[z].piece;
                                            data.cell = allybestList[z].cell;
                                            data.dir = allybestList[z].dir;
                                            return data;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


            }
            else //Quân ta không có nước để ăn quân địch
            {
                //Lấy nước mà quân địch sẽ ăn được nhiều điểm nhất
                //chạy quân đó đi hoặc def
                for (int i = 0; i < enemybestList.Count; i++)
                {
                    //Kiểm tra từng phần tử xem, nếu con cờ ở ô ăn của địch nằm trong những cell đi được=> bảo kê rồi => check tiếp những trường hợp khác
                    if (!CheckProtectPiece(enemybestList[i].cell))
                    {
                        DataNeeded defData = DefenceEnemy(enemybestList[i]);
                        if (defData != null) return defData;
                    }
                    else continue;
                }
                //DataNeeded yellowData = CheckYellowList(allyYellowList);
                //if (yellowData != null) return yellowData;
                //else return CheckRedList(allyRedList);
            }
        }
        else // Quân địch không có nước để ăn quân ta
        {
            //Trường hợp này kiểm tra xem quân ta có ăn được quân địch không
            if (allybestList.Count > 0)
            {
                //Có thể ăn được quân của địch, thì lựa chọn quân ngon nhất
                for (int i = 0; i < allybestList.Count; i++)
                {

                    bool check = true;
                    for (int m = 0; m < enemyList.Count; m++)
                    {
                        if (enemyList[m].listCells.Contains(allybestList[i].cell))
                        {
                            check = false;
                        }
                    }
                    if (check)
                    {
                        if (allybestList[i].piece.canTeamUp) // kiem tra tiep con piece dang dung 1 minh hay dang team up
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.MoveTeamandKill;
                            data.piece = allybestList[i].piece;
                            data.cell = allybestList[i].cell;
                            data.dir = allybestList[i].dir;
                            return data;
                        }
                        else // team up here , random moveteam kill or move only kill
                        {
                            bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                            if (randomValue) //move team
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.MoveTeamandKill;
                                data.piece = allybestList[i].piece;
                                data.cell = allybestList[i].cell;
                                data.dir = allybestList[i].dir;
                                return data;
                            }
                            else
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.MoveSingleandKill;
                                data.piece = allybestList[i].piece;
                                data.cell = allybestList[i].cell;
                                data.dir = allybestList[i].dir;
                                return data;
                            }
                        }
                    }
                    else
                    {
                        int scoreAlly = allybestList[i].piece.mListPiece.score;
                        int scoreEnemy = allybestList[i].cell.mCurrentPiece.mListPiece.score;
                        if (scoreAlly < scoreEnemy)
                        {
                            if (allybestList[i].piece.canTeamUp) // kiem tra tiep con piece dang dung 1 minh hay dang team up
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.MoveTeamandKill;
                                data.piece = allybestList[i].piece;
                                data.cell = allybestList[i].cell;
                                data.dir = allybestList[i].dir;
                                return data;
                            }
                            else // team up here , random moveteam kill or move only kill
                            {
                                bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                                if (randomValue) //move team
                                {
                                    DataNeeded data = new DataNeeded();
                                    data.mode = mode.MoveTeamandKill;
                                    data.piece = allybestList[i].piece;
                                    data.cell = allybestList[i].cell;
                                    data.dir = allybestList[i].dir;
                                    return data;
                                }
                                else
                                {
                                    DataNeeded data = new DataNeeded();
                                    data.mode = mode.MoveSingleandKill;
                                    data.piece = allybestList[i].piece;
                                    data.cell = allybestList[i].cell;
                                    data.dir = allybestList[i].dir;
                                    return data;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //Get Yellow list của quân ta, lấy ngẫu nhiên một quân để đánh
                if (allyYellowList.Count > 0)
                {
                    DataNeeded yellowData = CheckYellowList(allyYellowList);
                    if (yellowData != null) return yellowData;
                    else return CheckRedList(allyRedList);
                }
                else // Quân ta đéo có quân nào đi thì nước sau ăn được quân địch
                {
                    return CheckRedList(allyRedList);
                }
            }
        }
        if (allyYellowList.Count > 0)
        {
            DataNeeded yellowData = CheckYellowList(allyYellowList);
            if (yellowData != null) return yellowData;
            else return CheckRedList(allyRedList);
        }
        else // Quân ta đéo có quân nào đi thì nước sau ăn được quân địch
        {
            return CheckRedList(allyRedList);
        }
        /*
         * TH 1 : Green list enemy null && green list ally null
         */
        //if (enemyGreenList.Count == 0 && allyGreenList.Count == 0) // not attack
        //{
        //    Debug.Log("~~~~~~~~~~~~~~~~~ TH 1");
        //    if (allyYellowList.Count > 0)
        //    {
        //        DataNeeded yellowData = CheckYellowList(allyYellowList);
        //        if (yellowData != null) return yellowData;
        //        else return CheckRedList(allyRedList);
        //    }
        //    else
        //    {
        //        red cell
        //        check xem cell do co nam trong safe cell hay khong ? 
        //        return CheckRedList(allyRedList);
        //    }
        //}

        ///*
        // * TH 2 : Green list enemy null && green list ally != null
        // */
        //if (enemyGreenList.Count == 0 && allyGreenList.Count > 0)
        //{
        //    Debug.Log("~~~~~~~~~~~~~~~~~ TH 2");
        //    DataNeeded greenData = CheckGreenListAlly(allyGreenList, allybestList);
        //    if (greenData != null) return greenData;
        //    else
        //    {
        //        if (allyYellowList.Count > 0)
        //        {
        //            DataNeeded yellowData = CheckYellowList(allyYellowList);
        //            if (yellowData != null) return yellowData;
        //            else return CheckRedList(allyRedList);
        //        }
        //        else
        //        {
        //            red cell
        //            check xem cell do co nam trong safe cell hay khong ? 
        //            return CheckRedList(allyRedList);
        //        }
        //    }
        //}
        ///*
        // * TH 3 : Green list enemy != null && green list ally  null
        // */
        //if (enemyGreenList.Count > 0 && allyGreenList.Count == 0)
        //{
        //    Debug.Log("~~~~~~~~~~~~~~~~~ TH 3");
        //    DataNeeded defData = DefenceEnemy(enemyGreenList);
        //    if (defData != null) return defData;
        //    else
        //    {
        //        DataNeeded yellowData = CheckYellowList(allyYellowList);
        //        if (yellowData != null) return yellowData;
        //        else return CheckRedList(allyRedList);
        //    }
        //}

        ///*
        // * TH 4 : Green list enemy != null && green list ally != null
        // */
        //if (enemyGreenList.Count > 0 && allyGreenList.Count > 0)
        //{
        //    /////////////////////////////
        //    ///truoc khi def thi kiem tra xem co an duoc con co` do khong , an dc thi an me luon
        //    ////////////////////////////

        //    Debug.Log("~~~~~~~~~~~~~~~~~ TH 4");
        //    if (enemyGreenList[0].greenCells[0].score == 100)//chieu tuong{
        //    {
        //        //check 
        //        for (int i = 0; i < allyGreenList.Count; i++)
        //        {
        //            if (allyGreenList[i].greenCells.Count > 0)
        //            {
        //                for (int y = 0; y < allyGreenList[i].greenCells.Count; y++)
        //                {
        //                    if (allyGreenList[i].greenCells[y].cell.mCurrentPiece.mID == enemyGreenList[0].myPiece.mID)
        //                    {
        //                        data.cell = allyGreenList[i].greenCells[y].cell;
        //                        data.piece = allyGreenList[i].myPiece;
        //                        data.dir = allyGreenList[i].greenCells[y].dir;
        //                        data.mode = mode.MoveTeamandKill;
        //                        return data;
        //                    }
        //                }
        //            }
        //        }
        //        DataNeeded defData = DefenceEnemy(enemyGreenList);
        //        if (defData != null) return defData;
        //        else
        //        {
        //            DataNeeded yellowData = CheckYellowList(allyYellowList);
        //            if (yellowData != null) return yellowData;
        //            else return CheckRedList(allyRedList);
        //        }
        //    }
        //    else
        //    {
        //        DataNeeded greenData = CheckGreenListAlly(allyGreenList, allybestList);
        //        if (greenData != null) return greenData;
        //        else
        //        {
        //            if (allyYellowList.Count > 0)
        //            {
        //                DataNeeded yellowData = CheckYellowList(allyYellowList);
        //                if (yellowData != null) return yellowData;
        //                else return CheckRedList(allyRedList);
        //            }
        //            else
        //            {
        //                //red cell
        //                //check xem cell do co nam trong safe cell hay khong ? 
        //                return CheckRedList(allyRedList);
        //            }
        //        }
        //    }
        //}
        //return data;
    }
    private bool CheckProtectPiece(Cell targetCell)
    {
        for (int i = 0; i < allyList.Count; i++)
        {
            if(allyList[i].myPiece.kind == Kind.Normal)
              if (allyList[i].listCells.Contains(targetCell) )
                return true;
        }
        return false;
    }

    private DataNeeded MoveKingData(DataCell targetScript, Cell kingCell)
    {
        BasePieces targetPiece = (BasePieces)targetScript.piece.Clone();
        BasePieces kingPiece = (BasePieces)kingCell.mCurrentPiece.Clone();

        List<Cell> targetCells = new List<Cell>();
        List<Cell> kingCells = kingPiece.CheckPathingBot();

        if (targetPiece.mNamePiece == "Long" || targetPiece.mNamePiece == "Phuong" || targetPiece.mNamePiece == "Xa")
        {
            targetCells = targetPiece.GetListCellLongPhuongXa();

            for (int i = 0; i < targetCells.Count; i++)
            {
                if (kingCells.Contains(targetCells[i]))
                {
                    kingCells.Remove(targetCells[i]);
                }
            }
            if (kingCells.Count > 0)
            {
                List<DataNeeded> listData = new List<DataNeeded>();
                for (int i = 0; i < kingCells.Count; i++)
                {
                    if (CheckCellContainSafeCells(kingCells[i]))
                    {
                        //kiem tra o dang dung co' con co` nao khong
                        //khong co moi di chuyen duoc
                        if (!kingCells[i].ContainPiece())
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.MoveSingle;
                            data.piece = kingCell.mCurrentPiece;
                            data.cell = kingCells[i];
                            data.dir = kingCell.mCurrentPiece.MStateRotation;
                            listData.Add(data);
                        }
                    }
                }
                if (listData.Count > 0) return listData[UnityEngine.Random.Range(0, listData.Count)];
                return null;
            }
        }
        else
        {
            List<DataNeeded> listData = new List<DataNeeded>();
            for (int i = 0; i < kingCells.Count; i++)
            {
                if (CheckCellContainSafeCells(kingCells[i]))
                {
                    if (!kingCells[i].ContainPiece())
                    {
                        DataNeeded data = new DataNeeded();
                        data.mode = mode.MoveSingle;
                        data.piece = kingCell.mCurrentPiece;
                        data.cell = kingCells[i];
                        data.dir = kingCell.mCurrentPiece.MStateRotation;
                        listData.Add(data);
                    }
                }
            }
            if (listData.Count > 0) return listData[UnityEngine.Random.Range(0, listData.Count)];
            return null;
        }
        return null;
    }

    private DataNeeded KillThisPiece(Cell targetCell, List<DataCell> allyGreenList)
    {
        DataNeeded data = new DataNeeded();
        //Kiểm tra con cell đó có nằm trong ally green list hay không
        for (int i = 0; i < allyGreenList.Count; i++)
        {

            if (allyGreenList[i].cell.mId == targetCell.mId)
            {
                BasePieces basePieces = (BasePieces)allyGreenList[i].piece.Clone();
                basePieces.UpdateStatusPiece();
                if (CheckCellContainSafeCells(basePieces.mCurrentCell)) // nam trong safecell
                {
                    if (basePieces.canTeamUp) // kiem tra tiep con piece dang dung 1 minh hay dang team up
                    {
                        data.mode = mode.MoveTeamandKill;
                        data.piece = allyGreenList[i].piece;
                        data.cell = allyGreenList[i].cell;
                        data.dir = allyGreenList[i].dir;
                        return data;
                    }
                    else // team up here , random moveteam kill or move only kill
                    {
                        bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                        if (randomValue) //move team
                        {
                            data.mode = mode.MoveTeamandKill;
                            data.piece = allyGreenList[i].piece;
                            data.cell = allyGreenList[i].cell;
                            data.dir = allyGreenList[i].dir;
                            return data;
                        }
                        else
                        {
                            data.mode = mode.MoveSingleandKill;
                            data.piece = allyGreenList[i].piece;
                            data.cell = allyGreenList[i].cell;
                            data.dir = allyGreenList[i].dir;
                            return data;
                        }
                    }
                }
                else
                {
                    //khong nam trong safe cell nen phai move team
                    data.mode = mode.MoveTeamandKill;
                    data.piece = allyGreenList[i].piece;
                    data.cell = allyGreenList[i].cell;
                    data.dir = allyGreenList[i].dir;
                    return data;
                }
            }

        }

        return null;
    }

    private DataNeeded DefenceEnemy(DataCell enemyDataCell)
    {

        ///////////////////
        ///Defence enemy thì làm gì ? ,trường hợp này quân ta k có nước nào đi vào ô đó để bảo kê cả
        ///Nên đầu tiên sẽ check xem quân ta có quân kind == UI nào không => sắp xếp theo score
        ///Nếu có => check xem nước đi của quân đó
        ///Chạy dòng for những nước đi được của quân đó => kiểm tra path đi được cả 4 hướng có hướng nào bảo kê được con bị target hay không
        ///Nếu được lưu thông tin con cờ đó và return value
        ///Nếu không còn con cờ UI nào cả thì sẽ kiểm tra tiếp nước đi của quân địch => đặc biệt là loại long phượng xa => nếu ta di chuyển ô đó đi có bị chiếu k
        ///Nếu không thì tiến hành move con cờ đi
        ///Check xem trong path đi được có ô nào an toàn không => nếu có => di chuyển vào ô đó 
        List<Calculator> uipieces = new List<Calculator>();
        for (int i = 0; i < allyList.Count; i++)
        {
            if (allyList[i].myPiece.kind == Kind.UI)
            {
                uipieces.Add(allyList[i]);
            }
        }
        if (uipieces.Count > 0)
        {
            List<DataCell> dataUI = new List<DataCell>();
            for (int i = 0; i < uipieces.Count; i++)
            {
                BasePieces clone = (BasePieces)uipieces[i].myPiece.Clone();
                List<Cell> clonelistCell = clone.CheckPathingBot();
                for (int j = 0; j < clonelistCell.Count; j++)
                {
                    List<DataCell> data = uipieces[i].myPiece.CheckPathContainCell(clonelistCell[j], enemyDataCell.cell,enemyList);
                  if(data != null) if (data.Count > 0) dataUI.AddRange(data);
                }
            }
            if (dataUI.Count > 0)
            {
                dataUI.OrderByDescending(x => x.score);
                if (dataUI[0].cell.ContainPiece())
                {
                    DataNeeded returnData = new DataNeeded();
                    returnData.piece = dataUI[0].piece;
                    returnData.dir = dataUI[0].dir;
                    returnData.cell = dataUI[0].cell;
                    returnData.mode = mode.TeamUpFormStoreToCell;
                    return returnData;
                }
                else
                {
                    DataNeeded returnData = new DataNeeded();
                    returnData.piece = dataUI[0].piece;
                    returnData.dir = dataUI[0].dir;
                    returnData.cell = dataUI[0].cell;
                    returnData.mode = mode.MoveFromStoreToCell;
                    return returnData;
                }
            }
        }
        if (enemyDataCell.piece.mNamePiece == "Long" || enemyDataCell.piece.mNamePiece == "Phuong" || enemyDataCell.piece.mNamePiece == "Xa")
        {
            BasePieces targetPiece = (BasePieces)enemyDataCell.piece.Clone();
            List<Cell> targetCells = targetPiece.GetListCellLongPhuongXa();
            bool checkKing = false;
            for (int i = 0; i < targetCells.Count; i++)
            {
                if (targetCells[i].ContainPiece())
                {
                    if (targetCells[i].mCurrentPiece.mNamePiece == "Vuong" && targetCells[i].mCurrentPiece.mColor != enemyDataCell.piece.mColor)
                        checkKing = true;
                }
            }
            if (checkKing) return null;
        }

        BasePieces enemyPieces = (BasePieces)enemyDataCell.piece.Clone();

        BasePieces allyTargetPiece = (BasePieces)enemyDataCell.cell.mCurrentPiece.Clone();

        List<Cell> allyTargetCells = allyTargetPiece.CheckPathingBot();

        List<DataNeeded> listData = new List<DataNeeded>();
        for (int i = 0; i < allyTargetCells.Count; i++)
        {
            if (CheckCellContainSafeCells(allyTargetCells[i]))
            {
                if (allyTargetPiece.canTeamUp) // con piece hien tai chi co 1 minh
                {
                    if (allyTargetCells[i].ContainPiece())// team up here
                    {
                        DataNeeded data = new DataNeeded();
                        data.mode = mode.TeamUp;
                        data.piece = enemyDataCell.cell.mCurrentPiece;
                        data.cell = allyTargetCells[i];
                        data.dir = enemyDataCell.cell.mCurrentPiece.MStateRotation;
                        listData.Add(data);
                    }
                    else
                    {
                        DataNeeded data = new DataNeeded();
                        data.mode = mode.MoveSingle;
                        data.piece = enemyDataCell.cell.mCurrentPiece;
                        data.cell = allyTargetCells[i];
                        data.dir = enemyDataCell.cell.mCurrentPiece.MStateRotation;
                        listData.Add(data);
                    }
                }
                else// can't team up , check free cell thi move
                {

                        if (!allyTargetCells[i].ContainPiece())
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.MoveTeam;
                        data.piece = enemyDataCell.cell.mCurrentPiece;
                        data.cell = allyTargetCells[i];
                        data.dir = enemyDataCell.cell.mCurrentPiece.MStateRotation;
                        listData.Add(data);
                        }
                }

            }
        }

        if (listData.Count > 0) return listData[UnityEngine.Random.Range(0, listData.Count)];
        return null;
        #region 
        //for (int i = 0; i < enemyGreenList.Count; i++)
        //{
        //    List<DataNeeded> listData = new List<DataNeeded>();
        //    for (int y = 0; y < enemyGreenList[i].greenCells.Count; y++)
        //    {
        //        BasePieces basePieces = (BasePieces)enemyGreenList[i].greenCells[y].cell.mCurrentPiece.Clone();
        //        basePieces.UpdateStatusPiece();
        //        List<Cell> checkCells = basePieces.CheckPathingBot();
        //        for (int z = 0; z < checkCells.Count; z++)
        //        {
        //            if (CheckCellContainSafeCells(checkCells[z]))
        //            {
        //                if (basePieces.canTeamUp) // con piece hien tai chi co 1 minh
        //                {
        //                    if (checkCells[i].ContainPiece())// team up here
        //                    {
        //                        DataNeeded data = new DataNeeded();
        //                        data.mode = mode.TeamUp;
        //                        data.piece = enemyGreenList[i].greenCells[y].cell.mCurrentPiece;
        //                        data.cell = checkCells[z];
        //                        data.dir = enemyGreenList[i].greenCells[y].cell.mCurrentPiece.MStateRotation;
        //                        listData.Add(data);
        //                    }
        //                    else//cell check not contain piece
        //                    {
        //                        DataNeeded data = new DataNeeded();
        //                        data.mode = mode.MoveSingle;
        //                        data.piece = enemyGreenList[i].greenCells[y].cell.mCurrentPiece;
        //                        data.cell = checkCells[z];
        //                        data.dir = enemyGreenList[i].greenCells[y].cell.mCurrentPiece.MStateRotation;
        //                        listData.Add(data);
        //                    }
        //                }
        //                else// can't team up , check free cell thi move
        //                {
        //                    if (!checkCells[i].ContainPiece())
        //                    {
        //                        DataNeeded data = new DataNeeded();
        //                        data.mode = mode.MoveTeam;
        //                        data.piece = enemyGreenList[i].greenCells[y].cell.mCurrentPiece;
        //                        data.cell = checkCells[z];
        //                        data.dir = enemyGreenList[i].greenCells[y].cell.mCurrentPiece.MStateRotation;
        //                        listData.Add(data);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    if (listData.Count > 0)
        //        return listData[UnityEngine.Random.Range(0, listData.Count)];
        //}
        //return null;
        #endregion
    }
    private DataNeeded CheckGreenListAlly(List<Calculator> allyGreenList, List<Calculator> bestList)
    {
        DataNeeded data = new DataNeeded();

        for (int i = 0; i < bestList.Count; i++)
        {
            for (int y = 0; y < bestList[i].bestCells.Count; y++)
            {
                if (CheckCellContainSafeCells(bestList[i].bestCells[y]))// in safecell
                {
                    if (bestList[i].myPiece.canTeamUp) // kiem tra tiep con piece dang dung 1 minh hay dang team up
                    {
                        data.mode = mode.MoveTeamandKill;
                        data.piece = bestList[i].myPiece;
                        data.cell = bestList[i].bestCells[y].cell;
                        data.dir = bestList[i].bestCells[y].dir;
                        return data;
                    }
                    else // team up here , random moveteam kill or move only kill
                    {
                        bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                        if (randomValue) //move team
                        {
                            data.mode = mode.MoveTeamandKill;
                            data.piece = bestList[i].myPiece;
                            data.cell = bestList[i].bestCells[y].cell;
                            data.dir = bestList[i].bestCells[y].dir;
                            return data;
                        }
                        else
                        {
                            data.mode = mode.MoveSingleandKill;
                            data.piece = bestList[i].myPiece;
                            data.cell = bestList[i].bestCells[y].cell;
                            data.dir = bestList[i].bestCells[y].dir;
                            return data;
                        }
                    }
                }
            }
        }
        for (int i = 0; i < allyGreenList.Count; i++)
        {
            for (int y = 0; y < allyGreenList[i].greenCells.Count; y++)
            {
                if (CheckCellContainSafeCells(allyGreenList[i].greenCells[y]))// in safecell
                {
                    if (allyGreenList[i].myPiece.canTeamUp) // kiem tra tiep con piece dang dung 1 minh hay dang team up
                    {
                        data.mode = mode.MoveTeamandKill;
                        data.piece = allyGreenList[i].myPiece;
                        data.cell = allyGreenList[i].greenCells[y].cell;
                        data.dir = allyGreenList[i].greenCells[y].dir;
                        return data;
                    }
                    else // team up here , random moveteam kill or move only kill
                    {
                        bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                        if (randomValue) //move team
                        {
                            data.mode = mode.MoveTeamandKill;
                            data.piece = allyGreenList[i].myPiece;
                            data.cell = allyGreenList[i].greenCells[y].cell;
                            data.dir = allyGreenList[i].greenCells[y].dir;
                            return data;
                        }
                        else
                        {
                            data.mode = mode.MoveSingleandKill;
                            data.piece = allyGreenList[i].myPiece;
                            data.cell = allyGreenList[i].greenCells[y].cell;
                            data.dir = allyGreenList[i].greenCells[y].dir;
                            return data;
                        }
                    }
                }
            }
        }

        data = null;
        return data;
    }

    private DataNeeded CheckRedList(List<DataCell> allyRedList)
    {
        List<DataNeeded> dataList = new List<DataNeeded>();
        allyRedList.Reverse();
        for (int i = 0; i < allyRedList.Count; i++)
        {

            if (CheckCellContainSafeCells(allyRedList[i]))
            {
                if (allyRedList[i].piece.canTeamUp) // con piece hien tai chi co 1 minh
                {
                    if (allyRedList[i].piece.kind == Kind.Normal)
                    {
                        if (allyRedList[i].cell.ContainPiece())// team up here
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.TeamUp;
                            data.piece = allyRedList[i].piece;
                            data.cell = allyRedList[i].cell;
                            data.dir = allyRedList[i].dir;
                            dataList.Add(data);
                        }
                        else
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.MoveSingle;
                            data.piece = allyRedList[i].piece;
                            data.cell = allyRedList[i].cell;
                            data.dir = allyRedList[i].dir;
                            dataList.Add(data);
                        }
                    }
                    else
                    {
                        if (allyRedList[i].cell.ContainPiece())// team up here
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.TeamUpFormStoreToCell;
                            data.piece = allyRedList[i].piece;
                            data.cell = allyRedList[i].cell;
                            data.dir = allyRedList[i].dir;
                            dataList.Add(data);
                        }
                        else
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.MoveFromStoreToCell;
                            data.piece = allyRedList[i].piece;
                            data.cell = allyRedList[i].cell;
                            data.dir = allyRedList[i].dir;
                            dataList.Add(data);
                        }
                    }
                }
                else// can't team up , check free cell thi move
                {
                    if (allyRedList[i].piece.kind == Kind.Normal)
                    {
                        bool randomValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
                        if (randomValue) //move team
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.MoveTeam;
                            data.piece = allyRedList[i].piece;
                            data.cell = allyRedList[i].cell;
                            data.dir = allyRedList[i].dir;
                            dataList.Add(data);
                        }
                        else //move only in team
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.MoveSingleinTeam;
                            data.piece = allyRedList[i].piece;
                            data.cell = allyRedList[i].cell;
                            data.dir = allyRedList[i].dir;
                            dataList.Add(data);
                        }
                    }
                    else
                    {
                        if (!allyRedList[i].cell.ContainPiece())//free cell
                        {
                            DataNeeded data = new DataNeeded();
                            data.mode = mode.MoveFromStoreToCell;
                            data.piece = allyRedList[i].piece;
                            data.cell = allyRedList[i].cell;
                            data.dir = allyRedList[i].dir;
                            dataList.Add(data);
                        }
                    }
                }
            }

        }
        if (dataList.Count > 0) return dataList[UnityEngine.Random.Range(0, dataList.Count)];
        return null;
    }

    private DataNeeded CheckYellowList(List<DataCell> allyYellowList)
    {
        List<DataNeeded> listData = new List<DataNeeded>();
        //for (int u = 0; u < enemyList.Count; u++)
        //{
        //    for (int m = 0; m < allyYellowList.Count; m++)
        //    {
        //        if (enemyList[u].listCells.Contains(allyYellowList[m].cell))
        //        {
        //            allyYellowList[m].canUse = false;
        //        }
        //    }
        //}
        for (int i = 0; i < allyYellowList.Count; i++)
        {
            //if (allyYellowList[i].canUse)
            //{
                if (CheckCellContainSafeCells(allyYellowList[i])) // best score and in safe cell
                {
                    if (allyYellowList[i].piece.canTeamUp) // con piece hien tai chi co 1 minh
                    {
                        if (allyYellowList[i].piece.kind == Kind.Normal)// NORMAL PIECE
                        {
                            if (allyYellowList[i].cell.ContainPiece())// team up here
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.TeamUp;
                                data.piece = allyYellowList[i].piece;
                                data.cell = allyYellowList[i].cell;
                                data.dir = allyYellowList[i].dir;
                                listData.Add(data);
                            }
                            else
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.MoveSingle;
                                data.piece = allyYellowList[i].piece;
                                data.cell = allyYellowList[i].cell;
                                data.dir = allyYellowList[i].dir;
                                listData.Add(data);
                            }
                        }
                        else //UI PIECE
                        {
                            if (allyYellowList[i].cell.ContainPiece())// team up here
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.TeamUpFormStoreToCell;
                                data.piece = allyYellowList[i].piece;
                                data.cell = allyYellowList[i].cell;
                                data.dir = allyYellowList[i].dir;
                                listData.Add(data);
                            }
                            else
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.MoveFromStoreToCell;
                                data.piece = allyYellowList[i].piece;
                                data.cell = allyYellowList[i].cell;
                                data.dir = allyYellowList[i].dir;
                                listData.Add(data);
                            }
                        }
                    }
                    else// can't team up , check free cell thi move
                    {
                        if (allyYellowList[i].piece.kind == Kind.Normal)// NORMAL PIECE
                        {
                            if (!allyYellowList[i].cell.ContainPiece())
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.MoveTeam;
                                data.piece = allyYellowList[i].piece;
                                data.cell = allyYellowList[i].cell;
                                data.dir = allyYellowList[i].dir;
                                listData.Add(data);
                            }
                        }
                        else
                        {
                            if (!allyYellowList[i].cell.ContainPiece())
                            {
                                DataNeeded data = new DataNeeded();
                                data.mode = mode.MoveFromStoreToCell;
                                data.piece = allyYellowList[i].piece;
                                data.cell = allyYellowList[i].cell;
                                data.dir = allyYellowList[i].dir;
                                listData.Add(data);
                            }
                        }
                    }
                }
           // }
        }
        if (listData.Count > 1) return listData[UnityEngine.Random.Range(0, listData.Count)];
        return null;
        //int maxScore = allyYellowList[0]..score;
        //List<Calculator> maxList = new List<Calculator>();
        //for (int i = 0; i < allyYellowList.Count; i++)
        //{
        //    if (maxScore >= allyYellowList[i].yellowCells[0].score)
        //    {
        //        maxList.Add(allyYellowList[i]);
        //    }
        //}

        /*
        if (maxList.Count > 0)
        {
            maxList.Shuffle(new System.Random());
            for (int i = 0; i < maxList.Count; i++)
            {
                if (maxList[i].yellowCells.Count > 0)
                {
                    for (int y = 0; y < maxList[i].yellowCells.Count; y++)
                    {
                        if (CheckCellContainSafeCells(maxList[i].yellowCells[y])) // best score and in safe cell
                        {
                            if (maxList[i].myPiece.canTeamUp) // con piece hien tai chi co 1 minh
                            {
                                if (maxList[i].myPiece.kind == Kind.Normal)// NORMAL PIECE
                                {
                                    if (maxList[i].yellowCells[y].cell.ContainPiece())// team up here
                                    {
                                        data.mode = mode.TeamUp;
                                        data.piece = maxList[i].myPiece;
                                        data.cell = maxList[i].yellowCells[y].cell;
                                        data.dir = maxList[i].yellowCells[y].dir;
                                        return data;
                                    }
                                    else
                                    {
                                        data.mode = mode.MoveSingle;
                                        data.piece = maxList[i].myPiece;
                                        data.cell = maxList[i].yellowCells[y].cell;
                                        data.dir = maxList[i].yellowCells[y].dir;
                                        return data;
                                    }
                                }
                                else //UI PIECE
                                {
                                    if (maxList[i].yellowCells[y].cell.ContainPiece())// team up here
                                    {
                                        data.mode = mode.TeamUpFormStoreToCell;
                                        data.piece = maxList[i].myPiece;
                                        data.cell = maxList[i].yellowCells[y].cell;
                                        data.dir = maxList[i].yellowCells[y].dir;
                                        return data;
                                    }
                                    else
                                    {
                                        data.mode = mode.MoveFromStoreToCell;
                                        data.piece = maxList[i].myPiece;
                                        data.cell = maxList[i].yellowCells[y].cell;
                                        data.dir = maxList[i].yellowCells[y].dir;
                                        return data;
                                    }
                                }
                            }
                            else// can't team up , check free cell thi move
                            {
                                if (maxList[i].myPiece.kind == Kind.Normal)// NORMAL PIECE
                                {
                                    if (!maxList[i].yellowCells[y].cell.ContainPiece())
                                    {
                                        data.mode = mode.MoveTeam;
                                        data.piece = maxList[i].myPiece;
                                        data.cell = maxList[i].yellowCells[y].cell;
                                        data.dir = maxList[i].yellowCells[y].dir;
                                        return data;
                                    }
                                }
                                else
                                {
                                    if (!maxList[i].yellowCells[y].cell.ContainPiece())
                                    {
                                        data.mode = mode.MoveFromStoreToCell;
                                        data.piece = maxList[i].myPiece;
                                        data.cell = maxList[i].yellowCells[y].cell;
                                        data.dir = maxList[i].yellowCells[y].dir;
                                        return data;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        for (int i = 0; i < allyYellowList.Count; i++)
        {
            for (int y = 0; y < allyYellowList[i].yellowCells.Count; y++)
            {
                if (CheckCellContainSafeCells(allyYellowList[i].yellowCells[y])) // best score and in safe cell
                {
                    if (allyYellowList[i].myPiece.canTeamUp) // con piece hien tai chi co 1 minh
                    {
                        if (allyYellowList[i].myPiece.kind == Kind.Normal)// NORMAL PIECE
                        {
                            if (allyYellowList[i].yellowCells[y].cell.ContainPiece())// team up here
                            {
                                data.mode = mode.TeamUp;
                                data.piece = allyYellowList[i].myPiece;
                                data.cell = allyYellowList[i].yellowCells[y].cell;
                                data.dir = allyYellowList[i].yellowCells[y].dir;
                                return data;
                            }
                            else
                            {
                                data.mode = mode.MoveSingle;
                                data.piece = allyYellowList[i].myPiece;
                                data.cell = allyYellowList[i].yellowCells[y].cell;
                                data.dir = allyYellowList[i].yellowCells[y].dir;
                                return data;
                            }
                        }
                        else //UI PIECE
                        {
                            if (allyYellowList[i].yellowCells[y].cell.ContainPiece())// team up here
                            {
                                data.mode = mode.TeamUpFormStoreToCell;
                                data.piece = allyYellowList[i].myPiece;
                                data.cell = allyYellowList[i].yellowCells[y].cell;
                                data.dir = allyYellowList[i].yellowCells[y].dir;
                                return data;
                            }
                            else
                            {
                                data.mode = mode.MoveFromStoreToCell;
                                data.piece = allyYellowList[i].myPiece;
                                data.cell = allyYellowList[i].yellowCells[y].cell;
                                data.dir = allyYellowList[i].yellowCells[y].dir;
                                return data;
                            }
                        }
                    }
                    else// can't team up , check free cell thi move
                    {
                        if (allyYellowList[i].myPiece.kind == Kind.Normal)// NORMAL PIECE
                        {
                            if (!allyYellowList[i].yellowCells[y].cell.ContainPiece())
                            {
                                data.mode = mode.MoveTeam;
                                data.piece = allyYellowList[i].myPiece;
                                data.cell = allyYellowList[i].yellowCells[y].cell;
                                data.dir = allyYellowList[i].yellowCells[y].dir;
                                return data;
                            }
                        }
                        else
                        {
                            if (!allyYellowList[i].yellowCells[y].cell.ContainPiece())
                            {
                                data.mode = mode.MoveFromStoreToCell;
                                data.piece = allyYellowList[i].myPiece;
                                data.cell = allyYellowList[i].yellowCells[y].cell;
                                data.dir = allyYellowList[i].yellowCells[y].dir;
                                return data;
                            }
                        }
                    }
                }
            }
        }
        data = null;
        return data;
        */
    }

    private bool CheckCellContainSafeCells(DataCell dataCell)
    {
        bool returnValue = false;
        if (safecells.Count > 0)
        {
            returnValue = safecells.Contains(dataCell.cell);
        }
        return returnValue;
    }
    private bool CheckCellContainSafeCells(Cell dataCell)
    {
        bool returnValue = false;
        if (safecells.Count > 0)
        {
            returnValue = safecells.Contains(dataCell);
        }
        return returnValue;
    }

    IEnumerator WaitForEndOfFrames()
    {
        yield return new WaitForSeconds(0.1f);
    }
    IEnumerator Waiting(Calculator calculator)
    {
        yield return new WaitForEndOfFrame();
        calculator.Calculate();
    }
    private IEnumerator Call(List<Calculator> targetList)
    {
        for (int i = 0; i < targetList.Count; i++)
        {
            yield return StartCoroutine(Waiting(targetList[i]));
        }

    }

    public void GetAllCell()
    {
        safecells.Clear();
        safecells = parentCells.GetAllCell();
    }
    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.F1))
    //    {
    //        Debug.Log("Call =>");
    //        StartCoroutine(CallCaculate());
    //    }
    //}
}
