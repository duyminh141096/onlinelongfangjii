/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: BotManager.cs
* Script Author: MinhLe 
* Created On: 7/20/2020 9:54:48 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BotManager : MonoBehaviour
{
    #region Normal Variables
    [Header("CLASS VARIABLES")]
    public ControllerBot controllerBot;
    public SetupLongPhuong setupLongPhuong;
    #endregion
    #region UI Variables 
    [Header("PANEL CHOOSE TEAM")]
    public GameObject panelChooseTeam;
    public Button greenTeamBtn;
    public Button orangeTeamBtn;
    #endregion

    #region Init Event Button
    private void Init()
    {
        panelChooseTeam.SetActive(false);
        greenTeamBtn.onClick.AddListener(() => {
            bool color = true;
            ControllerBot.teamColor = color;
            panelChooseTeam.SetActive(false);
            setupLongPhuong.SwitchSide(true, true);
        });
        orangeTeamBtn.onClick.AddListener(() => {
            bool color = false;
            ControllerBot.teamColor = color;
            panelChooseTeam.SetActive(false);
            setupLongPhuong.SwitchSide(true, true);
        });
    }
    #endregion
    private void Awake()
    {
        Init();
    }
}
