using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Minimax : MonoBehaviour
{
    public delegate List<DataPiece> Calculate();
    public static Calculate OnCalculate;

    public delegate int ScorePiece();
    public static ScorePiece ReturnScorePiece;

    public ControllerBot ControllerBot;
    private void Start()
    {
        Init();
    }
    public void Update() 
    {
    //    if (Input.GetKeyDown(KeyCode.P))
    //    {
    //        Move1(0, true, true);
    //    }
    //    if (Input.GetKeyDown(KeyCode.Q))
    //    {
    //        Move1(0, false, false);
    //    }
    //    if (Input.GetKeyDown(KeyCode.O))
    //    {
    //        Move1(1, true, true);
    //    }
    //    if (Input.GetKeyDown(KeyCode.I))
    //    {
    //        Move1(2, true, true);//best choice
    //    }
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    listData.Clear();
        //    listData = UglyMove(false);
        //}
        //if (Input.GetKeyDown(KeyCode.N))
        //{
        //    listData.Clear();
        //    listData = UglyMove(true);

        //}
        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    listData.Clear();
        //    listData = UglyMoveForTestUIPiece(true);
        //}
        //if (Input.GetKeyDown(KeyCode.K))
        //{
        //    listData.Clear();
        //    listData = UglyMoveForTestUIPiece(false);
        //}
    }
   // public Cell currentCell;
   // public Cell targetCell;
   // public DataPiece data;
    //public List<DataPiece> listData = new List<DataPiece>();
   // public List<DataPiece> totalData = new List<DataPiece>();
    public void ClearCell()
    {
        //if (currentCell != null) currentCell.SetColor(Color.white);
        //if (targetCell != null) targetCell.SetColor(Color.white);
        //targetCell = null;
        //currentCell = null;
    }
    #region ....
    /*
    public DataPiece TheBestMove()
    {
        bool botTeam = true;
        ClearCell();
        List<DataPiece> listDataOrange = UglyMove(true);
        List<DataPiece> listDataGreen = UglyMove(false);

        List<DataPiece> allmove = new List<DataPiece>();
        allmove.AddRange(listDataOrange);
        allmove.AddRange(listDataGreen);
        allmove = allmove.OrderBy(x => x.score).ToList();

        for (int i = 0; i < allmove.Count; i++)
        {
            if(allmove[i].targetPiece.mColor == botTeam)
            {
                //bot move
                if (allmove[i].targetCell.ContainPiece())
                {
                    if(allmove[i].targetCell.mCurrentPiece.mColor == botTeam)//move into ally piece or rotate
                    {
                        if (CheckSafeformove(allmove[i], botTeam)) return allmove[i]; 
                    }
                    else
                    {
                        //attack
                        if (CheckKing(allmove[i])) return allmove[i]; //check king
                        if (CheckSafeforAttack(allmove[i], botTeam ? listDataGreen : listDataOrange)) return allmove[i];
                    }
                }
                else
                {
                    if(CheckSafeformove(allmove[i], botTeam))  return allmove[i]; 
                }
            }
            else
            {
                //user move
                if (allmove[i].targetCell.ContainPiece())
                {
                    if (allmove[i].targetCell.mCurrentPiece.mColor == botTeam)//move into ally piece or rotate
                    {
                        if (CheckSafefordefence(allmove[i], botTeam ? listDataGreen : listDataOrange)) return allmove[i];
                    }
                }
            }
        }
        return StupidMove(allmove, botTeam );
    }

    private bool CheckSafefordefence(DataPiece dataPiece, List<DataPiece> usermoves)
    {
        
    }
    private DataPiece StupidMove(List<DataPiece> allmove, bool botTeam)
    {
        
    }
    private bool CheckSafeformove(DataPiece dataPiece, bool botTeam)
    {
        Cell currentCell = dataPiece.targetPiece.mCurrentCell;

        currentCell.RemovePiece();
        dataPiece.targetPiece.mCurrentCell = dataPiece.targetCell;
        bool notContainPiece = false;
        if (!targetCell.ContainPiece())
        {
            notContainPiece = true;
            dataPiece.targetCell.mCurrentPiece = dataPiece.targetPiece;
        }

        bool foundNotSafe = false;
        List<DataPiece> userMoves = UglyMove(!botTeam);
        foreach (var item in userMoves)
        {
            if(item.targetCell == dataPiece.targetCell)
            {
                foundNotSafe = true;
                break;
            }
        }

        //reset
        currentCell.mCurrentPiece = dataPiece.targetPiece;
        dataPiece.targetPiece.mCurrentCell = currentCell;

        if (notContainPiece)
            dataPiece.targetCell.RemovePiece();


        return foundNotSafe;
    }
    private bool CheckKing(DataPiece data )
    {
        bool foundKing = false;
        if (data.targetCell.mCurrentPiece.mNamePiece == "Vuong")
            return foundKing = true;
        return foundKing;
    }
    private bool CheckSafeforAttack(DataPiece data , List<DataPiece> usermoves)
    {
        Cell currentCell = data.targetPiece.mCurrentCell;

        BasePieces targetPiece = data.targetCell.mCurrentPiece;

        //move piece into target cell => remove 

    }
    */
    #endregion
    public void Move1(int dept, bool teamColor, bool myTeamColor)
    {
        //ClearCell();
        List<DataPiece> listData = UglyMove(teamColor);
        //yield return new WaitForEndOfFrame();
        List<DataPiece> total = new List<DataPiece>();
        int bestMoveMax = -9999;

        for (int i = 0; i < listData.Count; i++)
        {
            UglyMove(listData[i]);
            int value = Move2(dept, !teamColor, myTeamColor, -10000, 10000);
            UndoAction();
            //Debug.Log("Nuoc di thu : " + i + " ---- Value : " + value + " --- Best move : " + bestMoveMax);
            //undo
            if (value > bestMoveMax)
            {
                total.Clear();
                bestMoveMax = value;
                total.Add(listData[i]);
            }
            else if (value == bestMoveMax)
            {
                total.Add(listData[i]);
            }
            //yield return new WaitForEndOfFrame();
        }
//totalData = total;
        DataPiece returnData = TheBestChoice(total);
        //currentCell = returnData.targetPiece.mCurrentCell;
        //targetCell = returnData.targetCell;
        DataNeeded dataNeed = new DataNeeded();
        dataNeed.piece = returnData.targetPiece;
        dataNeed.cell = returnData.targetCell;
        dataNeed.dir = returnData.targetDir;
        dataNeed.mode = returnData.mode;
        ControllerBot.BotAction(dataNeed);
    }
    private DataPiece TheBestChoice(List<DataPiece> data)
    {
        List<DataPiece> dataValue = new List<DataPiece>();
        if (data.Count == 1) return data[0];
        DataPiece dataReturn = data[0];
        dataValue.Add(dataReturn);
        for (int i = 1; i < data.Count; i++)
        {
            if (data[i].score == dataReturn.score)
            {
                dataValue.Add(data[i]);
            }
            else break;
        }
        int index = 0;
        
        if(dataValue[0].score == 1)
        {
            List<DataPiece> indexs = new List<DataPiece>();
            for (int i = 0; i < dataValue.Count; i++)
            {
                if(dataValue[i].mode == mode.RotationPiece)
                {
                    indexs.Add(dataValue[i]);
                }
            }
            for (int i = 0; i < indexs.Count; i++)
            {
                dataValue.Remove(indexs[i]);
            }
            index = UnityEngine.Random.Range(0, dataValue.Count);
        }
        else
        {
            index = UnityEngine.Random.Range(0, dataValue.Count);
        }
        return dataValue[index];
    }

    private int Move2(int dept, bool teamColor, bool myTeamColor, int alpha, int beta)
    {
        if (dept == 0)
        {
            return -EvaluateBoard();
        }
        List<DataPiece> newGameMoves = UglyMove(teamColor);
        if (teamColor == myTeamColor)
        {
            int bestMove = -9999;

            for (int i = 0; i < newGameMoves.Count; i++)
            {
                //move the pieces => get action
                UglyMove(newGameMoves[i]);
                bestMove = Math.Max(bestMove, Move2(dept - 1, !teamColor, myTeamColor, alpha, beta));
                Debug.Log("CURRENT >>> : " + bestMove);
                UndoAction();
                alpha = Math.Max(alpha, bestMove);
                if (beta <= alpha)
                    return bestMove;
                //undo
            }
            return bestMove;
        }
        else
        {
            int bestMove = 9999;
            for (int i = 0; i < newGameMoves.Count; i++)
            {
                //move the pieces => get action
                UglyMove(newGameMoves[i]);
                bestMove = Math.Min(bestMove, Move2(dept - 1, !teamColor, myTeamColor, alpha, beta));
                Debug.Log("TARGET >>>>> : " + bestMove);
                UndoAction();
                beta = Math.Min(beta, bestMove);//10000 , -13
                if (beta <= alpha)
                    return bestMove;
                //undo
            }
            return bestMove;
        }
    }
    private void Minimaxroot(int dept, bool teamColor)
    {
        Debug.Log("Start MiniMax");
        ClearCell();
        List<DataPiece> listData = UglyMove(teamColor);
        int bestMove = -9999;
        DataPiece bestMoveFound = null;

        for (int i = 0; i < listData.Count; i++)
        {
            //move the pieces => get action
            UglyMove(listData[i]);
            int value = MinimaxFunc(dept, !teamColor);
            UndoAction();
            //undo
            if (value >= bestMove)
            {
                bestMove = value;
                bestMoveFound.SetValue(listData[i].targetPiece, listData[i].targetCell, listData[i].targetDir, listData[i].type);
            }
        }
        Debug.Log("End MiniMax");
        Debug.Log("Dir : " + bestMoveFound.targetDir);

    }

    /*
        lay tat cac nuoc di    

     */
    private int MinimaxFunc(int dept, bool isMaximisingPlayer)
    {
        if (dept == 0)
        {
            return -EvaluateBoard();
        }
        List<DataPiece> newGameMoves = UglyMove(isMaximisingPlayer);

        if (isMaximisingPlayer)
        {
            int bestMove = -9999;
            for (int i = 0; i < newGameMoves.Count; i++)
            {
                //move the pieces => get action
                UglyMove(newGameMoves[i]);
                bestMove = Math.Max(bestMove, MinimaxFunc(dept - 1, !isMaximisingPlayer));
                UndoAction();
                //undo
            }
            return bestMove;
        }
        else
        {
            int bestMove = -9999;
            for (int i = 0; i < newGameMoves.Count; i++)
            {
                //move the pieces => get action
                UglyMove(newGameMoves[i]);
                bestMove = Math.Min(bestMove, MinimaxFunc(dept - 1, !isMaximisingPlayer));
                UndoAction();
                //undo
            }
            return bestMove;
        }
    }

    private void UndoAction()
    {
        ControllerBot.UndoMini();
    }
    private void ActionCalculated(DataPiece data, mode mode)
    {
        DataNeeded dataNeed = new DataNeeded();
        dataNeed.piece = data.targetPiece;
        dataNeed.cell = data.targetCell;
        dataNeed.mode = mode;
        dataNeed.dir = data.targetDir;
        data.mode = mode;
        ControllerBot.BotActionForMinimax(dataNeed);
    }
    public void UglyMove(DataPiece data)
    {
        //check rotation => targetCell == currentCell && dir thay doi

        /* Case Rotation */
        if (data.targetPiece.mCurrentCell == data.targetCell && data.targetPiece.MStateRotation != data.targetDir)
        {
            ActionCalculated(data, mode.RotationPiece);
            return;
        }
        else

        /* Case Move single */
        if (data.targetPiece.mListPiece.GetListNameCount() == 1 && !data.targetCell.ContainPiece() && data.targetPiece.kind == Kind.Normal)
        {
            ActionCalculated(data, mode.MoveSingle);
            return;
        }

        else
        /*  Case Move single in Team ~~~ move team  */
        if (data.targetPiece.mListPiece.GetListNameCount() > 1 && !data.targetCell.ContainPiece() && data.targetPiece.kind == Kind.Normal)
        {
            ActionCalculated(data, data.type == type.single ? mode.MoveSingleinTeam : mode.MoveTeam);
            return;
        }
        else
        /* ~~~  Move single and kill ~~ move team and kill  */
        if (data.targetPiece.mListPiece.GetListNameCount() > 1 && data.targetCell.ContainPiece() && data.targetPiece.kind == Kind.Normal)
        {
            if (data.targetPiece.mColor != data.targetCell.mCurrentPiece.mColor)
            {
                ActionCalculated(data, data.type == type.single ? mode.MoveSingleandKill : mode.MoveTeamandKill);
                return;
                //kill
            }
        }
        else
        if (data.targetPiece.mListPiece.GetListNameCount() == 1 && data.targetCell.ContainPiece() && data.targetPiece.kind == Kind.Normal)
        {
            if (data.targetPiece.mColor != data.targetCell.mCurrentPiece.mColor)
            {
                ActionCalculated(data, mode.MoveTeamandKill);
                return;
                //kill
            }
            else
            {
                ActionCalculated(data, mode.TeamUp);
                return;
            }
        }
        else

        /* MoveFromStoreToCell and TeamUpFormStoreToCell */
        if (data.targetPiece.kind == Kind.UI)
        {
            if (data.targetCell.ContainPiece())
            {
                ActionCalculated(data, mode.TeamUpFormStoreToCell);
                return;
            }
            else
            {
                ActionCalculated(data, mode.MoveFromStoreToCell);
                return;
            }
        }
    }
    public GameObject ParentNormal;
    public GameObject ParentUIOrange;
    public GameObject ParentUIGreen;

    public Dictionary<int, GameObject> parent = new Dictionary<int, GameObject>();
    private void Init()
    {
        parent = new Dictionary<int, GameObject>();
        parent.Add(0, ParentNormal);
        parent.Add(1, ParentUIOrange);
        parent.Add(2, ParentUIGreen);
    }
    public List<GameObject> GetPiecesUI(bool teamColor, bool getall = false)
    {
        List<GameObject> GOs = new List<GameObject>();
        List<GameObject> child = teamColor ? parent[1].GetAllChild() : parent[2].GetAllChild();
        for (int i = 0; i < child.Count; i++)
        {
            calTest data = child[i].GetComponent<calTest>();
            BasePieces basePieces = child[i].GetComponent<BasePieces>();
            if (data && basePieces)
            {
                Store store = StorePieces.Instance.GetInfo(basePieces.mNamePiece, basePieces.mColor);
                if (store.count > 0)
                {
                    GOs.Add(child[i]);
                }
            }
        }
        return GOs;
    }
    public List<GameObject> GetPieces(bool teamColor, bool getall = false)
    {
        List<GameObject> GOs = new List<GameObject>();
        #region Normal
        List<GameObject> allchild = parent[0].GetAllChild();
        for (int i = 0; i < allchild.Count; i++)
        {
            calTest data = allchild[i].GetComponent<calTest>();
            BasePieces basePieces = allchild[i].GetComponent<BasePieces>();
            if (data && basePieces)
            {
                if (getall)
                {
                    GOs.Add(allchild[i]);
                    continue;
                }
                if (teamColor == basePieces.mColor && !getall)
                {
                    GOs.Add(allchild[i]);
                    continue;
                }

            }
        }
        if (getall) return GOs;
        List<GameObject> child = teamColor ? parent[1].GetAllChild() : parent[2].GetAllChild();
        for (int i = 0; i < child.Count; i++)
        {
            calTest data = child[i].GetComponent<calTest>();
            BasePieces basePieces = child[i].GetComponent<BasePieces>();
            if (data && basePieces)
            {
                Store store = StorePieces.Instance.GetInfo(basePieces.mNamePiece, basePieces.mColor);
                if (store.count > 0)
                {
                    GOs.Add(child[i]);
                }
            }
        }
        return GOs;
        #endregion
    }
    private List<DataPiece> UglyMoveForTestUIPiece(bool teamColor)
    {
        //get all piece in normal,ui can move
        List<GameObject> GOs = GetPiecesUI(teamColor);
        List<DataPiece> listData = new List<DataPiece>();
        for (int i = 0; i < GOs.Count; i++)
        {
            Calculator method = GOs[i].GetComponent<calTest>().calculator;
            List<DataPiece> returnValue = method.Calculatev2();
            if (returnValue == null || returnValue.Count == 0) continue;

            listData.AddRange(returnValue);
        }
        return listData;
        /*
        List<DataPiece> listData = new List<DataPiece>();
        var list = OnCalculate.GetInvocationList();
        Debug.Log("count : " + list.Length);
        int count = 0;
        foreach (var item in list)
        {
            Calculate method = (Calculate)item;
            List<DataPiece> returnValue = method();

            if (returnValue == null || returnValue.Count == 0) continue;
            count++;
            listData.AddRange(returnValue);
        }
        return listData;
        */
    }
    private List<DataPiece> UglyMove(bool teamColor)
    {
        //get all piece in normal,ui can move
        List<GameObject> GOs = GetPieces(teamColor);
        List<DataPiece> listData = new List<DataPiece>();
        for (int i = 0; i < GOs.Count; i++)
        {
            Calculator method = GOs[i].GetComponent<calTest>().calculator;
            List<DataPiece> returnValue = method.Calculatev2();
            if (returnValue == null || returnValue.Count == 0) continue;

            listData.AddRange(returnValue);
        }
        listData = listData.OrderBy(x => x.score).ToList();
        return listData;
        /*
        List<DataPiece> listData = new List<DataPiece>();
        var list = OnCalculate.GetInvocationList();
        Debug.Log("count : " + list.Length);
        int count = 0;
        foreach (var item in list)
        {
            Calculate method = (Calculate)item;
            List<DataPiece> returnValue = method();

            if (returnValue == null || returnValue.Count == 0) continue;
            count++;
            listData.AddRange(returnValue);
        }
        return listData;
        */
    }

    private void StartCalculate()
    {/*
        mainDataPieces.Clear();
        var list = OnCalculate.GetInvocationList();
        Debug.Log("count : " + list.Length);
        int count = 0;
        foreach (var item in list)
        {
            Calculate method = (Calculate)item;
            List<DataPiece> returnValue = method();

            if (returnValue == null || returnValue.Count == 0) continue;
            count++;
            mainDataPieces.AddRange(returnValue);
        }
        Debug.Log("Real count : " + count);

        DataPiece bestMove = null;
        int bestScore = -9999;

        for (int i = 0; i < mainDataPieces.Count; i++)
        {
            //move the pieces => get action
            int boardScore = EvaluateBoard();
            //undo

            if (boardScore > bestScore){
                bestScore = boardScore;
                bestMove.SetValue(mainDataPieces[i].targetPiece, mainDataPieces[i].targetCell, mainDataPieces[i].targetDir);
            }

        }
        */
    }

    private int EvaluateBoard()
    {

        /*
        var list = ReturnScorePiece.GetInvocationList();
        int count = 0;
        foreach (var item in list)
        {
            ScorePiece method = (ScorePiece)item;
            int score = method();

            count += score;
        }
        return count;
        */
        List<GameObject> list = GetPieces(true, true);
        int count = 0;

        foreach (var item in list)
        {
            BasePieces piece = item.GetComponent<BasePieces>();
            int returnValue = piece.ScoreThisPiece();
            count += returnValue;
        }
        //Debug.Log("EvaluateBoard : " + count);
        return count;

    }
    /* Region ~~~~~~~~~*/


}
#region Command Mini
public class MoveSingleMini : ICommandMini
{
    public BasePieces currentPiece;
    public Cell currentCell;
    public Cell targetCell;
    public int dir;

    public void Undo()
    {
        targetCell.mCurrentPiece = null;

        currentPiece.mCurrentCell = currentCell;
        currentCell.mCurrentPiece = currentPiece;

        currentPiece.MStateRotation = dir;
    }
}

public class MovesingleinteamMini : ICommandMini
{
    public BasePieces mainPiece;
    public Cell currentCell;
    public Cell targetCell;
    public int dir;
    public void Undo()
    {
        BasePieces piece1 = currentCell.mCurrentPiece;
        BasePieces piece2 = targetCell.mCurrentPiece;

        piece1.DestroyYourself();
        piece2.DestroyYourself();

        mainPiece.mCurrentCell = currentCell;
        currentCell.mCurrentPiece = mainPiece;

        mainPiece.MStateRotation = dir;
        mainPiece.GetComponent<calTest>().Subcribe();

        targetCell.mCurrentPiece = null;
    }

}

public class MoveTeamMini : ICommandMini
{
    public BasePieces currentPiece;
    public Cell currentCell;
    public Cell targetCell;
    public int dir;
    public void Undo()
    {
        targetCell.mCurrentPiece = null;

        currentCell.mCurrentPiece = currentPiece;
        currentPiece.mCurrentCell = currentCell;
        currentPiece.MStateRotation = dir;
    }
}

public class MoveOnlyAndKillMini : ICommandMini
{
    public BasePieces mainPieceTarget;
    public BasePieces mainPieceCurrent;

    public Cell currentCell;
    public Cell targetCell;

    public int dir;
    public void Undo()
    {
        BasePieces piece1 = currentCell.mCurrentPiece;
        BasePieces piece2 = targetCell.mCurrentPiece;

        piece1.DestroyYourself();
        piece2.DestroyYourself();

        currentCell.mCurrentPiece = null;
        targetCell.mCurrentPiece = null;

        mainPieceCurrent.GetComponent<calTest>().Subcribe();
        mainPieceTarget.GetComponent<calTest>().Subcribe();

        currentCell.mCurrentPiece = mainPieceCurrent;
        mainPieceCurrent.mCurrentCell = currentCell;
        mainPieceCurrent.MStateRotation = dir;

        targetCell.mCurrentPiece = mainPieceTarget;
        mainPieceTarget.mCurrentCell = targetCell;

        List<PieceInfo> clone = mainPieceTarget.mListPiece.Clone();
        foreach (var item in clone)
        {
            StorePieces.Instance.Remove(item.nPieceName, !mainPieceTarget.mColor, false);
        }
    }
}

public class MoveTeamAndKillMini : ICommandMini
{
    public BasePieces currentPiece;
    public BasePieces targetPiece;

    public Cell currentCell;
    public Cell targetCell;

    public int dir;
    public void Undo()
    {
        targetPiece.GetComponent<calTest>().Subcribe();

        currentPiece.mCurrentCell = currentCell;
        currentCell.mCurrentPiece = currentPiece;
        currentPiece.MStateRotation = dir;

        targetPiece.mCurrentCell = targetCell;
        targetCell.mCurrentPiece = targetPiece;

        List<PieceInfo> clone = targetPiece.mListPiece.Clone();
        foreach (var item in clone)
        {
            StorePieces.Instance.Remove(item.nPieceName, !targetPiece.mColor, false);
        }
    }
}

public class TeamUpMini : ICommandMini
{
    public BasePieces currentPiece;
    public BasePieces targetPiece;

    public BasePieces deletePiece;

    public Cell currentCell;
    public Cell targetCell;

    public int dir;
    public void Undo()
    {
        deletePiece.DestroyYourself();

        currentPiece.mCurrentCell = currentCell;
        currentCell.mCurrentPiece = currentPiece;
        currentPiece.MStateRotation = dir;

        targetPiece.mCurrentCell = targetCell;
        targetCell.mCurrentPiece = targetPiece;

        currentPiece.GetComponent<calTest>().Subcribe();
        targetPiece.GetComponent<calTest>().Subcribe();
    }
}

public class RotateMini : ICommandMini
{
    public BasePieces piece;
    public Cell cell;
    public int dir;
    public void Undo()
    {
        piece.MStateRotation = dir;
    }
}

public class MoveFromStoretoCell : ICommandMini
{
    /*
         Create piece 
         Set in target cell
        - di trong storePiece
         */

    public Cell currentCell;
    public BasePieces piece;
    public void Undo()
    {
        StorePieces.Instance.Add(piece.mNamePiece, piece.mColor,false);
        piece.DestroyYourself();
        currentCell.mCurrentPiece = null;
    }
}

public class TeamupFromStoretoCell : ICommandMini
{     /*
         Create piece
        teamup => set current cell = target cell
        unsub piece in targetCell
         */

    public BasePieces mainPiece;
    public BasePieces deletePiece;
    public Cell currentCell;
    public void Undo()
    {
        //BasePieces destroyPiece = currentCell.mCurrentPiece;

        StorePieces.Instance.Add(deletePiece.mNamePiece, deletePiece.mColor,false);

        currentCell.mCurrentPiece = mainPiece;
        mainPiece.mCurrentCell = currentCell;
        mainPiece.GetComponent<calTest>().Subcribe();

        deletePiece.DestroyYourself();
    }
}
#endregion
