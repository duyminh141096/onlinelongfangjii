/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: SoundController.cs
* Script Author: MinhLe 
* Created On: 5/11/2020 11:30:30 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SoundController : MonoBehaviour
{
    public static SoundController Instance;
    private void Awake()
    {
        Instance = this;
    }

    public AudioClip musicClip;
    public AudioClip placeClip;
    public AudioClip winClip;
    public AudioClip clickClip;

    public AudioSource music;
    public AudioSource vfx;

    public float vfxVolume;
    public float musicVolume;

    public Slider vfxSlider;
    public Slider musicSlider;

    public float minY;
    public float maxY;

    public GameObject container;

    public void Open()
    {
        container.transform.DOLocalMoveY(minY, 0.5f);
    }
    public void Close()
    {
        container.transform.DOLocalMoveY(maxY, 0.5f);
    }

    private void Start()
    {
        LoadVolume();
        PlayMusicBackgound();
    }
    /// <summary>
    /// for on value change slider
    /// </summary>
    /// <param name="volume"></param>
    public void SetVolume(string volume)
    {
        switch (volume)
        {
            case "vfxVolune":
                vfx.volume = vfxSlider.value;
                break;
            case "musicVolume":
                music.volume = musicSlider.value;
                break;
        }
    }

    public void SaveVolume()
    {
        PlayerPrefs.SetFloat("vfxVolune", vfxSlider.value);
        PlayerPrefs.SetFloat("musicVolume", musicSlider.value);
    }

    public void LoadVolume()
    {
        vfxVolume = PlayerPrefs.GetFloat("vfxVolune", 0.5f);
        musicVolume = PlayerPrefs.GetFloat("musicVolume", 0.5f);
        vfxSlider.value = vfxVolume;
        musicSlider.value = musicVolume;
    }

    public void PlayVfxClip(int clip)
    {
        vfx.clip = null;
        switch (clip)
        {
            case 1:
                vfx.clip = winClip;
                break;
            case 2:
                vfx.clip = placeClip;
                break;
            case 3:
                vfx.clip = clickClip;
                break;
        }
        vfx.Play();
    }
    public void PlayMusicBackgound()
    {
        music.clip = musicClip;
        music.loop = true;
        music.PlayDelayed(2f);
    }

    public void ClickDefaultButton()
    {
        vfxVolume = 0.5f;
        musicVolume = 0.5f;
        vfxSlider.value = vfxVolume;
        musicSlider.value = musicVolume;
        SaveVolume();
    }
}
