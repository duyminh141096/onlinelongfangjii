﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ControllRules : MonoBehaviour
{
    public RectTransform ezContent;
    public RectTransform mainContent;
    public RectTransform hardContent;

    public ScrollRect ScrollRect;

    public RectTransform startPos;

    public float duration;

    //==============/
    public Toggle ezRuleTg;
    public Toggle mainRuleTg;
    public Toggle hardRuleTg;

    private void Start()
    {
        ezRuleTg.onValueChanged.AddListener(x => {
            ezContent.gameObject.SetActive(x);
            if (x) { ScrollRect.content = ezContent;
                SoundController.Instance.PlayVfxClip(3);
            }
        });
        mainRuleTg.onValueChanged.AddListener(x => {
            mainContent.gameObject.SetActive(x);
            if (x) { ScrollRect.content = mainContent; SoundController.Instance.PlayVfxClip(3); }
        });
        hardRuleTg.onValueChanged.AddListener(x => {
            hardContent.gameObject.SetActive(x);
            if (x) { ScrollRect.content = hardContent; SoundController.Instance.PlayVfxClip(3); }
        });
    }

    public void ShowPanelRules()
    {
        startPos.DOAnchorPosY(0, duration);
    }
    public void HidePanelRules()
    {
        startPos.DOAnchorPosY(1200, duration);
    }

}
