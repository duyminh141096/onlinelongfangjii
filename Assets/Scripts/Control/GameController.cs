﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public enum State
{
    None, Ready
};
/// <summary> 
/// Record summary ///
/// dir  = state rotation
/// mode = command
/// target cell 2 cho: Cell null hoac Basepiece
/// target pieces : UI or Normal
/// 
/// Luu Table luc bat dau turn
/// Luu Turn khi ket thuc turn
/// </summary>
/// 

public partial class GameController : MonoBehaviour
{
    public static PlayMode playMode;
    public Turn turn = new Turn();
    public Table table = new Table();
    #region Control UI 


    //========= UI ===========
    //=======White Team ======
    [Header("--UI--")]
    public GameObject UI_Move;

    public GameObject UI_MoveandKill;

    //public GameObject ReadyImage;

    //public Animator Anim_UI_Ready;

    public GameObject UI_Rotation;
    public GameObject UI_RotateHardMode;

    public GameObject UI_Rotation_White;
    public GameObject UI_Rotation_Black;
    public GameObject UI_PassTurn_White;

    public Text Text_WinText;
    public Text TisoWin;

    public GameObject Panel_Win;
    public GameObject UI_Sur;
    public GameObject ImageRotation;

    public GameObject UI_Undo;
    public GameObject UI_Save;
    public GameObject UI_Menu;

    public GameObject UI_Home;
    public GameObject Shield;


    //================================

    [Header("------")]
    public GameObject StartPanel;
    [Header("Controller")]
    public CreateRecord CreateRecord;

    public void Surrender()
    {
        if (PiecesManager.instance.isBlackTurn)
        {
            WinGame("Xanh chiến thắng ván này!");
        }
        else
        {
            WinGame("Cam chiến thắng ván này!");
        }
    }

    public void ShieldGame()
    {
        Invoke("UnActiveShield", 0.51f);
    }
    void UnActiveShield()
    {
        Shield.SetActive(false);
    }

    public void ShowMenuUI(bool show)
    {
        UI_Menu.SetActive(show);
    }

    public void LoadSceneGame(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void UISurShow(bool isBlackTurn)
    {
        UI_Sur.SetActive(true);
        if (isBlackTurn)
        {
            UI_Sur.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }
        else
        {
            UI_Sur.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
    }
    public void UI_PassTurn_White_Active(bool active)
    {
        SoundController.Instance.PlayVfxClip(3);
        if (mCurentBasePiece.mColor)
        {
            UI_PassTurn_White.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            UI_PassTurn_White.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }
        UI_PassTurn_White.SetActive(active);
        ImageRotation.SetActive(active);
    }

    public void TurnOnUIMove()
    {
        UI_Move.SetActive(true);
        if (mCurentBasePiece.mColor)
        {
            UI_Move.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            UI_Move.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }
    }

    public void ShowUIRotation(bool show)
    {

        if (mCurentBasePiece.mColor)
        {
            UI_Rotation.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            UI_Rotation.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }
        UI_Rotation.SetActive(show);
    }

    public void ChooseRotaOrpass(bool rotation)
    {
        SoundController.Instance.PlayVfxClip(3);
        if (rotation)
        {
            //show Ui Rotation
            ShowUIRotation(true, mCurentBasePiece.mColor);
            UI_PassTurn_White_Active(dropPiece);
        }
        else
        {
            CreateRecord.AddDataMatch(table, turn.Clone());
            UI_Rotation.SetActive(false);
            mCurentBasePiece.isDropPiece = false;
            ShowUIRotation(false, mCurentBasePiece.mColor);
            PiecesManager.instance.SwitchSides(!mCurentBasePiece.mColor);
        }
    }
    public void AddDataRecord()
    {
        CreateRecord.AddDataMatch(table, turn.Clone());
    }

    private void Awake()
    {
        playMode = PlayerPrefs.GetInt("PlayMode", 0) == 0 ? PlayMode.MultiPlay : PlayMode.SinglePlay;
        Debug.Log("PLAY MODE : " + playMode.ToString());
        PieceState = State.None;
        instance = this;

        mHightLightCells = new List<Cell>();

        #region For UI
        UI_Move.SetActive(false);

        UI_MoveandKill.SetActive(false);

        // ReadyImage.SetActive(true);

        UI_Undo.SetActive(false);
        UI_Save.SetActive(false);
        UI_Home.SetActive(false);
        #endregion
    }
    private void Start()
    {
        StartPanel.SetActive(true);

        //IF GAME MODE = MULTI
        //Create new match for record
        CreateRecord.CreateFolder();
        CreateRecord.RefeshMatch();
    }

    public void ClickStartButton()
    {
        SoundController.Instance.PlayVfxClip(3);
        StartPanel.SetActive(false);

        if (playMode == PlayMode.MultiPlay)
            SetupLongPhuong.Instance.SwitchSide(true, true);
        else
            botManager.panelChooseTeam.SetActive(true);// active button
    }


    public void ShowUIRotation(bool active, bool iswhitepieces)
    {

        if (active)
        {
            if (iswhitepieces) UI_Rotation_White.GetComponent<RectTransform>().DOAnchorPosY(0, 0.3f);
            else
                UI_Rotation_Black.GetComponent<RectTransform>().DOAnchorPosY(0, 0.3f);

            // UI_Undo.SetActive(false);

        }
        else
        {
            if (iswhitepieces) UI_Rotation_White.GetComponent<RectTransform>().DOAnchorPosY(1000, 0.3f);
            else
                UI_Rotation_Black.GetComponent<RectTransform>().DOAnchorPosY(1000, 0.3f);
            // UI_Undo.SetActive(true);
        }

    }
    public void TurnOffButtonRotate(bool active)
    {
        SoundController.Instance.PlayVfxClip(3);
        if (active)
        {
            UI_Rotation_White.GetComponent<RectTransform>().DOAnchorPosY(0, 0.3f);

            UI_Rotation_Black.GetComponent<RectTransform>().DOAnchorPosY(0, 0.3f);

        }
        else
        {
            UI_Rotation_White.GetComponent<RectTransform>().DOAnchorPosY(1000, 0.3f);

            UI_Rotation_Black.GetComponent<RectTransform>().DOAnchorPosY(1000, 0.3f);
        }

    }
    //bool isStart = true;
    #region  old Ready
    /*
       IEnumerator TuronOff()
       {
           yield return new WaitForSeconds(0.5f);
           if (isStart)
           {
               Anim_UI_Ready.SetTrigger("Do_Black");
               Anim_UI_Ready.SetBool("Ready_White", false);
           }
           else
           {

               Anim_UI_Ready.SetBool("Ready_Black", false);
           }

       }
       IEnumerator TurnOnImage()
       {
           yield return new WaitForSeconds(0.5f);
           ReadyImage.SetActive(false);

       }


       public void SwitchWhitePieces(bool isSwitches)
       {
           if (isSwitches)
           {
               //todo
               PiecesManager.instance.SwitchPieces(true);
               Anim_UI_Ready.SetBool("Ready_White", true);
               isStart = true;
               StartCoroutine("TuronOff");
           }
           else
           {
               Anim_UI_Ready.SetBool("Ready_White", true);
               isStart = true;
               StartCoroutine("TuronOff");
           }
       }
       public void SwitchBlackPieces(bool isSwitches)
       {
           if (isSwitches)
           {
               //todo
               PiecesManager.instance.SwitchPieces(false);
               Anim_UI_Ready.SetBool("Ready_Black", true);
               isStart = false;
               StartCoroutine("TuronOff");
           }
           else
           {
               Anim_UI_Ready.SetBool("Ready_Black", true);
               isStart = false;
               StartCoroutine("TuronOff");

           }
           StartCoroutine("TurnOnImage");
       }
       */
    #endregion

    #endregion
    #region For Rotation selected Item
    [Header("End Region Controller")]
    public BasePieces SelectedPiece;

    public CanRotHard canRotHard;//se dc define o click item 
    public ClickSiteRot siteRot;
    private void ExecuteRotateHardModeRight()
    {
        switch (canRotHard)
        {
            case CanRotHard.Can:
                //show UI rot 1 or all
                UI_RotateHardMode.transform.localEulerAngles = PiecesManager.instance.isBlackTurn ? Vector3.zero :  new Vector3(0, 0, 180);
                UI_RotateHardMode.SetActive(true);
                break;
            case CanRotHard.Cannot:
                ChangeRotNormalModeRight();
                break;
        }
    }

    private void ExecuteRotateHardModeLeft()
    {
        switch (canRotHard)
        {
            case CanRotHard.Can:
                //show UI rot 1 or all
                UI_RotateHardMode.transform.localEulerAngles = PiecesManager.instance.isBlackTurn ? Vector3.zero :  new Vector3(0, 0, 180);
                UI_RotateHardMode.SetActive(true);
                break;
            case CanRotHard.Cannot:
                ChangeRotNormalModeLeft();
                break;
        }
    }

    public void ChangeRotateHardMode()
    {
        switch (siteRot)
        {
            case ClickSiteRot.Left:
                ChangeRotNormalModeLeft();
                break;
            case ClickSiteRot.Right:
                ChangeRotNormalModeRight();
                break;
        }
        PieceDetails.Instance.ReceiveRequest(SelectedPiece.gameObject, false);
    }

    public void ChangeRotateAllHardMode()
    {
        switch (siteRot)
        {
            case ClickSiteRot.Left:
                ChangeRotHardModeLeft();
                break;
            case ClickSiteRot.Right:
                ChangeRotHardModeRight();
                break;
        }
        PieceDetails.Instance.ReceiveRequest(SelectedPiece.gameObject, false);
    }

    private void ChangeRotHardModeRight()
    {
        PieceDetails.Instance.ReceiveRequest(SelectedPiece.gameObject, false);
        int pastState = SelectedPiece.MStateRotation;

        if (!SelectedPiece.isDropPiece)
        {
            if (PiecesManager.rotation)
            {
                turn.mode = mode.RotationPiece;
                RotationPiece command = new RotationPiece(SelectedPiece, pastState);
                UndoManager.instance.InsertCommand(command);
                PiecesManager.rotation = false;
            }

            PiecesManager.instance.SwitchSides(!SelectedPiece.mColor);
            instance.ShowUIRotation(false, SelectedPiece.mColor);
            instance.UI_PassTurn_White_Active(false);
        }
        switch (SelectedPiece.MStateRotation)
        {
            case 1:
                SelectedPiece.MStateRotation = 2;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, false, true);
                break;
            case 2:
                SelectedPiece.MStateRotation = 3;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, false, true);
                break;
            case 3:
                SelectedPiece.MStateRotation = 4;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, false, true);
                break;
            case 4:
                SelectedPiece.MStateRotation = 1;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, false, true);
                break;
        }
        turn.dir = SelectedPiece.MStateRotation;
        turn.clickSiteRot = ClickSiteRot.Right;
        mCurentBasePiece.ClearCells();
        mTargetCell = null;
        ClearListHightLightCell();
        SetState(State.None);
        if (!SelectedPiece.isDropPiece)
        {
            CreateRecord.AddDataMatch(table, turn.Clone());
        }
        turn.clickSiteRot = ClickSiteRot.None;
        SelectedCellColor(false);

        ShowUIRotation(false);
    }

    private void ChangeRotHardModeLeft()
    {
        PieceDetails.Instance.ReceiveRequest(SelectedPiece.gameObject, false);
        int pastState = SelectedPiece.MStateRotation;

        if (!SelectedPiece.isDropPiece)
        {
            if (PiecesManager.rotation)
            {
                turn.mode = mode.RotationPiece;
                RotationPiece command = new RotationPiece(SelectedPiece, pastState);
                UndoManager.instance.InsertCommand(command);
                PiecesManager.rotation = false;
            }

            PiecesManager.instance.SwitchSides(!SelectedPiece.mColor);
            instance.ShowUIRotation(false, SelectedPiece.mColor);
            instance.UI_PassTurn_White_Active(false);
        }
        switch (SelectedPiece.MStateRotation)
        {
            case 1:
                SelectedPiece.MStateRotation = 4;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation,true,true);
                //mLeftButton.onClick.RemoveAllListeners();
                break;
            case 2:
                SelectedPiece.MStateRotation = 1;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, true, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
            case 3:
                SelectedPiece.MStateRotation = 2;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, true, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
            case 4:
                SelectedPiece.MStateRotation = 3;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, true, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
        }
        turn.dir = SelectedPiece.MStateRotation;
        turn.clickSiteRot = ClickSiteRot.Left;
        mCurentBasePiece.ClearCells();
        mTargetCell = null;
        ClearListHightLightCell();
        SetState(State.None);
        if (!SelectedPiece.isDropPiece)
        {
            CreateRecord.AddDataMatch(table, turn.Clone());
        }
        turn.clickSiteRot = ClickSiteRot.None;
        SelectedCellColor(false);

        ShowUIRotation(false);

    }

    private void ChangeRotNormalModeRight()
    {
        PieceDetails.Instance.ReceiveRequest(SelectedPiece.gameObject, false);
        int pastState = SelectedPiece.MStateRotation;

        if (!SelectedPiece.isDropPiece)
        {
            if (PiecesManager.rotation)
            {
                turn.mode = mode.RotationPiece;
                RotationPiece command = new RotationPiece(SelectedPiece, pastState);
                UndoManager.instance.InsertCommand(command);
                PiecesManager.rotation = false;
            }

            PiecesManager.instance.SwitchSides(!SelectedPiece.mColor);
            instance.ShowUIRotation(false, SelectedPiece.mColor);
            instance.UI_PassTurn_White_Active(false);
        }
        switch (SelectedPiece.MStateRotation)
        {
            case 1:
                SelectedPiece.MStateRotation = 2;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation,false);
                break;
            case 2:
                SelectedPiece.MStateRotation = 3;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation,false);
                break;
            case 3:
                SelectedPiece.MStateRotation = 4;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation,false);
                break;
            case 4:
                SelectedPiece.MStateRotation = 1;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation,false);
                break;
        }
        turn.dir = SelectedPiece.MStateRotation;
        mCurentBasePiece.ClearCells();
        mTargetCell = null;
        ClearListHightLightCell();
        SetState(State.None);
        if (!SelectedPiece.isDropPiece)
        {
            CreateRecord.AddDataMatch(table, turn.Clone());
        }
        SelectedCellColor(false);

        ShowUIRotation(false);
    }

    public void ChangeRotationRight()
    {
        SoundController.Instance.PlayVfxClip(3);
        siteRot = ClickSiteRot.Right;
        if (SetupLongPhuong.Instance.typeSetup == TypeSetUp.hardRule)
        {
            //do some thing and return
            ExecuteRotateHardModeRight();
            return;
        }

        ChangeRotNormalModeRight();
    }

    private void ChangeRotNormalModeLeft()
    {
        PieceDetails.Instance.ReceiveRequest(SelectedPiece.gameObject, false);
        int pastState = SelectedPiece.MStateRotation;

        if (!SelectedPiece.isDropPiece)
        {
            if (PiecesManager.rotation)
            {
                turn.mode = mode.RotationPiece;

                RotationPiece command = new RotationPiece(SelectedPiece, pastState);
                UndoManager.instance.InsertCommand(command);
                PiecesManager.rotation = false;
            }

            PiecesManager.instance.SwitchSides(!SelectedPiece.mColor);
            ShowUIRotation(false, SelectedPiece.mColor);
            //deactive 
            UI_PassTurn_White_Active(false);
        }

        switch (SelectedPiece.MStateRotation)
        {
            case 1:
                SelectedPiece.MStateRotation = 4;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation,true);
                //mLeftButton.onClick.RemoveAllListeners();
                break;
            case 2:
                SelectedPiece.MStateRotation = 1;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
            case 3:
                SelectedPiece.MStateRotation = 2;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
            case 4:
                SelectedPiece.MStateRotation = 3;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
        }
        turn.dir = SelectedPiece.MStateRotation;

        //teamuplist.UpdateRotation(BasePieces.MStateRotation);

        mCurentBasePiece.ClearCells();
        SelectedCellColor(false);
        mTargetCell = null;
        ClearListHightLightCell();
        SetState(State.None);
        if (!SelectedPiece.isDropPiece)
        {
            CreateRecord.AddDataMatch(table, turn.Clone());
        }

        // GameController.instance.ShowUIRotation(false, BasePieces.mColor);
        ShowUIRotation(false);
    }

    public void ChangeRotationLeft()
    {
        SoundController.Instance.PlayVfxClip(3);
        siteRot = ClickSiteRot.Left;
        if (SetupLongPhuong.Instance.typeSetup == TypeSetUp.hardRule)
        {
            //do some thing and return
            ExecuteRotateHardModeLeft();
            return;
        }

        ChangeRotNormalModeLeft();
    }


    #endregion

    public State PieceState;

    public static GameController instance;
    public List<Cell> mHightLightCells;
    public bool isMoveTeam = false;
    public static bool dropPiece;

    public BasePieces mCurentBasePiece;
    public BasePieces mTargetBasePiece;

    public Cell mCurrentCell;
    public Cell mTargetCell;

    [Header("CLASS VARIABLES")]
    public StorePieces StorePieces;
    public BotManager botManager;
    public void ShowCell(BasePieces basePiece)
    {
        mCurentBasePiece = basePiece;
        PieceState = State.Ready;
        mCurentBasePiece.CheckPathing();
        mCurentBasePiece.ShowCells();

        mHightLightCells = mCurentBasePiece.mHightlightedCells;
        mCurrentCell = mCurentBasePiece.mCurrentCell;

        PiecesManager.rotation = true;

        UI_Undo.SetActive(false);
        UI_Save.SetActive(false);
        UI_Home.SetActive(false);
    }

    public void CheckPiece(BasePieces piece)
    {
        if (piece.mColor == PiecesManager.instance.isBlackTurn)
        {
            if (PieceState == State.Ready)
            {
                if (mCurentBasePiece.mID == piece.mID)
                {
                    SelectedCellColor(false);
                    mCurentBasePiece.ClearCells();
                    mTargetCell = null;
                    ClearListHightLightCell();
                    PieceState = State.None;
                    ShowUIRotation(false, mCurentBasePiece.mColor);
                    SelectedPiece = null;
                    UI_Undo.SetActive(true);
                    UI_Save.SetActive(true);
                    UI_Home.SetActive(true);
                    StorePieces.Instance.ShowSelectCell(piece.transform, false);
                }
                else
                {
                    Store storae = StorePieces.GetInfo(piece.mNamePiece, piece.mColor);
                    if (storae.count > 0)
                    {
                        SelectedCellColor(false);
                        mCurentBasePiece.ClearCells();
                        mTargetCell = null;
                        ClearListHightLightCell();
                        ShowUIRotation(false, piece.mColor);
                        SelectedCellColor(false);

                        PieceInfos info = new PieceInfos();
                        info.SetValue(piece);
                        turn.targetPiece = info;
                        turn.dir = piece.MStateRotation;
                        table = new Table();
                        table.Clear();
                        table = CreateRecord.SaveTable();

                        ShowCell(piece);
                        StorePieces.Instance.ShowSelectCell(piece.transform, true);
                    }
                    else
                    {
                        mCurentBasePiece.ClearCells();
                        SelectedCellColor(false);
                        PieceState = State.None;
                        ShowUIRotation(false, mCurentBasePiece.mColor);
                        SelectedPiece = null;
                        UI_Undo.SetActive(true);
                        UI_Save.SetActive(true);
                        UI_Home.SetActive(true);
                        StorePieces.Instance.ShowSelectCell(piece.transform, false);
                    }
                }
                return;
            }
            if (PieceState == State.None)
            {
                Store store = StorePieces.GetInfo(piece.mNamePiece, piece.mColor);
                if (store.count > 0)
                {
                    PieceInfos info = new PieceInfos();
                    info.SetValue(piece);
                    turn.targetPiece = info;
                    turn.dir = piece.MStateRotation;
                    table = new Table();
                    table.Clear();
                    table = CreateRecord.SaveTable();

                    ShowCell(piece);
                    StorePieces.Instance.ShowSelectCell(piece.transform, true);
                }
            }
        }
    }

    public Color normalCellColor;
    public Color selectedCellColor;

    public Cell SelectedCell;

    public void SelectedCellColor(bool enable)
    {
        if (SelectedCell != null)
        {
            SelectedCell.mOutlineImage.enabled = enable;
            if (enable)
                SelectedCell.mOutlineImage.color = selectedCellColor;
            else
                SelectedCell.SetColor();
        }
    }

    private void CheckRotState(BasePieces basePieces)
    {
        if (basePieces.mListPiece.GetListNameCount() == 1) canRotHard = CanRotHard.Cannot;
        else canRotHard = CanRotHard.Can;
    }
    public void ResetRotState()
    {
        canRotHard = CanRotHard.None;
    }

    public void GetInfoFromPiece(BasePieces basePiece)
    {
        if (PieceState == State.None)
        {
            if (basePiece.mColor == PiecesManager.instance.isBlackTurn)
            {
                //it's for recore
                PieceInfos info = new PieceInfos();
                turn.dir = basePiece.MStateRotation;
                table = new Table();
                table.Clear();
                table = CreateRecord.SaveTable();
                //////////////////
                ShowCell(basePiece);
                PieceDetails.Instance.ReceiveRequest(basePiece.gameObject, true);
                CheckRotState(basePiece);
                if (basePiece.mNamePiece == "Long" || basePiece.mNamePiece == "Phuong" || basePiece.mNamePiece == "Vuong" || basePiece.isDropPiece == true)
                {
                    SelectedCell = basePiece.mCurrentCell;

                    info.SetValue(basePiece);//record
                    turn.SetPieceInfo(info);

                    SelectedCellColor(true);
                    SelectedPiece = basePiece;
                    ShowUIRotation(false, basePiece.mColor);
                    return;
                }
                ShowUIRotation(true, basePiece.mColor);
                SelectedCell = basePiece.mCurrentCell;
                SelectedCellColor(true);
                SelectedPiece = basePiece;
                info.SetValue(SelectedPiece);
                turn.SetPieceInfo(info);

                return;
            }
        }
        if (PieceState == State.Ready)
        {
            if (basePiece.mID == mCurentBasePiece.mID)
            {
                mCurentBasePiece.ClearCells();
                PieceDetails.Instance.ReceiveRequest(basePiece.gameObject, false);
                PieceState = State.None;
                ShowUIRotation(false, mCurentBasePiece.mColor);
                SelectedPiece = null;
                UI_Undo.SetActive(true);
                UI_Save.SetActive(true);
                UI_Home.SetActive(true);
                SelectedCell = basePiece.mCurrentCell;
                SelectedCellColor(false);
            }
            else
            {
                if (basePiece.kind == Kind.Normal)
                {
                    if (basePiece.mColor == mCurentBasePiece.mColor) // team
                    {
                        bool checkMove = CheckContainCell(basePiece.mCurrentCell);
                        if (checkMove)//Piece nam trong cell hightlight
                        {
                            if (mCurentBasePiece.kind == Kind.Normal)
                            {
                                PieceDetails.Instance.ReceiveRequest(basePiece.gameObject, false);
                                turn.mode = mode.TeamUp;
                                turn.targetCell = basePiece.mCurrentCell.mId;
                                TeamUp command = new TeamUp();
                                command.GetStandPiece(basePiece);
                                command.GetMovePiece(mCurentBasePiece);
                                PiecesManager.rotation = false;
                                UndoManager.instance.InsertCommand(command);

                                mCurentBasePiece.TeamUp(basePiece.mListPiece.GetPieceNamesList());//teamup
                                mCurentBasePiece.Move(basePiece.mCurrentCell, true, false);//move
                                basePiece.DisableAfterIimer(0.3f);
                                PieceState = State.None;
                                SelectedPiece = mCurentBasePiece;
                                Destroy(basePiece.gameObject, 0.4f);

                                string NamePiece = mCurentBasePiece.mNamePiece;
                                if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
                                {
                                    GameController.instance.AddDataRecord();
                                    PiecesManager.instance.SwitchSides(!basePiece.mColor);
                                }
                                else
                                {
                                    ShowUIRotation(true);
                                    //PiecesManager.instance.SwitchSides(!basePiece.mColor);
                                    //UI_PassTurn_White_Active(true);
                                }
                                return;
                            }
                            if (mCurentBasePiece.kind == Kind.UI)
                            {
                                //create , team up , remove from store , add command
                                //assign command
                                PieceDetails.Instance.ReceiveRequest(basePiece.gameObject, false);
                                turn.mode = mode.TeamUpFormStoreToCell;
                                turn.targetCell = basePiece.mCurrentCell.mId;
                                TeamUpFormStoreToCell command = new TeamUpFormStoreToCell();
                                command.GetInfoOldPieces(basePiece, mCurentBasePiece.transform.position, basePiece.mColor);

                                //create new piece
                                StorePieces.Instance.ShowSelectCell(mCurentBasePiece.transform, false);
                                GameObject newPiece = PiecesManager.instance.CreateBasePiece(mCurentBasePiece.mColor, mCurentBasePiece.MStateRotation, mCurentBasePiece.mNamePiece);
                                newPiece.GetComponent<BasePieces>().isDropPiece = true;

                                command.GetInfoNewPiece(newPiece.GetComponent<BasePieces>().mNamePiece);
                                UndoManager.instance.InsertCommand(command);//done command

                                SelectedPiece = newPiece.GetComponent<BasePieces>();
                                newPiece.transform.position = mCurentBasePiece.transform.position;//done create

                                newPiece.GetComponent<BasePieces>().TeamUp(basePiece.mListPiece.GetPieceNamesList());//teamup
                                newPiece.GetComponent<BasePieces>().Move(basePiece.mCurrentCell, true, false);//move

                                SelectedPiece = newPiece.GetComponent<BasePieces>();
                                Destroy(basePiece.gameObject, 0.4f);

                                StorePieces.Instance.Remove(mCurentBasePiece.mNamePiece, mCurentBasePiece.mColor);//remove from store
                                SetState(State.None);
                                mCurentBasePiece.ClearCells();
                                ClearListHightLightCell();
                                SelectedCellColor(false);

                                mCurentBasePiece = newPiece.GetComponent<BasePieces>();

                                string NamePiece = mCurentBasePiece.mNamePiece;
                                if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
                                {
                                    GameController.instance.AddDataRecord();
                                    PiecesManager.instance.SwitchSides(!basePiece.mColor);
                                }
                                else
                                {
                                    ShowUIRotation(true);
                                    //PiecesManager.instance.SwitchSides(!basePiece.mColor);
                                    //UI_PassTurn_White_Active(true);
                                }
                                return;
                            }
                        }
                        else //nam ngoai hight light cell cua mCurrentPiece
                        {

                            CheckRotState(basePiece);
                            SelectedCellColor(false);
                            mCurentBasePiece.ClearCells();
                            mTargetCell = null;
                            ClearListHightLightCell();
                            ShowCell(basePiece);
                            PieceDetails.Instance.ReceiveRequest(basePiece.gameObject, true);
                            if (basePiece.mNamePiece == "Long" || basePiece.mNamePiece == "Phuong"
                                || basePiece.mNamePiece == "Vuong" || basePiece.isDropPiece == true)
                                ShowUIRotation(false, basePiece.mColor);
                            else
                            {
                                ShowUIRotation(true, basePiece.mColor);
                            }
                            SelectedPiece = basePiece;
                            SelectedCell = basePiece.mCurrentCell;

                            PieceInfos info = new PieceInfos();//record
                            info.SetValue(basePiece);
                            turn.SetPieceInfo(info);

                            SelectedCellColor(true);
                        }

                    }
                    else // khac team
                    {

                        mTargetCell = basePiece.mCurrentCell;
                        mTargetBasePiece = basePiece;

                        bool checkMove = CheckContainCell(mTargetCell);

                        if (checkMove)
                        {
                            PieceDetails.Instance.ReceiveRequest(SelectedPiece.gameObject, false);
                            turn.targetCell = mTargetCell.mId;
                            TurnOffButtonRotate(false);
                            CheckMove(false);
                        }
                        else
                        {
                            mTargetCell = null;
                            mTargetBasePiece = null;
                        }
                    }
                }
            }
        }
    }

    public void GetInfoFromCell(Cell cell)
    {
        if (PieceState == State.Ready && cell.mCurrentPiece == null)
        {
            mTargetCell = cell;

            bool checkMove = CheckContainCell(mTargetCell);

            if (checkMove)
            {
                PieceDetails.Instance.ReceiveRequest(SelectedPiece.gameObject, false);
                turn.targetCell = mTargetCell.mId;//it's for record
                CheckMove(true);
            }
        }
    }

    public State GetState()
    {
        return PieceState;
    }
    public void ClearState()
    {
        PieceState = State.None;
    }
    public void SetState(State state)
    {
        Debug.Log("Set State");
        PieceState = state;
        //if (PieceState == State.Ready) UISurShow(false);
        //else UISurShow(true);
    }

    public void CheckMove(bool isNotPiece)
    {

        if (isNotPiece)
        {
            if (mCurentBasePiece.mListPiece.GetListNameCount() > 1)
            {
                TurnOffButtonRotate(false);
                TurnOnUIMove();
            }
            else
            {
                if (mCurentBasePiece.kind == Kind.Normal)
                {
                    turn.mode = mode.MoveSingle;//it's for record
                    turn.targetCell = mTargetCell.mId;
                    MoveSingleCommand command = new MoveSingleCommand(mCurentBasePiece, mTargetCell);
                    UndoManager.instance.InsertCommand(command);
                    PiecesManager.rotation = false;
                    if (mCurentBasePiece.mNamePiece == "Long" || mCurentBasePiece.mNamePiece == "Phuong" || mCurentBasePiece.mNamePiece == "Vuong" || mCurentBasePiece.isDropPiece == true)
                        AddDataRecord();
                    mCurentBasePiece.Move(mTargetCell, false, false);
                    SetState(State.None);
                    return;
                }
                if (mCurentBasePiece.kind == Kind.UI)
                {
                    turn.mode = mode.MoveFromStoreToCell;
                    turn.targetCell = mTargetCell.mId;
                    //add command
                    Store store = StorePieces.Instance.GetInfo(mCurentBasePiece.mNamePiece, mCurentBasePiece.mColor);
                    if (store != null)
                    {
                        MoveFromStoreToCell command = new MoveFromStoreToCell(store.pos.localPosition, mTargetCell, store.teamColor);
                        UndoManager.instance.InsertCommand(command);
                    }
                    PiecesManager.rotation = false;
                    StorePieces.Instance.ShowSelectCell(mCurentBasePiece.transform, false);
                    GameObject newPiece = PiecesManager.instance.CreateBasePiece(mCurentBasePiece.mColor, mCurentBasePiece.MStateRotation, mCurentBasePiece.mNamePiece);
                    newPiece.GetComponent<BasePieces>().isDropPiece = true;
                    SelectedPiece = newPiece.GetComponent<BasePieces>();
                    newPiece.transform.position = mCurentBasePiece.transform.position;
                    mCurentBasePiece.ClearCells();
                    SelectedCellColor(false);
                    mCurentBasePiece = newPiece.GetComponent<BasePieces>();
                    newPiece.GetComponent<BasePieces>().Move(mTargetCell, false, false);
                    if (mCurentBasePiece.mNamePiece == "Long" || mCurentBasePiece.mNamePiece == "Phuong" || mCurentBasePiece.mNamePiece == "Vuong")
                        AddDataRecord();
                    StorePieces.Instance.Remove(mCurentBasePiece.mNamePiece, mCurentBasePiece.mColor);
                    SetState(State.None);
                    ClearListHightLightCell();
                    return;
                    //move
                }
            }
        }
        else
        {
            if (mCurentBasePiece.mListPiece.GetListNameCount() > 1)
            {
                if (mCurentBasePiece.mColor)
                {
                    UI_MoveandKill.SetActive(true);
                    UI_MoveandKill.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
                }
                else
                {
                    UI_MoveandKill.SetActive(true);
                    UI_MoveandKill.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                }

            }
            else
            {
                turn.mode = mode.MoveTeamandKill;
                turn.targetCell = mTargetCell.mId;
                MoveTeamandKill command = new MoveTeamandKill(mCurentBasePiece, mTargetCell);
                command.GetKilledPiece(mTargetBasePiece);
                string namePieces = mCurentBasePiece.mNamePiece;
                if (namePieces == "Vuong" || namePieces == "Long" || namePieces == "Phuong")
                {
                    GameController.instance.AddDataRecord();
                }
                mCurentBasePiece.Move(mTargetCell, false, true);
                SelectedPiece = mCurentBasePiece;
                mTargetBasePiece.Kill(mTargetBasePiece.mListPiece.GetPieceNamesList(), command);
                UndoManager.instance.InsertCommand(command);
                PiecesManager.rotation = false;
                //PiecesManager.instance.SwitchSides(!mCurentBasePiece.mColor);
                SetState(State.None);
            }
        }

    }

    public void WinGame(string winString)
    {
        //CreateRecord.SaveRecord();

        SoundController.Instance.PlayVfxClip(1);
        Text_WinText.text = winString;

        if (PiecesManager.instance.isBlackTurn)
        {
            Panel_Win.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            Panel_Win.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }

        Panel_Win.SetActive(true);
    }



    public void PassTurn()
    {
        CreateRecord.AddDataMatch(table, turn.Clone());

        PiecesManager.instance.SwitchSides(!mCurentBasePiece.mColor);
        mCurentBasePiece.isDropPiece = false;
        ShowUIRotation(false, mCurentBasePiece.mColor);
    }

    public void CheckMoveTeam(bool check)
    {
        if (check)
        {
            mCurentBasePiece.MoveTeam(mTargetCell, ref turn);
        }
        else
        {
            canRotHard = CanRotHard.Cannot;
            mCurentBasePiece.MoveOnly(mTargetCell, ref turn);
        }
    }
    public void CheckMoveandKill(bool check)
    {
        if (check)
        {
            mCurentBasePiece.MoveTeamAndKill(mTargetCell, ref turn);
        }
        else
        {
            canRotHard = CanRotHard.Cannot;
            mCurentBasePiece.MoveOnlyAndKill(mTargetCell, ref turn);
        }
    }
    public bool CheckContainCell(Cell mTargetCell)
    {
        if (mHightLightCells != null && mTargetCell != null)
        {
            for (int i = 0; i < mHightLightCells.Count; i++)
            {
                if (mHightLightCells[i].mId == mTargetCell.mId)
                {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public void ClearListHightLightCell()
    {
        mHightLightCells.Clear();
    }

}
