/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: GameController.singleplay.cs
* Script Author: MinhLe 
* Created On: 7/28/2020 10:43:29 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameController 
{
    public void StartGame()
    {
        Debug.Log("Start Game");
        if (playMode == PlayMode.SinglePlay)
        {
            
            if (ControllerBot.teamColor == true) //orange = true , teamColor green = true
            {
                StartCoroutine(ControllerBot.Instance.CallCaculate());
            }
        }
        if(playMode == PlayMode.MultiPlay)
        {
            UISurShow(false);
            PiecesManager.instance.ChangeSignal(true);
        }
    }
}
