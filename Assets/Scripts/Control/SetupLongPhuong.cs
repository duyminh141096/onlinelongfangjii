/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: SetupLongPhuong.cs
* Script Author: MinhLe 
* Created On: 5/12/2020 11:41:48 PM*/
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public enum StateSetup
{
    pick = 0, place = 1
}
public enum TypeSetUp
{
    beginnerRule,
    mainRule,
    hardRule
}


[System.Serializable]
public class Items
{
    public bool use;
    public BasePieces item;
}
[System.Serializable]
public class Cells
{
    public bool use;
    public Cell cell;
    public int stt;
}
public class SetupLongPhuong : MonoBehaviour
{
    public static SetupLongPhuong Instance;
    private void Awake()
    {
        Instance = this;
        switch (PlayerPrefs.GetInt("TypeSetUp"))
        {
            case 0:
                typeSetup = TypeSetUp.beginnerRule;
                break;
            case 1:
                typeSetup = TypeSetUp.mainRule;
                break;
            case 2:
                typeSetup = TypeSetUp.hardRule;
                break;
        }
    }

    public GameObject setupPanel;
    public GameObject placePanelX, placePanelC;
    public TypeSetUp typeSetup;
    public StateSetup state;

    public int itemP;
    public bool teamC;

    public GameObject panelC, panelX;
    public int openYC, openYX, closeYC, closeYX;

    public List<Items> ItemsC, ItemsX;
    public List<Cells> CellsC, CellsX;//7:7,1:7,7:1,1:1

    public List<GameObject> buttonItemC, buttonItemX;

    [Header("Arrow")]
    public List<GameObject> ArrowC, ArrowX;
    [Header("ORTHER")]
    public GameObject panelSur;

    public void SwitchSide(bool team, bool show)
    {
        if (team)
            panelSur.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
        else
            panelSur.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        panelSur.SetActive(true);
        PiecesManager.instance.ChangeSignal(team);
        Show(team, show);
        state = StateSetup.pick;
    }
    public bool CheckStartGame()
    {
        bool start = true;
        for (int i = 0; i < ItemsX.Count; i++)
        {
            if (ItemsC[i].use == false || ItemsX[i].use == false)
                start = false;
        }
        return start;
    }
    public void ClickItem(int i)
    {
        itemP = i;
        panelSur.SetActive(false);

        //for record
        table = new Table();
        table.Clear();
        table = GameController.instance.CreateRecord.SaveTable();
    }
    public void ClickTeam(bool team)
    {
        state = StateSetup.place;
        teamC = team;
        Show(teamC, false);
        switch (GameController.playMode)
        {
            case PlayMode.SinglePlay:

                if (typeSetup == TypeSetUp.beginnerRule)
                {
                    if (teamC)
                        placePanelX.SetActive(true);
                    else
                        placePanelC.SetActive(true);
                }
                else
                {
                    //hight light nhung cell co the duoc chon
                    if (teamC)
                    {
                        for (int i = 0; i < CellsC.Count; i++)
                        {
                            CellsC[i].cell.SetColor(Color.red);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < CellsC.Count; i++)
                        {
                            CellsX[i].cell.SetColor(Color.red);
                        }
                    }
                }
                break;
            case PlayMode.MultiPlay:
                if (typeSetup == TypeSetUp.beginnerRule)
                {
                    if (teamC)
                        placePanelX.SetActive(true);
                    else
                        placePanelC.SetActive(true);
                }
                else
                {
                    //hight light nhung cell co the duoc chon
                    if (teamC)
                    {
                        for (int i = 0; i < CellsC.Count; i++)
                        {
                            CellsC[i].cell.SetColor(Color.red);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < CellsC.Count; i++)
                        {
                            CellsX[i].cell.SetColor(Color.red);
                        }
                    }
                }
                break;
        }
    }
    public void AddItem(BasePieces initem, bool teamC)
    {
        Items a = new Items();
        a.use = false;
        a.item = initem;
        if (teamC)
            ItemsC.Add(a);
        else
            ItemsX.Add(a);
    }
    int sttC = -1;
    int sttX = -1;
    public void AddCell(Cell cell, bool team)
    {
        Cells a = new Cells();
        a.cell = cell;
        a.use = false;
        if (team) { sttC++; a.stt = sttC; }
        else { sttX++; a.stt = sttX; };

        if (team)
            CellsC.Add(a);
        else
            CellsX.Add(a);
    }
    public void CheckPiece(BasePieces pieces)
    {
        Items check;
        if (teamC)
            check = ItemsC.Find(x => x.item == pieces);
        else
            check = ItemsX.Find(x => x.item == pieces);
        if (check != null)
        {
            Cell checkcell = pieces.mCurrentCell;
            Cells checkCells;
             if (teamC)
                checkCells = CellsC.Find(x => x.cell == checkcell);
            else
                checkCells = CellsX.Find(x => x.cell == checkcell);
            if (checkCells != null)
            {
                if (typeSetup == TypeSetUp.mainRule || typeSetup == TypeSetUp.hardRule)
                {
                    ActionTeamUp(checkCells);
                }
            }
           
        }

    }
    public void CheckCell(Cell cell)
    {
        Cells check;
        if (teamC)
            check = CellsC.Find(x => x.cell == cell);
        else
            check = CellsX.Find(x => x.cell == cell);
        if (check != null)
        {
            if (!check.use)
            {
                Action(check);
            }
        }


    }
    public Turn turn = new Turn();
    public Table table = new Table();
    public void RandomCellifBotChoose(bool team)
    {
        // CellsC or CellsX
        Cell targetCell = new Cell();

        if (team)// true orange
        {
            if (typeSetup == TypeSetUp.beginnerRule)
            {
                int value = new int();
                //bot choose piece 
                if (CellValues.Count > 1)// 2 value
                {
                    int index = Random.Range(0, 2);
                    value = CellValues[index];
                    CellValues.Remove(index);
                }
                else
                {
                    value = CellValues[0];
                }
                targetCell = CellsC[value].cell;
            }
            else
            {
                int value = new int();
                int index = Random.Range(0, CellValuesLine.Count);
                value = CellValuesLine[index];
                CellValuesLine.Remove(index);

                targetCell = CellsC[value].cell;
            }
        }
        else
        {
            if (typeSetup == TypeSetUp.beginnerRule)
            {
                int value = new int();
                if (CellValues.Count > 1)// 2 value
                {
                    int index = Random.Range(0, 2);
                    value = CellValues[index];
                    CellValues.Remove(index);
                }
                else
                {
                    value = CellValues[0];
                }
                targetCell = CellsX[value].cell;
            }
            else
            {
                int value = new int();
                int index = Random.Range(0, CellValuesLine.Count);
                value = CellValuesLine[index];
                CellValuesLine.Remove(index);

                targetCell = CellsX[value].cell;
            }
        }
        CheckCell(targetCell);
    }
    private void TeamUp(BasePieces basePieces, Cell targetCell)
    {

        basePieces.TeamUp(targetCell.mCurrentPiece.mListPiece.GetPieceNamesList());//teamup
        if (basePieces.mCurrentCell != null)
            basePieces.mCurrentCell.mCurrentPiece = null;
        //switch cells
        Destroy(targetCell.mCurrentPiece.gameObject);
        basePieces.mCurrentCell = targetCell;
        basePieces.mCurrentCell.mCurrentPiece = basePieces;
        StartCoroutine(TransformAction(basePieces, targetCell));//  basePieces.Place(targetCell);
        basePieces.mIsPlace = true;
    }
    public void ActionTeamUp(Cells cell)
    {
        if (typeSetup == TypeSetUp.mainRule || typeSetup == TypeSetUp.hardRule)
        {
            if (state == StateSetup.place)
            {
                if (teamC)
                {
                    SetUpTeamUpCommand command = new SetUpTeamUpCommand();
                    command.itemPlace = itemP;
                    command.team = true;
                    command.button = buttonItemC[itemP];
                    command.currentCell = cell.cell;
                    command.stt = 141096;
                    command.oldTypePiece = cell.cell.mCurrentPiece.mNamePiece;
                    command.newTypePiece = ItemsC[itemP].item.mNamePiece;
                    command.dir = 2;
                    UndoManager.instance.InsertCommand(command);

                    turn = new Turn();
                    turn.targetPiece.SetValue(ItemsC[itemP].item);
                    turn.targetCell = cell.cell.mId;
                    turn.dir = ItemsC[itemP].item.MStateRotation;
                    turn.mode = mode.SetupTeamUp;

                    GameController.instance.CreateRecord.AddDataMatch(table, turn.Clone());

                    TeamUp(ItemsC[itemP].item, cell.cell);
                    ItemsC[itemP].use = true;
                    //ArrowC[cell.stt].SetActive(false);
                }
                else
                {
                    SetUpTeamUpCommand command = new SetUpTeamUpCommand();
                    command.itemPlace = itemP;
                    command.team = false;
                    command.button = buttonItemX[itemP];
                    command.currentCell = cell.cell;
                    command.stt = 141096;
                    command.oldTypePiece = cell.cell.mCurrentPiece.mNamePiece;
                    command.newTypePiece = ItemsX[itemP].item.mNamePiece;
                    command.dir = 2;
                    UndoManager.instance.InsertCommand(command);

                    turn = new Turn();
                    turn.targetPiece.SetValue(ItemsX[itemP].item);
                    turn.targetCell = cell.cell.mId;
                    turn.dir = ItemsX[itemP].item.MStateRotation;
                    turn.mode = mode.SetupTeamUp;

                    GameController.instance.CreateRecord.AddDataMatch(table, turn.Clone());

                    TeamUp(ItemsX[itemP].item, cell.cell);
                    ItemsX[itemP].use = true;
                    //ArrowX[cell.stt].SetActive(false);
                }
                if (!CheckStartGame())
                {
                    state = StateSetup.pick;
                    SwitchSide(!teamC, true);
                }
                else
                {
                    //DeleteAll();
                    GameManager.Instance.Mode = GameMode.Play;
                    GameController.instance.StartGame();
                }
                if (typeSetup == TypeSetUp.mainRule || typeSetup == TypeSetUp.hardRule)
                {
                    for (int i = 0; i < CellsC.Count; i++)
                    {
                        CellsC[i].cell.SetColor(Color.white);
                    }
                    for (int j = 0; j < CellsX.Count; j++)
                    {
                        CellsX[j].cell.SetColor(Color.white);
                    }
                }
            }
        }
    }

    private IEnumerator TransformAction(BasePieces piece , Cell cell)
    {
        piece.transform.DOMove(cell.transform.position,0.29f);
        yield return new WaitForSeconds(0.3f);
        piece.Place(cell);
    } 
    public void Action(Cells cell)
    {
        if (typeSetup == TypeSetUp.mainRule || typeSetup == TypeSetUp.hardRule)
        {
            if (state == StateSetup.place)
            {
                if (teamC)
                {
                    turn = new Turn();
                    turn.targetPiece.SetValue(ItemsC[itemP].item);
                    turn.targetCell = cell.cell.mId;
                    turn.dir = ItemsC[itemP].item.MStateRotation;
                    turn.mode = mode.Setupincell;

                    GameController.instance.CreateRecord.AddDataMatch(table, turn.Clone());

                    StartCoroutine( TransformAction(ItemsC[itemP].item, cell.cell));
                    //ItemsC[itemP].item.Place(cell.cell);
                    ItemsC[itemP].use = true;
                    //ArrowC[cell.stt].SetActive(false);

                    SetUpCommand command = new SetUpCommand();
                    command.itemPlace = itemP;
                    command.team = true;
                    command.button = buttonItemC[itemP];
                    command.currentCell = cell.cell;
                    command.stt = 141096;
                    UndoManager.instance.InsertCommand(command);
                }
                else
                {
                    turn = new Turn();
                    turn.targetPiece.SetValue(ItemsX[itemP].item);
                    turn.targetCell = cell.cell.mId;
                    turn.dir = ItemsX[itemP].item.MStateRotation;
                    turn.mode = mode.Setupincell;

                    GameController.instance.CreateRecord.AddDataMatch(table, turn.Clone());

                    //ItemsX[itemP].item.Place(cell.cell);
                    StartCoroutine( TransformAction(ItemsX[itemP].item, cell.cell));
                    ItemsX[itemP].use = true;
                    //ArrowX[cell.stt].SetActive(false);

                    SetUpCommand command = new SetUpCommand();
                    command.itemPlace = itemP;
                    command.team = false;
                    command.button = buttonItemX[itemP];
                    command.currentCell = cell.cell;
                    command.stt = 141096;
                    UndoManager.instance.InsertCommand(command);
                }
                cell.use = true;
                
                if (!CheckStartGame())
                {
                    state = StateSetup.pick;
                    SwitchSide(!teamC, true);
                }
                else
                {
                    //DeleteAll();
                    GameManager.Instance.Mode = GameMode.Play;
                    GameController.instance.StartGame();
                }
                if (typeSetup == TypeSetUp.mainRule || typeSetup == TypeSetUp.hardRule)
                {
                    for (int i = 0; i < CellsC.Count; i++)
                    {
                        CellsC[i].cell.SetColor(Color.white);
                    }
                    for (int j = 0; j < CellsX.Count; j++)
                    {
                        CellsX[j].cell.SetColor(Color.white);
                    }
                }
            }
        }
        if (typeSetup == TypeSetUp.beginnerRule)
        {
            if (state == StateSetup.place)
            {
                if (teamC)
                {
                    turn = new Turn();
                    turn.targetPiece.SetValue(ItemsC[itemP].item);
                    turn.targetCell = cell.cell.mId;
                    turn.dir = ItemsC[itemP].item.MStateRotation;
                    turn.mode = mode.Setupincell;

                    GameController.instance.CreateRecord.AddDataMatch(table, turn.Clone());

                    //ItemsC[itemP].item.Place(cell.cell);
                    StartCoroutine(TransformAction(ItemsC[itemP].item, cell.cell));
                    ItemsC[itemP].use = true;
                    ArrowC[cell.stt].SetActive(false);

                    SetUpCommand command = new SetUpCommand();
                    command.itemPlace = itemP;
                    command.team = true;
                    command.button = buttonItemC[itemP];
                    command.currentCell = cell.cell;
                    command.stt = cell.stt;
                    UndoManager.instance.InsertCommand(command);

                }
                else
                {
                    turn = new Turn();
                    turn.targetPiece.SetValue(ItemsX[itemP].item);
                    turn.targetCell = cell.cell.mId;
                    turn.dir = ItemsX[itemP].item.MStateRotation;
                    turn.mode = mode.Setupincell;

                    GameController.instance.CreateRecord.AddDataMatch(table, turn.Clone());
                    //ItemsX[itemP].item.Place(cell.cell);
                    StartCoroutine( TransformAction(ItemsX[itemP].item, cell.cell));
                    ItemsX[itemP].use = true;
                    ArrowX[cell.stt].SetActive(false);

                    SetUpCommand command = new SetUpCommand();
                    command.itemPlace = itemP;
                    command.team = false;
                    command.button = buttonItemX[itemP];
                    command.currentCell = cell.cell;
                    command.stt = cell.stt;
                    UndoManager.instance.InsertCommand(command);
                }
                cell.use = true;
                if (!CheckStartGame())
                {
                    state = StateSetup.pick;
                    SwitchSide(!teamC, true);
                }
                else
                {
                    //DeleteAll();
                    GameManager.Instance.Mode = GameMode.Play;
                    GameController.instance.StartGame();
                }
                placePanelX.SetActive(false);
                placePanelC.SetActive(false);
            }
        }
    }
    List<int> CellValues = new List<int>() { 0, 1 };
    List<int> CellValuesLine = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
    List<int> PiecesValues = new List<int>() { 0, 1 };
    private void DeleteAll()
    {
        Debug.Log("Delete all ");
        Destroy(setupPanel, 0.2f);
        Destroy(this, 0.2f);
    }
    /// <summary>
    /// show panel for user pickup
    /// </summary>
    /// <param name="show"></param>
    public void Show(bool team, bool show)
    {
        if (show)
        {
            if (typeSetup == TypeSetUp.beginnerRule)
            {
                if (team)
                    panelC.transform.DOLocalMoveY(openYC, 0.5f);
                else
                    panelX.transform.DOLocalMoveY(openYX, 0.5f);
                if (GameController.playMode == PlayMode.SinglePlay)
                {
                    if (team == ControllerBot.teamColor)
                    {
                        int value = new int();
                        //bot choose piece 
                        if (PiecesValues.Count > 1)// 2 value
                        {
                            int index = Random.Range(0, 2);
                            value = PiecesValues[index];
                            PiecesValues.Remove(index);
                        }
                        else
                        {
                            value = PiecesValues[0];
                        }

                        ClickItem(value); // 0 or 1
                        ClickTeam(ControllerBot.teamColor);// true or false

                        //and cell
                        RandomCellifBotChoose(ControllerBot.teamColor);
                    }
                }
            }
            else
            {
                //////////////////////////
                ///Kiểm tra xem teamcolor có phải là cam hay không
                ///Nếu là CAM
                ///Kiểm tra xem đã đặt con cờ nào chưa
                ///Nếu chưa -> đặt Long vào 2 ô 1 hoặc 7 của hàng của mình
                ///Nếu rồi -> đặt con cờ còn lại vào ô 1 hoặc 7
                ///Nếu là XANH thì kiểm tra xem con đầu tiên được đặt ở bên CAM là con nào, nếu là  Long thì đặt đối diện nó
                ///Nếu là phượng thì đặt ô 1 hoặc 7 
                ///Thay vì mũi tên thì sẽ hight light những cell có thể đặt lên
                ////////////////////////
                if (team)
                    panelC.transform.DOLocalMoveY(openYC, 0.5f);
                else
                    panelX.transform.DOLocalMoveY(openYX, 0.5f);
                if (GameController.playMode == PlayMode.SinglePlay)
                {
                    //if (typeSetup == TypeSetUp.beginnerRule)
                    //{
                    //    if (team == ControllerBot.teamColor)
                    //    {
                    //        int value = new int();
                    //        //bot choose piece 
                    //        if (PiecesValues.Count > 1)// 2 value
                    //        {
                    //            int index = Random.Range(0, 2);
                    //            value = PiecesValues[index];
                    //            PiecesValues.Remove(index);
                    //        }
                    //        else
                    //        {
                    //            value = PiecesValues[0];
                    //        }

                    //        ClickItem(value); // 0 or 1
                    //        ClickTeam(ControllerBot.teamColor);// true or false

                    //        //and cell
                    //        RandomCellifBotChoose(ControllerBot.teamColor);
                    //    }
                    //}
                    if (typeSetup == TypeSetUp.mainRule)
                    {
                        if (team == ControllerBot.teamColor)
                        {
                            int value = new int();
                            if (PiecesValues.Count > 1)// 2 value
                            {
                                int index = Random.Range(0, 2);
                                value = PiecesValues[index];
                                PiecesValues.Remove(index);
                            }
                            else
                            {
                                value = PiecesValues[0];
                            }

                            ClickItem(value); // 0 or 1
                            ClickTeam(ControllerBot.teamColor);// true or false

                            //and cell
                            RandomCellifBotChoose(ControllerBot.teamColor);
                        }
                    }
                }

            }
        }
        else
        {
            if (typeSetup == TypeSetUp.beginnerRule)
            {
                if (team)
                {
                    panelC.transform.DOLocalMoveY(closeYC, 0.5f);
                    OnComplete();
                }
                else
                {
                    panelX.transform.DOLocalMoveY(closeYX, 0.5f);
                    OnComplete();
                }
            }
            if (typeSetup == TypeSetUp.mainRule || typeSetup == TypeSetUp.hardRule)
            {

                if (team)
                {
                    panelC.transform.DOLocalMoveY(closeYC, 0.5f);
                    OnComplete();
                }
                else
                {
                    panelX.transform.DOLocalMoveY(closeYX, 0.5f);
                    OnComplete();
                }
            }
        }
    }
    public void ResetPieceTeamUp(bool teamColor, int itemP)
    {
        if (teamColor)//teamC
        {
            ItemsC[itemP].use = false;
        }
        else
        {
            ItemsX[itemP].use = false;
        }
    }
    public void ResetPieceandCell(bool teamColor ,int itemP,Cell cell)
    {
        if (teamColor)//teamC
        {
            ItemsC[itemP].use = false;
            if (cell)
            {
                Cells check;
                check = CellsC.Find(x => x.cell == cell);
                if (check != null) check.use = false;
            }
            if (cell.mCurrentPiece)
            {
                Items check;
                check = ItemsC.Find(x => x.item.mNamePiece == cell.mCurrentPiece.mNamePiece);
                if (check != null)
                {
                    check.use = false;
                }
            }
        }
        else
        {
            ItemsX[itemP].use = false;
            if (cell)
            {
                Cells check;
                check = CellsX.Find(x => x.cell == cell);
                if (check != null) check.use = false;
            }
            if (cell.mCurrentPiece)
            {
                Items check;
                check = ItemsX.Find(x => x.item == cell.mCurrentPiece);
                if (check != null)
                {
                    check.use = false;
                }
            }
        }
    }
    private bool CheckListButton(List<GameObject> buttons)
    {
        int value = 0;
        for (int i = 0; i < buttons.Count; i++)
        {
            if (buttons[i].activeSelf) value++;
        }
        if (value == 2) return true;
        return false;
    }
    public void ResetUI(bool teamC, GameObject button,int stt)
    {
        if (teamC)
        {
            button.SetActive(true);

            if (CheckListButton(buttonItemC))
            {
                for (int i = 0; i < buttonItemC.Count; i++)
                {
                    if (i % 2 == 0)
                        buttonItemC[i].transform.localPosition = new Vector3(-190, buttonItemC[i].transform.localPosition.y);
                    else
                        buttonItemC[i].transform.localPosition = new Vector3(190, buttonItemC[i].transform.localPosition.y);
                }
            }
            else
            {
                for (int i = 0; i < buttonItemC.Count; i++)
                {
                    if (buttonItemC[i].activeSelf)
                    buttonItemC[i].transform.localPosition = new Vector3(0, buttonItemC[i].transform.localPosition.y);
                }
            }
            panelX.transform.DOLocalMoveY(closeYX, 0.5f);
            if (stt == 141096) return;   ArrowC[stt].SetActive(true);
        }
        else
        {
            button.SetActive(true);

            if (CheckListButton(buttonItemX))
            {
                for (int i = 0; i < buttonItemX.Count; i++)
                {
                    buttonItemX[i].SetActive(true);
                    if (i % 2 == 0)
                        buttonItemX[i].transform.localPosition = new Vector3(-190, buttonItemX[i].transform.localPosition.y);
                    else
                        buttonItemX[i].transform.localPosition = new Vector3(190, buttonItemX[i].transform.localPosition.y);
                }
            }
            else
            {
                for (int i = 0; i < buttonItemX.Count; i++)
                {
                    if (buttonItemX[i].activeSelf)
                    buttonItemX[i].transform.localPosition = new Vector3(0, buttonItemX[i].transform.localPosition.y);
                }
            }
            panelC.transform.DOLocalMoveY(closeYC, 0.5f);
            if (stt == 141096) return; ArrowX[stt].SetActive(true);
        }
    }
    private void OnComplete()
    {
        if (teamC)
        {
            buttonItemC[itemP].SetActive(false);
            if (itemP == 0) buttonItemC[1].transform.localPosition = new Vector3(0, buttonItemC[1].transform.localPosition.y);
            else buttonItemC[0].transform.localPosition = new Vector3(0, buttonItemC[0].transform.localPosition.y);
        }
        else
        {
            buttonItemX[itemP].SetActive(false);
            if (itemP == 0) buttonItemX[1].transform.localPosition = new Vector3(0, buttonItemX[1].transform.localPosition.y);
            else buttonItemX[0].transform.localPosition = new Vector3(0, buttonItemX[0].transform.localPosition.y);
        }
    }
    public void SetItems(bool team,BasePieces standPiece, BasePieces movePiece)
    {
        if (team)
        {
            if(standPiece.mNamePiece == "Long")
            {
                ItemsC[0].item = movePiece ;
                ItemsC[1].item = standPiece;
            }
            else
            {
                ItemsC[1].item = standPiece;
                ItemsC[0].item = movePiece;
            }
        }
        else
        {
            if (standPiece.mNamePiece == "Long")
            {
                ItemsX[1].item = standPiece;
                ItemsX[0].item = movePiece;
            }
            else
            {
                ItemsX[1].item = movePiece;
                ItemsX[0].item = standPiece;
            }
        }
    }
}


