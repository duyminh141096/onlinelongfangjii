﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum GameMode {
    Ready = 0,
    Play = 1
}


public class GameManager : MonoBehaviour
{
    public GameMode Mode;
    // Start is called before the first frame update
    public Board board;

    public PiecesManager mPiecesManager;
    public StorePieces StorePieces;
    public static GameManager Instance;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Mode = GameMode.Ready;

        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;

        board.Create();
        //board.CreateBlackBoard(1, 7, board.mBlackBoard);
        //board.CreateWhiteBoard(1, 7, board.mWhiteBoard);

        //board.UpdateCountList(true);
        //board.UpdateCountList(false);

        mPiecesManager.SetUp(board);

        mPiecesManager.Spawn(true, 3, board.whiteBoard);
        mPiecesManager.Spawn(false, 1, board.blackBoard);

        StorePieces.UpdateText();
    }

}
