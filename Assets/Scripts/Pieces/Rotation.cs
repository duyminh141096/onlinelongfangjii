﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.EventSystems;
using UnityEngine.UI;



public class Rotation : MonoBehaviour//, IPointerDownHandler
{/*
    public Button mRightButton;

    public Button mLeftButton;

    public BasePieces BasePieces;

    private Image mImage;

    public TeamUpList teamuplist;

    private void Awake()
    {

        mImage = gameObject.GetComponent<Image>();

        BasePieces = gameObject.GetComponent<BasePieces>();
        teamuplist = gameObject.GetComponent<TeamUpList>();
        ////Debug.Log("My team color " + BasePieces.mColor);

        StartCoroutine("Setup");

    }

    IEnumerator Setup()
    {
        yield return new WaitForSeconds(0.4f);

        if (BasePieces.mColor)
        {
            mRightButton = GameObject.Find("RightButtonWhite").GetComponent<Button>();

            mLeftButton = GameObject.Find("LeftButtonWhite").GetComponent<Button>();

        }
        else
        {
            mRightButton = GameObject.Find("RightButtonBlack").GetComponent<Button>();

            mLeftButton = GameObject.Find("LeftButtonBlack").GetComponent<Button>();
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        mRightButton.onClick.RemoveAllListeners();

        mRightButton.onClick.AddListener(ChangeRotationRight);

        mLeftButton.onClick.RemoveAllListeners();

        mLeftButton.onClick.AddListener(ChangeRotationLeft);

    }

   IEnumerator ActiveButton()
    {
        yield return new WaitForSeconds(0.5f);

        mRightButton.onClick.RemoveAllListeners();

        mRightButton.onClick.AddListener(ChangeRotationRight);

        mLeftButton.onClick.RemoveAllListeners();

        mLeftButton.onClick.AddListener(ChangeRotationLeft);
    }

    public void EnableImage(bool isEnable)
    {
        mImage.enabled = isEnable;
    }

    private void ChangeRotationLeft()
    {
        int pastState = BasePieces.MStateRotation;

        if (!BasePieces.isDropPiece)
        {
            if (PiecesManager.rotation)
            {
                RotationPiece command = new RotationPiece(BasePieces, pastState);

                UndoManager.instance.InsertCommand(command);

                PiecesManager.rotation = false;
            }

            PiecesManager.instance.SwitchSides(!BasePieces.mColor);

            GameController.instance.ShowUIRotation(false, BasePieces.mColor);

            //deactive 
            GameController.instance.UI_PassTurn_White_Active(false);
        }
      
        switch (BasePieces.MStateRotation)
        {
            case 1:
                BasePieces.MStateRotation = 4;
            
                BasePieces.GetandSetStateRotaion(BasePieces.MStateRotation);
                mLeftButton.onClick.RemoveAllListeners();
                break;
            case 2:
                BasePieces.MStateRotation = 1;
            
                BasePieces.GetandSetStateRotaion(BasePieces.MStateRotation);
                mLeftButton.onClick.RemoveAllListeners();
                break;
            case 3:
                BasePieces.MStateRotation = 2;
           
                BasePieces.GetandSetStateRotaion(BasePieces.MStateRotation);
                mLeftButton.onClick.RemoveAllListeners();
                break;
            case 4:
                BasePieces.MStateRotation = 3;
         
                BasePieces.GetandSetStateRotaion(BasePieces.MStateRotation);
                mLeftButton.onClick.RemoveAllListeners();
                break;
        }

        //teamuplist.UpdateRotation(BasePieces.MStateRotation);

        GameController.instance.mCurentBasePiece.ClearCells();

        GameController.instance.mTargetCell = null;

        GameController.instance.ClearListHightLightCell();

        GameController.instance.SetState(State.None);

        if(BasePieces.isDropPiece)
        {
            mRightButton.onClick.RemoveAllListeners();

            mRightButton.onClick.AddListener(ChangeRotationRight);

            mLeftButton.onClick.RemoveAllListeners();

            mLeftButton.onClick.AddListener(ChangeRotationLeft);
        }

        // GameController.instance.ShowUIRotation(false, BasePieces.mColor);

        GameController.instance.ShowUIRotation(false);
        
    }

    private void ChangeRotationRight()
    {
        int pastState = BasePieces.MStateRotation;

        if (!BasePieces.isDropPiece)
        {
            if (PiecesManager.rotation)
            {
                RotationPiece command = new RotationPiece(BasePieces, pastState);

                UndoManager.instance.InsertCommand(command);

                PiecesManager.rotation = false;
            }

            PiecesManager.instance.SwitchSides(!BasePieces.mColor);

            GameController.instance.ShowUIRotation(false, BasePieces.mColor);

            GameController.instance.UI_PassTurn_White_Active(false);
        }
        switch (BasePieces.MStateRotation)
        {
            case 1:
               ////Debug.Log("Change State Rotation");
                BasePieces.MStateRotation = 2;
                //Xoay hay lam gi do
                BasePieces.GetandSetStateRotaion(BasePieces.MStateRotation);
                mRightButton.onClick.RemoveAllListeners();
                break;
            case 2:
                BasePieces.MStateRotation = 3;
                //Xoay hay lam gi do
                BasePieces.GetandSetStateRotaion(BasePieces.MStateRotation);
                mRightButton.onClick.RemoveAllListeners();
                break;
            case 3:
                BasePieces.MStateRotation = 4;
                //Xoay hay lam gi do
                BasePieces.GetandSetStateRotaion(BasePieces.MStateRotation);
                mRightButton.onClick.RemoveAllListeners();
                break;
            case 4:
                BasePieces.MStateRotation = 1;
                //Xoay hay lam gi do
                BasePieces.GetandSetStateRotaion(BasePieces.MStateRotation);
                mRightButton.onClick.RemoveAllListeners();
                break;
        }
        //teamuplist.UpdateRotation(BasePieces.MStateRotation);

        GameController.instance.mCurentBasePiece.ClearCells();

        GameController.instance.mTargetCell = null;

        GameController.instance.ClearListHightLightCell();

        GameController.instance.SetState(State.None);

        if (BasePieces.isDropPiece)
        {
            mRightButton.onClick.RemoveAllListeners();

            mRightButton.onClick.AddListener(ChangeRotationRight);

            mLeftButton.onClick.RemoveAllListeners();

            mLeftButton.onClick.AddListener(ChangeRotationLeft);
        }

        GameController.instance.ShowUIRotation(false);
    }
    */
}
