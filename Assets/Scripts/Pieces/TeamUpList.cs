﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PieceInfo
{
    public string nPieceName;

    public int mPieceRotation;

    public PieceInfo(string mpiecename, int mpiecesrotation)
    {
        nPieceName = mpiecename;

        mPieceRotation = mpiecesrotation;
    }
    public PieceInfo Clone()
    {
        PieceInfo clone = new PieceInfo(nPieceName, mPieceRotation);
        return clone;
    }
}



public class TeamUpList : MonoBehaviour
{

    public int score; 

    public BasePieces basePieces;

    public List<PieceInfo> mPiecesInfo = new List<PieceInfo>();

    public void UpdateScore() {
        score = 0;
        if(mPiecesInfo.Count > 0)
        {
            foreach (var item in mPiecesInfo)
            {
                score += ScorePiece.instance.GetScorePiece(item.nPieceName);
            }
        }
    }

    public List<PieceInfo> Clone()
    {
        List<PieceInfo> clone = new List<PieceInfo>();
        for (int i = 0; i < mPiecesInfo.Count; i++)
        {
            clone.Add(mPiecesInfo[i].Clone());
        }
        return clone;
    }

    public List<PieceInfo> GetPieceNamesList()
    {
        List<PieceInfo> clone = new List<PieceInfo>();
        for (int i = 0; i < mPiecesInfo.Count; i++)
        {
            clone.Add(mPiecesInfo[i].Clone());
        }
        return clone;
    }

    void Start()
    {
        basePieces = gameObject.GetComponent<BasePieces>();
        UpdateScore();
        //AddPiece(basePieces.mNamePiece, basePieces.MStateRotation);
    }

    public void AddPiece(string pieceName,int stateRotation)
    {
 
        if (pieceName != null)
        {
            mPiecesInfo.Insert(0, new PieceInfo(pieceName,stateRotation));
            UpdateScore();
        }
    }

    public void AddPiecesList(List<PieceInfo> listPieces)
    {
        foreach (PieceInfo piece in listPieces)
        {
            if (piece != null)
            {
                mPiecesInfo.Add(piece);
            }
        }
        UpdateScore();
    }
    public void AddNewPiecesList(List<PieceInfo> listPieces)
    {
        mPiecesInfo.Clear();
        foreach (PieceInfo piece in listPieces)
        {
            if (piece != null)
            {
                mPiecesInfo.Add(piece);
            }
        }
        UpdateScore();
    }

    public void RemovePieceList()
    {
        mPiecesInfo.RemoveAt(0);
        UpdateScore();
    }

    public void UpdateRotation(int rotation)
    {
        mPiecesInfo[0].mPieceRotation = rotation;
    }

    public void UpdateListRotate(bool left)
    {
        if (left) UpdateListRotateLeft();
        else UpdateListRotateRight();
    }

    private void UpdateListRotateLeft()
    {
        for (int i = 0; i < mPiecesInfo.Count; i++)
        {
            mPiecesInfo[i].mPieceRotation = GetLeftdir(mPiecesInfo[i].mPieceRotation);
        }
    }

    private int GetLeftdir(int dir)
    {
        switch (dir)
        {
            case 1:
                return 4 ;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            default:
                return 1;
        }
    }

    private void UpdateListRotateRight()
    {
        for (int i = 0; i < mPiecesInfo.Count; i++)
        {
            mPiecesInfo[i].mPieceRotation = GetRightdir(mPiecesInfo[i].mPieceRotation);
        }
    }

    private int GetRightdir(int dir)
    {
        switch (dir)
        {
            case 1:
                return 2;
            case 2:
                return 3;
            case 3:
                return 4;
            case 4:
                return 1;
            default:
                return 1;
        }
    }

    public string GetPieceList()
    {
        return mPiecesInfo[0].nPieceName;
    }

    public int GetPieceListRotation()
    {
        return mPiecesInfo[0].mPieceRotation;
    }

    public int GetListNameCount()
    {
        return mPiecesInfo.Count;
    }


}
