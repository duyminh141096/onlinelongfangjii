/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: BasePieces.bot.cs
* Script Author: MinhLe 
* Created On: 7/10/2020 12:26:58 AM*/
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class BasePieces
{

    public void CheckPathifPieceisUI()
    {
        mHightlightedCells.Clear();
        for (int y = 0; y < 9; y++)
        {
            for (int x = 0; x < 9; x++)
            {
                CellState cellState = CellState.None;
                cellState = Board.instance.ValidateCell(x, y, this);

                if (cellState == CellState.Friendly || cellState == CellState.Free)
                {
                    mHightlightedCells.Add(Board.instance.AllCells[x, y]);
                }
            }
        }
    }
    public List<Cell> GetListCellLongPhuongXa()
    {
        List<Cell> listCell = new List<Cell>();
        switch (mNamePiece)
        {
            case "Xa":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:
                            CreateCellPathBot(0, -1, 8, listCell,true);
                            break;
                        case 2:
                            CreateCellPathBot(-1, 0, 8, listCell, true);
                            break;
                        case 3:
                            CreateCellPathBot(0, 1, 8, listCell, true);
                            break;
                        case 4:
                            CreateCellPathBot(1, 0, 8, listCell, true);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case "Phuong":
                if (mIsPlace)
                {
                    CreateCellPathBot(-1, 1, 9, listCell, true);
                    CreateCellPathBot(1, 1, 9, listCell, true);

                    CreateCellPathBot(-1, -1,9, listCell, true);
                    CreateCellPathBot(1, -1, 9, listCell, true);
                }
                break;
            case "Long":
                if (mIsPlace)
                {
                    CreateCellPathBot(1, 0,9, listCell, true);
                    CreateCellPathBot(-1, 0, 9, listCell, true);

                    //vertical
                    CreateCellPathBot(0, 1, 9, listCell, true);
                    CreateCellPathBot(0, -1, 9, listCell, true);
                }
                break;
        }
        return listCell;
    }

    protected void CreateCellPathBot(int xDirection, int yDirection, int movement, List<Cell> listCells, bool special = false) 
    {
        //target position
        bool team = false;
        if (mListPiece.GetListNameCount() > 1)
        {
            // tu 2 tro len
            // la 1 doi nen k add hight light them
            team = true;
        }

        // //Debug.Log("Create Cell Path");
       // if (mCurrentCell == null) return;
        int currentX = mCurrentCell.mBoardPosition.x;
        int currentY = mCurrentCell.mBoardPosition.y;

        for (int i = 0; i < movement; i++)
        {
            currentX += xDirection;

            currentY += yDirection;

            //add cellState and check validate
            CellState cellState = CellState.None;
            cellState = mCurrentCell.mBoard.ValidateCell(currentX, currentY, this);
            if (special)
            {
                if(cellState != CellState.None && cellState != CellState.OutofBounds)
                {
                    listCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
                    continue;
                }
            }
            //if enemy, add to list, break
            if (cellState == CellState.Enemy)
            {
                listCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
                break;
            }
            if (exp)
            {
                if (cellState == CellState.Friendly || cellState == CellState.Free) {
                    listCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
                        continue; }
            }
            if (!team)
            {
                if (cellState == CellState.Friendly)
                {
                    if (mNamePiece != "Vuong")
                    {
                        listCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
                        break;
                    }
                }
            }
            if (cellState != CellState.Free)
            {
                break;
            }

            listCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
        }
    }
    public void CreatePathKiBot(int dir, int xDirection, int yDirection, List<Cell> listCells)
    {

        bool team = false;
        if (mListPiece.GetListNameCount() > 1)
        {
            team = true;
        }
        int currentX = mCurrentCell.mBoardPosition.x;
        int currentY = mCurrentCell.mBoardPosition.y;

        currentX += xDirection;
        currentY += yDirection;

        CellState cellState = CellState.None;
        cellState = mCurrentCell.mBoard.ValidateCell(currentX, currentY, this);

        //if enemy, add to list, break
        if (cellState == CellState.Enemy)
        {
            listCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
            return;
        }
        if (!team)
        {
            if (cellState == CellState.Friendly)
            {
                if (mNamePiece != "Vuong")
                {
                    listCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
                    return;
                }
            }
        }
        if (cellState != CellState.Free)
        {
            return;
        }
        listCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
    }
    bool exp = true;
    public List<Cell> CheckPathingBot(bool Exp = false)
    {
        exp = Exp;
        List<Cell> cells = new List<Cell>();
        if (kind == Kind.UI)
        {
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    //add cellState and check validate
                    CellState cellState = Board.instance.ValidateCell(x, y, this);

                    //if enemy, add to list, break
                    if (cellState == CellState.Friendly || cellState == CellState.Free)
                    {
                        cells.Add(Board.instance.AllCells[x, y]);
                    }
                }
            }
            return cells;
        }
        else
        {
            switch (mNamePiece)
            {
                case "Binh":
                    if (mIsPlace)
                    {
                        switch (MStateRotation)
                        {
                            case 1:
                                CreateCellPathBot(0, -1, Mathf.Abs(mMovement.y),cells);
                                break;
                            case 2:
                                CreateCellPathBot(-1, 0, Mathf.Abs(mMovement.y), cells);
                                break;
                            case 3:
                                CreateCellPathBot(0, 1, Mathf.Abs(mMovement.y), cells);
                                break;
                            case 4:
                                CreateCellPathBot(1, 0, Mathf.Abs(mMovement.y), cells);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case "Ki":
                    if (mIsPlace)
                    {
                        switch (MStateRotation)
                        {
                            case 1:
                                CreatePathKiBot(3, -1, -2,cells);

                                CreatePathKiBot(3, 1, -2, cells);

                                CreatePathKiBot(3, 0, 2, cells);

                                CreatePathKiBot(3, 2, 2, cells);

                                CreatePathKiBot(3, -2, 2, cells);

                                break;
                            case 2:
                                CreatePathKiBot(4, -2, -1, cells);

                                CreatePathKiBot(4, -2, 1, cells);

                                CreatePathKiBot(4, 2, 0, cells);

                                CreatePathKiBot(4, 2, -2, cells);

                                CreatePathKiBot(4, 2, 2, cells);



                                break;
                            case 3:
                                CreatePathKiBot(1, -1, 2, cells);

                                CreatePathKiBot(1, 1, 2, cells);

                                CreatePathKiBot(1, -2, -2, cells);

                                CreatePathKiBot(1, 2, -2, cells);

                                CreatePathKiBot(1, 0, -2, cells);


                                break;
                            case 4:
                                CreatePathKiBot(2, 2, 1, cells);

                                CreatePathKiBot(2, 2, -1, cells);

                                CreatePathKiBot(2, -2, 0, cells);

                                CreatePathKiBot(2, -2, 2, cells);

                                CreatePathKiBot(2, -2, -2, cells);

                                break;

                        }
                    }
                    break;
                case "Phuong":
                    if (mIsPlace)
                    {
                        CreateCellPathBot(-1, 1, mMovement.z, cells);
                        CreateCellPathBot(1, 1, mMovement.z, cells);

                        CreateCellPathBot(-1, -1, mMovement.z, cells);
                        CreateCellPathBot(1, -1, mMovement.z, cells);
                    }
                    break;
                case "Long":
                    if (mIsPlace)
                    {
                        CreateCellPathBot(1, 0, mMovement.x, cells);
                        CreateCellPathBot(-1, 0, mMovement.x, cells);

                        //vertical
                        CreateCellPathBot(0, 1, mMovement.y, cells);
                        CreateCellPathBot(0, -1, mMovement.y, cells);
                    }
                    break;
                case "Tuong":
                    if (mIsPlace)
                    {
                        switch (MStateRotation)
                        {
                            case 1:

                                CreateCellPathBot(1, 0, mMovement.x, cells);
                                CreateCellPathBot(-1, 0, mMovement.x, cells);


                                CreateCellPathBot(0, 1, mMovement.y, cells);
                                CreateCellPathBot(0, -1, mMovement.y, cells);


                                CreateCellPathBot(-1, -1, mMovement.z, cells);


                                CreateCellPathBot(1, -1, mMovement.z, cells);

                                break;
                            case 2:
                                CreateCellPathBot(1, 0, mMovement.x, cells);
                                CreateCellPathBot(-1, 0, mMovement.x, cells);

                                //vertical
                                CreateCellPathBot(0, 1, mMovement.y, cells);
                                CreateCellPathBot(0, -1, mMovement.y, cells);


                                CreateCellPathBot(-1, 1, mMovement.z, cells);


                                CreateCellPathBot(-1, -1, mMovement.z, cells);

                                break;
                            case 3:


                                CreateCellPathBot(1, 0, mMovement.x, cells);
                                CreateCellPathBot(-1, 0, mMovement.x, cells);
                                CreateCellPathBot(0, 1, mMovement.y, cells);
                                CreateCellPathBot(0, -1, mMovement.y, cells);
                                CreateCellPathBot(-1, 1, mMovement.z, cells);
                                CreateCellPathBot(1, 1, mMovement.z, cells);
                                break;
                            case 4:

                                CreateCellPathBot(1, 0, mMovement.x, cells);
                                CreateCellPathBot(-1, 0, mMovement.x, cells);

                                //vertical
                                CreateCellPathBot(0, 1, mMovement.y, cells);
                                CreateCellPathBot(0, -1, mMovement.y, cells);

                                CreateCellPathBot(1, 1, mMovement.z, cells);

                                CreateCellPathBot(1, -1, mMovement.z, cells);
                                break;
                        }
                    }
                    break;
                case "Si":
                    if (mIsPlace)
                    {
                        switch (MStateRotation)
                        {
                            case 1:


                                CreateCellPathBot(0, -1, mMovement.y,cells);

                                //duong cheo tren trai
                                CreateCellPathBot(-1, 1, mMovement.z, cells);
                                //duong cheo tren phai
                                CreateCellPathBot(1, 1, mMovement.z, cells);

                                //duong cheo duoi trai
                                CreateCellPathBot(-1, -1, mMovement.z, cells);

                                //duong cheo duoi phai
                                CreateCellPathBot(1, -1, mMovement.z, cells);
                                break;
                            case 2:

                                CreateCellPathBot(-1, 0, mMovement.x, cells);

                                //duong cheo tren trai
                                CreateCellPathBot(-1, 1, mMovement.z, cells);
                                //duong cheo tren phai
                                CreateCellPathBot(1, 1, mMovement.z, cells);

                                //duong cheo duoi trai
                                CreateCellPathBot(-1, -1, mMovement.z, cells);

                                //duong cheo duoi phai
                                CreateCellPathBot(1, -1, mMovement.z, cells);
                                break;
                            case 3:

                                CreateCellPathBot(0, 1, mMovement.y, cells);

                                //duong cheo tren trai
                                CreateCellPathBot(-1, 1, mMovement.z, cells);
                                //duong cheo tren phai
                                CreateCellPathBot(1, 1, mMovement.z, cells);

                                //duong cheo duoi trai
                                CreateCellPathBot(-1, -1, mMovement.z, cells);

                                //duong cheo duoi phai
                                CreateCellPathBot(1, -1, mMovement.z, cells);
                                break;
                            case 4:


                                CreateCellPathBot(-1, 1, mMovement.z, cells);

                                CreateCellPathBot(1, 1, mMovement.z, cells);


                                CreateCellPathBot(-1, -1, mMovement.z, cells);


                                CreateCellPathBot(1, -1, mMovement.z, cells);

                                CreateCellPathBot(1, 0, mMovement.z, cells);
                                break;

                        }
                    }
                    break;
                case "Vuong":
                    if (mIsPlace)
                    {
                        CreateCellPathBot(1, 0, mMovement.x, cells);
                        CreateCellPathBot(-1, 0, mMovement.x, cells);


                        CreateCellPathBot(0, 1, mMovement.y, cells);
                        CreateCellPathBot(0, -1, mMovement.y, cells);


                        CreateCellPathBot(-1, 1, mMovement.z, cells);

                        CreateCellPathBot(1, 1, mMovement.z, cells);

                        CreateCellPathBot(-1, -1, mMovement.z, cells);

                        CreateCellPathBot(1, -1, mMovement.z, cells);
                    }
                    break;
                case "Xa":
                    if (mIsPlace)
                    {
                        switch (MStateRotation)
                        {
                            case 1:
                                CreateCellPathBot(0, -1, Mathf.Abs(mMovement.y), cells);
                                break;
                            case 2:
                                CreateCellPathBot(-1, 0, Mathf.Abs(mMovement.y), cells);
                                break;
                            case 3:
                                CreateCellPathBot(0, 1, Mathf.Abs(mMovement.y), cells);
                                break;
                            case 4:
                                CreateCellPathBot(1, 0, Mathf.Abs(mMovement.y), cells);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
            }
        }
        return cells;
    }


    bool GetDir(int dir, int currrentDir)
    {
        bool returnValue = false;
        Vector2 returnVector = new Vector2();
        switch (dir)
        {
            case 1:
                returnVector=  new Vector2(4, 2);
                break;
            case 2:
               returnVector= new Vector2(1, 3);
                break;
            case 3:
               returnVector= new Vector2(2, 4);
                break;
            case 4:
                returnVector =new Vector2(3, 1);
                break;
        }
        if (returnVector.x == currrentDir) return returnValue= true;
        if (returnVector.y == currrentDir) return returnValue= true;
        return returnValue;
    }
    public DataCell CheckPathingAtCell(Cell targetCell)
    {
        int dir = this.MStateRotation;
        //clone, asign cell , check path;
        List<DataCell> cellList = new List<DataCell>();
        
        for (int i = 1; i < 5; i++)
        {
            BasePieces clone = (BasePieces)this.Clone();
            clone.mCurrentCell = targetCell;
            clone.MStateRotation = i;
            clone.kind = Kind.Normal;
            clone.mIsPlace = true;
            List<Cell> clonelistCell = new List<Cell>();
            clonelistCell = clone.CheckPathingBot();
            if (clonelistCell.Count > 0)
            {
                for (int y = 0; y < clonelistCell.Count; y++)
                {
                    if (clonelistCell[y].mCurrentPiece)
                    {
                        if(kind == Kind.Normal)
                        {
                            if (clonelistCell[y].mCurrentPiece.mColor != mColor)
                            {
                                if (GetDir(dir, i))
                                {
                                    DataCell datateturn = new DataCell(targetCell, 0, 0,this);
                                    datateturn.cell = targetCell;
                                    datateturn.dir = i;
                                    datateturn.score = clonelistCell[y].mCurrentPiece.mListPiece.score;
                                    cellList.Add(datateturn);
                                }
                            }
                        }
                        else
                        {
                            if (clonelistCell[y].mCurrentPiece.mColor != mColor)
                            {
                                DataCell datateturn = new DataCell(targetCell, 0, 0, this);
                                datateturn.cell = targetCell;
                                datateturn.dir = i;
                                datateturn.score = clonelistCell[y].mCurrentPiece.mListPiece.score;
                                cellList.Add(datateturn);
                            }
                        }
                    }
                }
            }
        }
        cellList = cellList.OrderByDescending(x => x.score).ToList();
        if (cellList.Count > 0) return cellList[0];
        DataCell data = new DataCell(null, 0, 0, this);
        return data;
    }

    public List<DataCell> CheckPathContainCell(Cell currentCell, Cell targetCell, List<Calculator> enemyList)
    {
        int dir = this.MStateRotation;
        //clone, asign cell , check path;
        List<DataCell> cellList = new List<DataCell>();

        for (int i = 1; i < 5; i++)
        {
            BasePieces clone = (BasePieces)this.Clone();
            clone.mCurrentCell = currentCell;
            clone.MStateRotation = i;
            clone.kind = Kind.Normal;
            clone.mIsPlace = true;
            List<Cell> clonelistCell = new List<Cell>();
            clonelistCell = clone.CheckPathingBot();
            if (clonelistCell.Count > 0)
            {
                if (clonelistCell.Contains(targetCell) )
                {
                    bool checkContain = false;
                    for (int j = 0; j < enemyList.Count; j++)
                    {
                        if (enemyList[j].listCells.Contains(targetCell))
                            checkContain = true;
                    }
                    if (!checkContain)
                    {
                        DataCell returnData = new DataCell(currentCell, clone.mListPiece.score, i, currentCell.mCurrentPiece);
                        cellList.Add(returnData);
                    }
                }
            }
        }
        if (cellList.Count > 0) return cellList;
        return null;
    }

}
