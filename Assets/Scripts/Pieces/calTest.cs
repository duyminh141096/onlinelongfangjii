/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: calTest.cs
* Script Author: MinhLe 
* Created On: 7/24/2020 1:27:10 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class calTest : MonoBehaviour
{
    [HideInInspector]
    public Calculator calculator;
    public TeamUpList scorePiece;
    bool teamColor = false;
    public bool subcribe = false;
    private void Start()
    {
        scorePiece = GetComponent<TeamUpList>();
        teamColor = GetComponent<BasePieces>().mColor;
        calculator.SetDefaultValue(GetComponent<BasePieces>());
        //Subcribe();
        subcribe = true;
    }
    public void Subcribe()
    {
        if (GetComponent<BasePieces>().kind == Kind.Normal)
        {
            this.transform.SetParent(ControllerBot.Instance.parentItemNormal.transform);
        }
       subcribe = true;
        //Minimax.OnCalculate += Calculate;
       // Minimax.ReturnScorePiece += ScoreThisPiece;
    }

    public void Unsubcribe()
    {
       if(GetComponent<BasePieces>().kind == Kind.Normal) this.transform.SetParent(ControllerBot.Instance.parentItemFake.transform);
        subcribe = false;
        //Minimax.OnCalculate -= Calculate;
     //Minimax.ReturnScorePiece -= ScoreThisPiece;
    }

 
    public int ScoreThisPiece()
    {
        if (!subcribe) 
           return 0;
        scorePiece.UpdateScore();
        if (GetComponent<BasePieces>().kind == Kind.UI) return 0;
        return teamColor == true ? scorePiece.score : -scorePiece.score; //ControllerBot.teamColor == teamColor ?  scorePiece.score : -scorePiece.score;
    }
}
