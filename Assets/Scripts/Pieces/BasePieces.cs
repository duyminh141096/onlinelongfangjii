﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public partial class BasePieces : MonoBehaviour, IPointerDownHandler
{
 
    public Kind kind;

    public string mNamePiece; //SetUp

    public bool mColor;

    public Image parentImage; //need set in INSPECTOR

    public Image childImage;//need set in INSPECTOR

    protected Cell mOriginalCell = null;

    public Cell mCurrentCell = null;

    protected RectTransform RectTransform = null;

    protected PiecesManager PiecesManager;

    public Cell mTargetCell = null;

    public Vector3Int mMovement = Vector3Int.one;
    [HideInInspector]
    public List<Cell> mHightlightedCells = new List<Cell>();

    //up = 1  ; right = 2 ; down = 3 ; left = 4

    public int MStateRotation;

    protected Color32 pieceColor;

    public bool mIsPlace;

    public int mID;

    public Rotation rotationCS;

    public TeamUpList mListPiece;

    //===============
    public bool canRotation;

    public bool canTeamUp;

    public Text mCountText;

    public bool isDropPiece = false;

    //============

    public void UpdateStatusPiece()
    {
        if(mListPiece.GetListNameCount() > 1)
        {
            canTeamUp = false;
        }
        else
        {
            canTeamUp = true;
        }
    }

    public void UpdateKind(Kind kind)
    {
        this.kind = kind;
    }

    public int ScorePieces()
    {
        return mListPiece.score;
    }

    private void Update()
    {
        if (mListPiece.GetListNameCount() > 1)
        {
            mCountText.text = "" + mListPiece.GetListNameCount();
        }
        else
        {
            mCountText.text = "";
        }
    }

    public void SetUp(bool newTeamColor, PiecesManager newpiecesManager, int stateRotation, bool isPlace, string namePiece, Sprite parentPrite, Sprite childSprite, bool isDrop, Kind kind)
    {
        Color32 newSpriteColor;
        if (newTeamColor)
        {
            newSpriteColor = new Color32(255, 176, 93, 255);
        }
        else
        {
            newSpriteColor = new Color32(58, 241, 56, 255);
        }
        this.kind = kind;
        childImage = gameObject.transform.GetChild(0).GetComponent<Image>();
        parentImage.sprite = parentPrite;
        childImage.sprite = childSprite;
        PiecesManager = newpiecesManager;
        mColor = newTeamColor;
        GetComponent<Image>().color = newSpriteColor;
        RectTransform = GetComponent<RectTransform>();
        MStateRotation = stateRotation;
        mIsPlace = isPlace;
        mNamePiece = namePiece;
        isDropPiece = isDrop;
        mListPiece = gameObject.GetComponent<TeamUpList>();
        mListPiece.AddPiece(mNamePiece, stateRotation);
        ////Debug.Log("SetUp Piece with isPlace = " + mIsPlace);

        switch (mNamePiece)
        {
            case "Binh":
                mMovement = mColor == true ? new Vector3Int(0, 1, 0) : new Vector3Int(0, -1, 0);
                break;
            case "Phuong":
                mMovement = new Vector3Int(0, 0, 9);
                break;
            case "Long":
                mMovement = new Vector3Int(9, 9, 0);
                break;
            case "Tuong":
                mMovement = new Vector3Int(1, 1, 1);
                break;
            case "Si":
                mMovement = new Vector3Int(1, 1, 1);
                break;
            case "Xa":
                mMovement = new Vector3Int(0, 8, 0);
                break;
        }
    }
    public BasePieces Clone(bool parentPiecesManager = false)
    {
        return Instantiate(this.gameObject, parentPiecesManager ? PiecesManager.gameObject.transform : PiecesManager.parentClonePieces).GetComponent<BasePieces>();
    }

    public void ChangeParent(bool parentPiecesManager)
    {
        this.transform.SetParent(parentPiecesManager ? PiecesManager.gameObject.transform : PiecesManager.parentClonePieces);
    }
    public int ScoreThisPiece()
    {
        if (GetComponent<BasePieces>().kind == Kind.UI) return 0;
        return mColor == ControllerBot.teamColor ? -mListPiece.score : mListPiece.score; //ControllerBot.teamColor == teamColor ?  scorePiece.score : -scorePiece.score;
    }
    public void Destroyyourself(float time)
    {
        Destroy(gameObject, time);
    }

    private void Start()
    {
        mID = PiecesManager.instance.GetStt();
        ////Debug.Log("My Stt " + mID);

        rotationCS = gameObject.GetComponent<Rotation>();

        Invoke("SetCurrentCell", 0.5f);

        GetandSetStateRotaion(MStateRotation);
    }

    void SetCurrentCell()
    {
        if (mCurrentCell != null)
        mCurrentCell.mCurrentPiece = this;
    }
    public void GetandSetStateRotaionMini(int state)
    {
        MStateRotation = state;

        mListPiece.UpdateRotation(MStateRotation);
    }

    public void GetAndSetListRotate(int state, List<PieceInfo> info)
    {
        switch (state)
        {
            case 1://up
                //gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
                gameObject.transform.DORotate(new Vector3(0, 0, 0), 0.5f);

                if (mColor)
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, -180);
                }
                else
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, 0);
                }

                break;
            case 2:
                //gameObject.transform.eulerAngles = new Vector3(0, 0, -90);
                gameObject.transform.DORotate(new Vector3(0, 0, -90), 0.5f);
                if (mColor)
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, -90);
                }
                else
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, 90);
                }

                break;
            case 3:
                // gameObject.transform.eulerAngles = new Vector3(0, 0, 180);
                gameObject.transform.DORotate(new Vector3(0, 0, 180), 0.5f);
                if (mColor)
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, 0);
                }
                else
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, -180);
                }

                break;
            case 4:
                //gameObject.transform.eulerAngles = new Vector3(0, 0, 90);
                gameObject.transform.DORotate(new Vector3(0, 0, 90), 0.5f);
                if (mColor)
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, 90);
                }
                else
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, -90);
                }
                break;

        }
        MStateRotation = state;
        mListPiece.AddNewPiecesList(info);
    }

    public void GetandSetStateRotaion(int state ,bool left = false , bool all = false)
    {
        switch (state)
        {
            case 1://up
                //gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
                gameObject.transform.DORotate(new Vector3(0, 0, 0), 0.5f);

                if (mColor)
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, -180);
                }
                else
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, 0);
                }

                break;
            case 2:
                //gameObject.transform.eulerAngles = new Vector3(0, 0, -90);
                gameObject.transform.DORotate(new Vector3(0, 0, -90), 0.5f);
                if (mColor)
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, -90);
                }
                else
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, 90);
                }

                break;
            case 3:
                // gameObject.transform.eulerAngles = new Vector3(0, 0, 180);
                gameObject.transform.DORotate(new Vector3(0, 0, 180), 0.5f);
                if (mColor)
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, 0);
                }
                else
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, -180);
                }

                break;
            case 4:
                //gameObject.transform.eulerAngles = new Vector3(0, 0, 90);
                gameObject.transform.DORotate(new Vector3(0, 0, 90), 0.5f);
                if (mColor)
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, 90);
                }
                else
                {
                    mCountText.transform.localEulerAngles = new Vector3(0, 0, -90);
                }
                break;

        }
        MStateRotation = state;
        UpdateRotationListPiece(MStateRotation, left,all);
        //Debug.Log("My State Rotation :" + MStateRotation);
    }

    private void UpdateRotationListPiece(int state , bool left = false , bool all = false)
    {
        if(!all)
            mListPiece.UpdateRotation(state);
        else
            mListPiece.UpdateListRotate(left);
    }

    public void Place(Cell newCell)
    {
        SoundController.Instance.PlayVfxClip(2);
        ////Debug.Log("Place Piece");

        mCurrentCell = newCell;

        mOriginalCell = newCell;

        mCurrentCell.mCurrentPiece = this;

        transform.position = newCell.transform.position;

        gameObject.SetActive(true);
    }
    public void PlaceNoVfx(Cell newCell)
    {

        mCurrentCell = newCell;

        mOriginalCell = newCell;

        mCurrentCell.mCurrentPiece = this;

        transform.position = newCell.transform.position;

        gameObject.SetActive(true);
    }

    #region Movement
    protected void CreateCellPath(int xDirection, int yDirection, int movement)
    {
        //target position
        bool team = false;
        if (mListPiece.GetListNameCount() > 1)
        {
            // tu 2 tro len
            // la 1 doi nen k add hight light them
            team = true;
        }

        // //Debug.Log("Create Cell Path");
        int currentX = mCurrentCell.mBoardPosition.x;
        int currentY = mCurrentCell.mBoardPosition.y;

        for (int i = 0; i < movement; i++)
        {
            currentX += xDirection;

            currentY += yDirection;

            //add cellState and check validate
            CellState cellState = CellState.None;
            cellState = mCurrentCell.mBoard.ValidateCell(currentX, currentY, this);

            //if enemy, add to list, break
            if (cellState == CellState.Enemy)
            {
                mHightlightedCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
                break;
            }
            if (!team)
            {
                if (cellState == CellState.Friendly)
                {
                    if (mNamePiece != "Vuong")
                    {
                        mHightlightedCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
                        break;
                    }
                }
            }
            if (cellState != CellState.Free)
            {
                break;
            }

            mHightlightedCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
        }
    }

    public void CreatePathKi(int dir, int xDirection, int yDirection)
    {

        bool team = false;
        if (mListPiece.GetListNameCount() > 1)
        {
            team = true;
        }
        int currentX = mCurrentCell.mBoardPosition.x;
        int currentY = mCurrentCell.mBoardPosition.y;

        currentX += xDirection;
        currentY += yDirection;

        CellState cellState = CellState.None;
        cellState = mCurrentCell.mBoard.ValidateCell(currentX, currentY, this);

        //if enemy, add to list, break
        if (cellState == CellState.Enemy)
        {
            mHightlightedCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
            return;
        }
        if (!team)
        {
            if (cellState == CellState.Friendly)
            {
                if (mNamePiece != "Vuong")
                {
                    mHightlightedCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
                    return;
                }
            }
        }
        if (cellState != CellState.Free)
        {
            return;
        }
        mHightlightedCells.Add(mCurrentCell.mBoard.AllCells[currentX, currentY]);
    }

    public void CheckPathing()
    {
        mHightlightedCells.Clear();
        if (!mIsPlace)
        {
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    //add cellState and check validate
                    CellState cellState = CellState.None;
                    cellState = Board.instance.ValidateCell(x, y, this);

                    //if enemy, add to list, break
                    if (cellState == CellState.Friendly || cellState == CellState.Free)
                    {
                        mHightlightedCells.Add(Board.instance.AllCells[x, y]);
                    }
                }
            }
            return;
        }
        switch (mNamePiece)
        {
            case "Binh":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:
                            CreateCellPath(0, -1, Mathf.Abs(mMovement.y));
                            break;
                        case 2:
                            CreateCellPath(-1, 0, Mathf.Abs(mMovement.y));
                            break;
                        case 3:
                            CreateCellPath(0, 1, Mathf.Abs(mMovement.y));
                            break;
                        case 4:
                            CreateCellPath(1, 0, Mathf.Abs(mMovement.y));
                            break;
                        default:
                            break;
                    }
                }
                break;
            case "Ki":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:
                            CreatePathKi(3, -1, -2);

                            CreatePathKi(3, 1, -2);

                            CreatePathKi(3, 0, 2);

                            CreatePathKi(3, 2, 2);

                            CreatePathKi(3, -2, 2);

                            break;
                        case 2:
                            CreatePathKi(4, -2, -1);

                            CreatePathKi(4, -2, 1);

                            CreatePathKi(4, 2, 0);

                            CreatePathKi(4, 2, -2);

                            CreatePathKi(4, 2, 2);



                            break;
                        case 3:
                            CreatePathKi(1, -1, 2);

                            CreatePathKi(1, 1, 2);

                            CreatePathKi(1, -2, -2);

                            CreatePathKi(1, 2, -2);

                            CreatePathKi(1, 0, -2);


                            break;
                        case 4:
                            CreatePathKi(2, 2, 1);

                            CreatePathKi(2, 2, -1);

                            CreatePathKi(2, -2, 0);

                            CreatePathKi(2, -2, 2);

                            CreatePathKi(2, -2, -2);

                            break;

                    }
                }
                break;
            case "Phuong":
                if (mIsPlace)
                {
                    CreateCellPath(1, 0, mMovement.x);
                    CreateCellPath(-1, 0, mMovement.x);

                    CreateCellPath(0, 1, mMovement.y);
                    CreateCellPath(0, -1, mMovement.y);

                    CreateCellPath(-1, 1, mMovement.z);
                    CreateCellPath(1, 1, mMovement.z);

                    CreateCellPath(-1, -1, mMovement.z);
                    CreateCellPath(1, -1, mMovement.z);
                }
                break;
            case "Long":
                if (mIsPlace)
                {
                    CreateCellPath(1, 0, mMovement.x);
                    CreateCellPath(-1, 0, mMovement.x);

                    //vertical
                    CreateCellPath(0, 1, mMovement.y);
                    CreateCellPath(0, -1, mMovement.y);


                    CreateCellPath(-1, 1, mMovement.z);

                    CreateCellPath(1, 1, mMovement.z);


                    CreateCellPath(-1, -1, mMovement.z);


                    CreateCellPath(1, -1, mMovement.z);
                }
                break;
            case "Tuong":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:

                            CreateCellPath(1, 0, mMovement.x);
                            CreateCellPath(-1, 0, mMovement.x);


                            CreateCellPath(0, 1, mMovement.y);
                            CreateCellPath(0, -1, mMovement.y);


                            CreateCellPath(-1, -1, mMovement.z);


                            CreateCellPath(1, -1, mMovement.z);

                            break;
                        case 2:
                            CreateCellPath(1, 0, mMovement.x);
                            CreateCellPath(-1, 0, mMovement.x);

                            //vertical
                            CreateCellPath(0, 1, mMovement.y);
                            CreateCellPath(0, -1, mMovement.y);


                            CreateCellPath(-1, 1, mMovement.z);


                            CreateCellPath(-1, -1, mMovement.z);

                            break;
                        case 3:


                            CreateCellPath(1, 0, mMovement.x);
                            CreateCellPath(-1, 0, mMovement.x);
                            CreateCellPath(0, 1, mMovement.y);
                            CreateCellPath(0, -1, mMovement.y);
                            CreateCellPath(-1, 1, mMovement.z);
                            CreateCellPath(1, 1, mMovement.z);
                            break;
                        case 4:

                            CreateCellPath(1, 0, mMovement.x);
                            CreateCellPath(-1, 0, mMovement.x);

                            //vertical
                            CreateCellPath(0, 1, mMovement.y);
                            CreateCellPath(0, -1, mMovement.y);


                            CreateCellPath(1, 1, mMovement.z);


                            CreateCellPath(1, -1, mMovement.z);
                            break;
                    }
                }
                break;
            case "Si":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:


                            CreateCellPath(0, -1, mMovement.y);

                            //duong cheo tren trai
                            CreateCellPath(-1, 1, mMovement.z);
                            //duong cheo tren phai
                            CreateCellPath(1, 1, mMovement.z);

                            //duong cheo duoi trai
                            CreateCellPath(-1, -1, mMovement.z);

                            //duong cheo duoi phai
                            CreateCellPath(1, -1, mMovement.z);
                            break;
                        case 2:

                            CreateCellPath(-1, 0, mMovement.x);

                            //duong cheo tren trai
                            CreateCellPath(-1, 1, mMovement.z);
                            //duong cheo tren phai
                            CreateCellPath(1, 1, mMovement.z);

                            //duong cheo duoi trai
                            CreateCellPath(-1, -1, mMovement.z);

                            //duong cheo duoi phai
                            CreateCellPath(1, -1, mMovement.z);
                            break;
                        case 3:

                            CreateCellPath(0, 1, mMovement.y);

                            //duong cheo tren trai
                            CreateCellPath(-1, 1, mMovement.z);
                            //duong cheo tren phai
                            CreateCellPath(1, 1, mMovement.z);

                            //duong cheo duoi trai
                            CreateCellPath(-1, -1, mMovement.z);

                            //duong cheo duoi phai
                            CreateCellPath(1, -1, mMovement.z);
                            break;
                        case 4:

                         
                            CreateCellPath(-1, 1, mMovement.z);
                     
                            CreateCellPath(1, 1, mMovement.z);

                           
                            CreateCellPath(-1, -1, mMovement.z);

                         
                            CreateCellPath(1, -1, mMovement.z);

                            CreateCellPath(1, 0, mMovement.z);
                            break;

                    }
                }
                break;
            case "Vuong":
                if (mIsPlace)
                {
                    CreateCellPath(1, 0, mMovement.x);
                    CreateCellPath(-1, 0, mMovement.x);


                    CreateCellPath(0, 1, mMovement.y);
                    CreateCellPath(0, -1, mMovement.y);


                    CreateCellPath(-1, 1, mMovement.z);

                    CreateCellPath(1, 1, mMovement.z);

                    CreateCellPath(-1, -1, mMovement.z);

                    CreateCellPath(1, -1, mMovement.z);
                }
                break;
            case "Xa":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:
                            CreateCellPath(0, -1, Mathf.Abs(mMovement.y));
                            break;
                        case 2:
                            CreateCellPath(-1, 0, Mathf.Abs(mMovement.y));
                            break;
                        case 3:
                            CreateCellPath(0, 1, Mathf.Abs(mMovement.y));
                            break;
                        case 4:
                            CreateCellPath(1, 0, Mathf.Abs(mMovement.y));
                            break;
                        default:
                            break;
                    }
                }
                break;
        }

    }

    public void ShowCells()
    {
        // //Debug.Log("HightLightCell = " + mHightlightedCells.Count);
        if (mHightlightedCells.Count <= 0)
        {
            return;
        }
        foreach (Cell item in mHightlightedCells)
        {
            item.mOutlineImage.enabled = true;
        }

    }

    public void ClearCells()
    {
        foreach (Cell item in mHightlightedCells)
        {
            item.mOutlineImage.enabled = false;
        }
        mHightlightedCells.Clear();
    }


    public void Move(Cell targetCell, bool isTeamup, bool isKill)
    {
        SoundController.Instance.PlayVfxClip(2);

        GameController.instance.Shield.SetActive(true);

        GameController.dropPiece = isDropPiece;

        if (GameController.instance.mCurentBasePiece.mNamePiece != "Long" && GameController.instance.mCurentBasePiece.mNamePiece != "Phuong" && GameController.instance.mCurentBasePiece.mNamePiece != "Vuong")
        {
            GameController.instance.ShowUIRotation(false, mColor);
        }

        if (!isTeamup)
        {
            if (GameController.instance.mCurentBasePiece.mNamePiece != "Long" && GameController.instance.mCurentBasePiece.mNamePiece != "Phuong" && GameController.instance.mCurentBasePiece.mNamePiece != "Vuong")
            {
                GameController.instance.SelectedPiece = this;
                if (isDropPiece)
                {
                    GameController.instance.UI_PassTurn_White_Active(true);
                    GameController.instance.ShowUIRotation(true, GameController.instance.mCurentBasePiece.mColor);
                }
                else

                {
                    GameController.instance.ShowUIRotation(true);
                }
            }
            else
                PiecesManager.SwitchSides(!mColor);
        }

        //targetCell.RemovePiece();

        //clear current
        if(mCurrentCell != null)
        mCurrentCell.mCurrentPiece = null;
        //switch cells
        mCurrentCell = targetCell;
        mCurrentCell.mCurrentPiece = this;
        transform.SetParent(PiecesManager.transform);
        transform.DOMove(targetCell.transform.position, 0.5f);
        GameController.instance.ShieldGame();
        //targetCell = null;
        mIsPlace = true;
        ClearCells();
        GameController.instance.SelectedCellColor(false);
    }



    #endregion

    protected bool isReset = false;

    public void Reset()
    {
        isReset = true;

        Place(mOriginalCell);
    }

    public void TeamUp(List<PieceInfo> mTargetPieceList)
    {
        mListPiece.AddPiecesList(mTargetPieceList);
    }


    int newState;

    public void Kill(List<PieceInfo> targetPiece, IKillCommand command)
    {

        if (!mColor) { pieceColor = new Color32(255, 176, 93, 255); }
        else
        {
            pieceColor = new Color32(58, 241, 56, 255);
        }

        //switch (mColor)
        //{
        //    case true:
        //        newState = 1;
        //        break;
        //    case false:
        //        newState = 3;
        //        break;
        //    default:
        //        break;
        //}

        foreach (PieceInfo item in targetPiece)
        {
            if (item.nPieceName == "Vuong")
            {
                PiecesManager.misVuongAlive = false;
                string newString;
                if (mColor) { newString = "Cam chiến thắng ván này !"; }
                else { newString = "Xanh chiến thắng ván này!";  }

                GameController.instance.WinGame(newString);
                return;
            }

            StorePieces.Instance.Add(item.nPieceName, !mColor);
            if(command != null)  command.GetCell(PiecesManager.GetCellforUndo(),item.nPieceName);
            PiecesManager.CellforUndo = null; 
        }

        Destroy(gameObject, 0.4f);

    }

    #region Event

    public void MoveOnly(Cell targetCell, ref Turn turn)
    {

        if (targetCell.mCurrentPiece == null)
        {
            turn.mode = mode.MoveSingleinTeam;

            MoveSingleinTeam command = new MoveSingleinTeam();
            turn.targetCell = targetCell.mId;
            
            GameObject newPiece = PiecesManager.instance.CreateBasePiece(mColor, mListPiece.GetPieceListRotation(), mListPiece.GetPieceList());

            PiecesManager.rotation = false;

            if (newPiece.GetComponent<BasePieces>() != null)
            {
                newPiece.GetComponent<BasePieces>().Place(mCurrentCell);
                string newPieceName = newPiece.GetComponent<BasePieces>().mNamePiece;
                if (newPieceName == "Vuong" || newPieceName == "Long" || newPieceName == "Phuong")
                {
                    GameController.instance.AddDataRecord();
                }
                newPiece.GetComponent<BasePieces>().Move(targetCell, false, false);
                GameController.instance.SelectedPiece = newPiece.GetComponent<BasePieces>();
                //newPiece.GetComponent<BasePieces>().rotationCS.StartCoroutine("ActiveButton");

                command.GetMovePiece(newPiece.GetComponent<BasePieces>());
            }


            mListPiece.RemovePieceList();

            if (mListPiece.GetListNameCount() > 1)
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(mColor, mListPiece.GetPieceListRotation(), mListPiece.GetPieceList());

                mListPiece.RemovePieceList();

                newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(mListPiece.GetPieceNamesList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            else
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(mColor, mListPiece.GetPieceListRotation(), mListPiece.GetPieceList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }

            UndoManager.instance.InsertCommand(command);
            GameController.instance.mCurentBasePiece.ClearCells();
            GameController.instance.SelectedCellColor(false);
            GameController.instance.mTargetCell = null;
            GameController.instance.ClearListHightLightCell();
            GameController.instance.SetState(State.None);


            //clear current
            //mCurrentCell.mCurrentPiece = null;

            ////switch cells
            //mCurrentCell = targetCell;

            //mCurrentCell.mCurrentPiece = this;

            ////transform.DOMove(targetCell.transform.position, 0.5f);

            //targetCell = null;

            //mIsPlace = true;

            Destroy(gameObject, 0.4f);
        }
    }

    public void MoveTeam(Cell targetCell, ref Turn turn)
    {
        if (targetCell.mCurrentPiece == null)
        {
            GameController.instance.Shield.SetActive(true);

            turn.mode = mode.MoveTeam;
            MoveTeam command = new MoveTeam(this, targetCell);
            turn.targetCell = targetCell.mId;
            UndoManager.instance.InsertCommand(command);

            PiecesManager.rotation = false;

            //targetCell.RemovePiece();

            //clear current
            mCurrentCell.mCurrentPiece = null;

            //switch cells
            mCurrentCell = targetCell;

            mCurrentCell.mCurrentPiece = this;

            transform.DOMove(targetCell.transform.position, 0.5f);

            GameController.instance.ShieldGame();

            targetCell = null;

            mIsPlace = true;

            ClearCells();

            //GameController.instance.mCurentBasePiece.ClearCells();

            GameController.instance.mTargetCell = null;

            GameController.instance.ClearListHightLightCell();
            GameController.instance.SelectedCellColor(false);
            GameController.instance.SetState(State.None);

            GameController.instance.ShowUIRotation(false, mColor);

            if (checkNamePiece(this))
            {  PiecesManager.SwitchSides(!mColor);
                GameController.instance.AddDataRecord();
            }
            else
            {
                GameController.instance.SelectedPiece = this;
                GameController.instance.ShowUIRotation(true);
            }

            //PiecesManager.SwitchSides(!mColor);
        }
    }


    public void DisableAfterIimer(float time)
    {
        StartCoroutine(DisableAfter(time));
    }
    private IEnumerator DisableAfter(float timer)
    {
        yield return new WaitForSeconds(timer);
        this.gameObject.SetActive(false);
    }

    public void MoveOnlyAndKill(Cell targetCell, ref Turn turn)
    {
        if (targetCell.mCurrentPiece != null)
        {
            turn.mode = mode.MoveSingleandKill;
            turn.targetCell = targetCell.mId;
            MoveSingleandKill command = new MoveSingleandKill();
            GameObject newPiece = PiecesManager.instance.CreateBasePiece(mColor, mListPiece.GetPieceListRotation(), mListPiece.GetPieceList());
            PiecesManager.rotation = false;

            if (newPiece.GetComponent<BasePieces>() != null)
            {
                newPiece.GetComponent<BasePieces>().Place(mCurrentCell);
                string newPieceName = newPiece.GetComponent<BasePieces>().mNamePiece;
                if (newPieceName == "Vuong" || newPieceName == "Long" || newPieceName == "Phuong")
                {
                    GameController.instance.AddDataRecord();
                }
                newPiece.GetComponent<BasePieces>().Move(targetCell, false, true);
                GameController.instance.mTargetBasePiece.DisableAfterIimer(0.3f);
                GameController.instance.SelectedPiece = newPiece.GetComponent<BasePieces>();
                //newPiece.GetComponent<BasePieces>().rotationCS.StartCoroutine("ActiveButton");
                command.GetMovePiece(newPiece.GetComponent<BasePieces>());
            }

            mListPiece.RemovePieceList();

            if (mListPiece.GetListNameCount() > 1)
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(mColor, mListPiece.GetPieceListRotation(), mListPiece.GetPieceList());
                mListPiece.RemovePieceList();
                newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(mListPiece.GetPieceNamesList());
                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            else
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(mColor, mListPiece.GetPieceListRotation(), mListPiece.GetPieceList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(mCurrentCell);
                }
                command.GetStandPiece(newOrderPiece.GetComponent<BasePieces>());
            }
            GameController.instance.mCurentBasePiece.ClearCells();
            GameController.instance.ClearListHightLightCell();
            GameController.instance.SetState(State.None);
            GameController.instance.SelectedCellColor(false);
            command.GetKilledPiece(GameController.instance.mTargetBasePiece);
            GameController.instance.mTargetBasePiece.Kill(GameController.instance.mTargetBasePiece.mListPiece.GetPieceNamesList(),command);
            UndoManager.instance.InsertCommand(command);
            GameController.instance.mTargetCell = null;
            //PiecesManager.SwitchSides(!mColor);
            Destroy(gameObject, 0.4f);
        }
    }

    public void MoveTeamAndKill(Cell targetCell, ref Turn turn)
    {
        if (targetCell.mCurrentPiece != null)
        {
            BasePieces targetPiece = targetCell.mCurrentPiece;
            turn.mode = mode.MoveTeamandKill;
            turn.targetCell = targetCell.mId;
            GameController.instance.Shield.SetActive(true);
            MoveTeamandKill command = new MoveTeamandKill(this,targetCell);

            command.GetKilledPiece(GameController.instance.mTargetBasePiece);

            PiecesManager.rotation = false;

            GameController.instance.ShowUIRotation(false, mColor);

            //PiecesManager.SwitchSides(!mColor);
            
            //targetCell.RemovePiece();

            //clear current
            mCurrentCell.mCurrentPiece = null;
            //switch cells
            mCurrentCell = targetCell;
            mCurrentCell.mCurrentPiece = this;
            transform.DOMove(targetCell.transform.position, 0.5f);
            targetPiece.DisableAfterIimer(0.3f);
            GameController.instance.ShieldGame();
            targetCell = null;
            //mIsPlace = true;
            ClearCells();

            //GameController.instance.mCurentBasePiece.ClearCells();
            GameController.instance.SelectedCellColor(false);
            GameController.instance.mTargetCell = null;

            GameController.instance.ClearListHightLightCell();

            GameController.instance.mTargetBasePiece.Kill(GameController.instance.mTargetBasePiece.mListPiece.GetPieceNamesList(),command);

            UndoManager.instance.InsertCommand(command);

            GameController.instance.SetState(State.None);

            if (mNamePiece == "Vuong" || mNamePiece == "Long" || mNamePiece == "Phuong")
            { PiecesManager.SwitchSides(!mColor);
                GameController.instance.AddDataRecord();
            }
            else
            {
                GameController.instance.ShowUIRotation(true);
                GameController.instance.SelectedPiece = this;
            }

        }
    }

   public  bool checkNamePiece(BasePieces piece)
    {
        if (piece.mNamePiece == "Vuong" || piece.mNamePiece == "Long" || piece.mNamePiece == "Phuong") return true;
        else return false;
    }

    public void DestroyYourself()
    {
        DestroyImmediate(this.gameObject);
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        SoundController.Instance.PlayVfxClip(2);
        if (GameManager.Instance.Mode == GameMode.Ready)
        {
            //send to setup
            SetupLongPhuong.Instance.CheckPiece(this);
            return;
        }
        if (kind == Kind.Normal)
        {
            if (GameManager.Instance.Mode == GameMode.Play)
            {
                GameController.instance.GetInfoFromPiece(this);
            }
            return;
        }
        else if(kind == Kind.UI)
        {
            if (GameManager.Instance.Mode == GameMode.Play)
            {
                GameController.instance.CheckPiece(this);
  
            }
        }
    }
}


#endregion