﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


[System.Serializable]
public class PiecesResources
{

    public string name;

    public Sprite parentSprite;

    public Sprite childSprite;

}


public class PiecesManager : MonoBehaviour
{
    //for undo
    public static PiecesManager instance;
    private void Awake()
    {
        instance = this;
    }

    public static bool rotation = false;
    public List<PiecesResources> PiecesResources = new List<PiecesResources>();
 
    public bool misVuongAlive = true;
    public GameObject mPicecePrefab;

    private List<GameObject> mWhitePieces = null;
    private List<GameObject> mBlackPieces = null;

    public Board board;

    public GameObject greenTeamColor;
    public GameObject orangeTeamColor;
    public GameObject signalGreen;
    public GameObject signalOrange;

    private Vector3 defaultPosC = new Vector3(0,-600,0);
    private Vector3 defaultPosX = new Vector3(0,600,0);

    public Cell CellforUndo;

    private GameObject[] mSwitchWhitePieces = new GameObject[2];
    private GameObject[] mSwitchBlackPieces = new GameObject[2];

    public Transform parentClonePieces;
    public void SwitchPieces(bool mteamColor)
    {
        if (mteamColor)
        {
            Cell temp = mSwitchWhitePieces[0].GetComponent<BasePieces>().mCurrentCell;//temp = b
            mSwitchWhitePieces[0].GetComponent<BasePieces>().Place(mSwitchWhitePieces[1].GetComponent<BasePieces>().mCurrentCell);
            mSwitchWhitePieces[1].GetComponent<BasePieces>().Place(temp);
        }
        else
        {
            Cell temp = mSwitchBlackPieces[0].GetComponent<BasePieces>().mCurrentCell;//temp = b
            mSwitchBlackPieces[0].GetComponent<BasePieces>().Place(mSwitchBlackPieces[1].GetComponent<BasePieces>().mCurrentCell);
            mSwitchBlackPieces[1].GetComponent<BasePieces>().Place(temp);

        }
    }
    public int stt = 0;

    public int GetStt()
    {
        stt++;
        return stt;
    }

    private string[] mPiecesOrder = new string[20]
    {
        "Binh","Binh","Binh","Binh","Binh","Binh","Binh","Binh","Binh",

        "Xa","Ki","Si","Tuong","Vuong","Tuong","Si","Ki","Xa","Long","Phuong"
    };

    #region Spawn Piece for 2 sub board
    [Header(" Spawn Piece for 2 sub board")]
    private string[] listSpawn = new string[7] {
    "Long", "Phuong" , "Tuong" , "Si", "Ki" , "Xa","Binh"
    };
    public List<Text> listTextCount;
    public void Spawn(bool teamColor , int stateRotation,Transform parent,Kind kind = Kind.UI) {

        for (int i = 0; i < listSpawn.Length; i++)
        {
            GameObject newBasePiece = Instantiate(mPicecePrefab);

            newBasePiece.transform.SetParent(parent);
            newBasePiece.transform.localPosition = new Vector3(newBasePiece.transform.position.x, newBasePiece.transform.position.x, 0);
            //set scale and rotation
            newBasePiece.transform.localScale = new Vector3(1, 1, 1);
            newBasePiece.transform.eulerAngles = GetandSetStateRotaion(stateRotation);

            Sprite parentSprite = GetSpriteFormList(PiecesResources, listSpawn[i], true);
            Sprite childSprite = GetSpriteFormList(PiecesResources, listSpawn[i], false);

            //newListBasePiece.Add(newBasePiece);
            if (newBasePiece.GetComponent<BasePieces>() != null)
            {
                newBasePiece.GetComponent<BasePieces>().SetUp(teamColor, this, stateRotation, false, listSpawn[i], parentSprite, childSprite, true,kind);
            }
            if (teamColor)
            {
                Store store = new Store(listSpawn[i], newBasePiece.transform, 0, teamColor, listTextCount[i]);
                StorePieces.Instance.storesPiece.Add(store);
            }
            else
            {
                Store store = new Store(listSpawn[i], newBasePiece.transform, 0, teamColor, listTextCount[i+7]);
                StorePieces.Instance.storesPiece.Add(store);
            }
           
        }
    }

    #endregion

    public GameObject CreateBasePiece(bool teamColor, int stateRotation, string typePiece, Kind kind = Kind.Normal)
    {
        GameObject newBasePiece = Instantiate(mPicecePrefab);

        newBasePiece.transform.SetParent(transform);

        newBasePiece.transform.localScale = new Vector3(1, 1, 1);

        newBasePiece.transform.eulerAngles = GetandSetStateRotaion(stateRotation);

        Sprite parentSprite = GetSpriteFormList(PiecesResources, typePiece, true);

        Sprite childSprite = GetSpriteFormList(PiecesResources, typePiece, false);

        if (newBasePiece.GetComponent<BasePieces>() != null)
        {
            newBasePiece.GetComponent<BasePieces>().SetUp(teamColor, this, stateRotation, true, typePiece, parentSprite, childSprite,false, kind);
        }

        return newBasePiece;
    }

    public List<GameObject> CreateBasePiecesForStartGame(bool teamColor, Board board, int stateRotation , Kind kind)
    {
        List<GameObject> newListBasePiece = new List<GameObject>();
        for (int i = 0; i < mPiecesOrder.Length; i++)
        {
            GameObject newBasePiece = Instantiate(mPicecePrefab);

            newBasePiece.transform.SetParent(transform);

            //set scale and rotation
            newBasePiece.transform.localScale = new Vector3(1, 1, 1);

            newBasePiece.transform.eulerAngles = GetandSetStateRotaion(stateRotation);

            Sprite parentSprite = GetSpriteFormList(PiecesResources, mPiecesOrder[i], true);

            Sprite childSprite = GetSpriteFormList(PiecesResources, mPiecesOrder[i], false);

            newListBasePiece.Add(newBasePiece);

            if (newBasePiece.GetComponent<BasePieces>() != null)
            {
                newBasePiece.GetComponent<BasePieces>().SetUp(teamColor, this, stateRotation, true, mPiecesOrder[i], parentSprite, childSprite,false, kind);
            }
            //
           
        }
        return newListBasePiece;

    }

    Sprite GetSpriteFormList(List<PiecesResources> listpiece, string name, bool parent)
    {
        for (int i = 0; i < listpiece.Count; i++)
        {
            if (listpiece[i].name == name)
            {
                if (parent)
                {
                    return listpiece[i].parentSprite;
                }
                else
                {
                    return listpiece[i].childSprite;
                }
            }
        }
        return null;
    }

    public void SetUp(Board board)
    {
        mWhitePieces = CreateBasePiecesForStartGame(true, board,3, Kind.Normal);

        mBlackPieces = CreateBasePiecesForStartGame(false, board, 1, Kind.Normal);

        PlacePiecesForFirstPlace(0, 2, 1, 1, 7, mWhitePieces, board,true);

        PlacePiecesForFirstPlace(8, 6, 7, 1, 7, mBlackPieces, board,false);

        SwitchSides(true);
    }


    /*
    public void CreatePiece(bool teamColor, int stateRotation, string PieceType,bool isDrop)//Board board
    {

        GameObject newBasePiece = Instantiate(mPicecePrefab);

        newBasePiece.transform.localScale = new Vector3(1, 1, 1);

        newBasePiece.transform.eulerAngles = GetandSetStateRotaion(stateRotation);

        Sprite parentSprite = GetSpriteFormList(PiecesResources, PieceType, true);

        Sprite childSprite = GetSpriteFormList(PiecesResources, PieceType, false);

        if (newBasePiece.GetComponent<BasePieces>() != null)
        {
            newBasePiece.GetComponent<BasePieces>().SetUp(teamColor, this, stateRotation, false, PieceType, parentSprite, childSprite,true);
        }
        BasePieces pieces = newBasePiece.GetComponent<BasePieces>();
        //newPieces.SetUp(teamColor, this, stateRotation, false);

        PlacePiece(teamColor, 1, 7, pieces);

        ////Debug.Log("Create Piece");
    }
    */
  

    public Vector3 GetandSetStateRotaion(int state)
    {
        switch (state)
        {
            case 1://up
                return new Vector3(0, 0, 0);
            case 2:
                return new Vector3(0, 0, -90);
            case 3:
                return new Vector3(0, 0, 180);
            case 4:
                return new Vector3(0, 0, 90);
            default:
                return new Vector3(0, 0, 0);
        }
    }

    public Cell GetCellforUndo()
    {
        if (CellforUndo != null)
        {
            //Debug.Log("Get Cell : "+CellforUndo.name);
            return CellforUndo;

        }
        Debug.Log("Null Cell");
        return null;
    }
    /*
    public void PlacePiece(bool teamColor, int xColumn, int yColumn, BasePieces piece)
    {
        CellforUndo = null;

        bool isPlace = false;

        if (teamColor)
        {
            for (int y = 0; y < yColumn; y++)
            {
                for (int x = 0; x < xColumn; x++)
                {
                    if (!board.WhiteCell[x, y].ContainPiece())
                    {
                        piece.Place(board.WhiteCell[x, y]);
                        piece.transform.SetParent(board.WhiteCell[x, y].transform);
                        piece.transform.localScale = Vector3.one;
                        ////Debug.Log("Place White Piece");
                        isPlace = true;
                        CellforUndo = board.WhiteCell[x, y];
                        break;
                    }
                }
                if (isPlace) return;
            }
            
        }
        else
        {
            for (int y = 0; y < yColumn; y++)
            {
                for (int x = 0; x < xColumn; x++)
                {
                    if (!board.BlackCell[x, y].ContainPiece())
                    {
                        piece.Place(board.BlackCell[x, y]);
                        piece.transform.SetParent(board.BlackCell[x, y].transform);
                        piece.transform.localScale = Vector3.one;
                        // //Debug.Log("Place Black Piece");
                        CellforUndo = board.BlackCell[x, y];
                         isPlace = true;
                        break;
                    }
                }
                if (isPlace) return;
            }
           
        }

    }
    */
    private void PlacePiecesForFirstPlace(int firstRow, int thirdRow, int secondRow, int secondRow1, int secondRow2, List<GameObject> pieces, Board board,bool teamColor)
    {
        for (int i = 0; i < 9; i++)
        {
            if (pieces[i].GetComponent<BasePieces>() != null)
            {
                pieces[i].GetComponent<BasePieces>().Place(board.AllCells[i, thirdRow]);// 0 >> 8 la binh hang 3
            }
            if (pieces[i + 9].GetComponent<BasePieces>() != null)
            {
                pieces[i + 9].GetComponent<BasePieces>().Place(board.AllCells[i, firstRow]); // 9 >> 17 la hang 1
            }

        }
        if (teamColor)//white team
        {
            if (pieces[18].GetComponent<BasePieces>() != null || pieces[19].GetComponent<BasePieces>() != null)
            {

                //pieces[19].GetComponent<BasePieces>().Place(board.AllCells[secondRow1, secondRow]);1:1
                //mSwitchWhitePieces[0] = pieces[19];
                pieces[19].transform.localPosition = defaultPosC;
                SetupLongPhuong.Instance.AddItem(pieces[19].GetComponent<BasePieces>(),true);
                //pieces[18].GetComponent<BasePieces>().Place(board.AllCells[secondRow2, secondRow]); 7:1
                //mSwitchWhitePieces[1] = pieces[18];
                pieces[18].transform.localPosition = defaultPosC;
                SetupLongPhuong.Instance.AddItem(pieces[18].GetComponent<BasePieces>(),true);
            }

        }
        else
        {
            if (pieces[18].GetComponent<BasePieces>() != null || pieces[19].GetComponent<BasePieces>() != null)
            {
                //pieces[18].GetComponent<BasePieces>().Place(board.AllCells[secondRow1, secondRow]);
                //mSwitchBlackPieces[0] = pieces[18];
                //pieces[19].GetComponent<BasePieces>().Place(board.AllCells[secondRow2, secondRow]);
                //mSwitchBlackPieces[1] = pieces[19];
                pieces[19].transform.localPosition = defaultPosX;
                SetupLongPhuong.Instance.AddItem(pieces[19].GetComponent<BasePieces>(),false);
                pieces[18].transform.localPosition = defaultPosX;
                SetupLongPhuong.Instance.AddItem(pieces[18].GetComponent<BasePieces>(),false);
            }

        }
    
    }

    public bool isBlackTurn;

    public bool GetTurn()
    {
        return isBlackTurn;
    }
    public void ChangeSignal(bool team)
    {
        if (!team)
        {
            //  mTurnColor.color = new Color32(58, 241, 56, 255);
            orangeTeamColor.SetActive(false);
            signalOrange.SetActive(false);
            greenTeamColor.SetActive(true);
            signalGreen.SetActive(true);
        }
        else
        {
            orangeTeamColor.SetActive(true);
            signalOrange.SetActive(true);
            greenTeamColor.SetActive(false);
            signalGreen.SetActive(false);
            // mTurnColor.color = new Color32(255, 176, 93, 255);
        }
    }
    public void SwitchSides(bool color)
    {
        GameController.instance.ResetRotState();
        isBlackTurn = color;

        ChangeSignal(color);

        GameController.instance.UISurShow(!color);

        GameController.instance.UI_Undo.SetActive(true);
        GameController.instance.UI_Save.SetActive(true);
        GameController.instance.UI_Home.SetActive(true);

        if (GameController.playMode == PlayMode.SinglePlay)
        {
            if(ControllerBot.teamColor == color && GameManager.Instance.Mode == GameMode.Play)
            {
                GameController.instance.UI_Undo.SetActive(false);
                GameController.instance.UI_Save.SetActive(false);
                GameController.instance.UI_Home.SetActive(false);
                StartCoroutine(ControllerBot.Instance.CallCaculate());
            }
        }
    }

    public void ResetPieces()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
