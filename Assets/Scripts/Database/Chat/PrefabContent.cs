/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: PrefabContent.cs
* Script Author: MinhLe 
* Created On: 8/20/2021 5:52:05 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PrefabContent : MonoBehaviour
{
    public TMP_Text userName;
    public TMP_Text chatContent;

    public PrefabContent Init(string userName, string chatContent)
    {
        this.userName.text = userName;
        this.chatContent.text = chatContent;
        return this;
    }
}
