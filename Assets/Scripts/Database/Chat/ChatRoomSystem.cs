/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: ChatRoomSystem.cs
* Script Author: MinhLe 
* Created On: 8/20/2021 5:37:41 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChatRoomSystem : MonoBehaviour
{
    #region Singleton

    public static ChatRoomSystem Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
      
    }

    #endregion


    [Header("UI Object")]
    public TMP_InputField contentText;
    public Button sendButton;

    [Header("Object")]
    public Transform parentContent;
    public GameObject prefabContent;

    private void Start()
    {
        InitializedEvent();
        DatabaseUtils.Instance.ListenChatRoom();
    }

    private void InitializedEvent()
    {
        sendButton.onClick.AddListener(() => { EventSendContent();}); 
    }

    private void EventSendContent()
    {
        sendButton.interactable = false;
        Debug.Log(contentText.text.Trim().Length);
        if (contentText.text.Trim().Length == 0) return;
        string userID = PlayerPrefs.GetString("UserName");
        SendChatDataEvent(userID, contentText.text);
    }

    public async void SendChatDataEvent(string userName, string content)
    {
        await DatabaseUtils.Instance.SendChatContent(userName,content);
        contentText.text = "";
        sendButton.interactable = true ;
    }

    public void CreateChatContent(string userName , string content)
    {
        Debug.Log("Create chat");
        PrefabContent clone = Instantiate(prefabContent, parentContent).GetComponent<PrefabContent>().Init(userName, content);
    }

    public void DestroyChat()
    {
        foreach (Transform child in parentContent)
        {
            Destroy(child.gameObject);
        }
    }
}
