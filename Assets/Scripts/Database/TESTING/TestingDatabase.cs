/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: TestingDatabase.cs
* Script Author: MinhLe 
* Created On: 8/22/2021 12:06:06 PM*/
using System.Collections;
using System.Collections.Generic;

using UnityEngine;



public class TestingDatabase : MonoBehaviour
{
    private void Start()
    {
        PushDatas();
    }

    private void PushDatas()
    {
        UserInfo user1 = new UserInfo();
        user1.userID = "0005";
        UserPublicInfo public1 = new UserPublicInfo();
        public1.userName = "CarimBanzema";
        public1.match = 63;
        public1.elo = 311;
        user1.publicInfo = public1;

        DatabaseUtils.Instance.PushDataUser(user1);
    }


}
