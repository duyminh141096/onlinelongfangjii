/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: StructClass.cs
* Script Author: MinhLe 
* Created On: 8/17/2021 11:26:40 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Matchmode
{
    Ranked,
    Normal
}

public enum MatchStatus
{
    Waiting, 
    Played
}

[System.Serializable]
public class ChatData
{
    public string userName;
    public string content;
}

[System.Serializable]
public class RoomInfo
{
    public int idRoom;
    public int idRoomName;
    public string name;
    public Matchmode matchMode;
    public int playerJoined;
    public MatchStatus matchStatus;

    public PlayerWaitingRoom host;
    public PlayerWaitingRoom player;
    public RoomInfo(Matchmode mode , string name, int id)
    {
        host = new PlayerWaitingRoom();
        player = new PlayerWaitingRoom();
        host.id = PlayerPrefs.GetString("ID");
        player.id = "None";
        matchMode = mode;
        this.name = name ;
        this.idRoom = id;
        idRoomName = ++id;
    }

}

[System.Serializable]
public class PlayerWaitingRoom
{
    public string id;
    public bool readyStatus = false;
}


[System.Serializable]
public class UserInfo
{
    public string userID;
    public UserPublicInfo publicInfo;
    public UserPrivateInfo privateInfo;
}

[System.Serializable]
public class UserPublicInfo
{
    public string userName;
    public int match;
    public int elo;
}
[System.Serializable]
public class UserPrivateInfo
{
    
}