/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: RoomController.cs
* Script Author: MinhLe 
* Created On: 8/20/2021 5:37:22 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class RoomController : MonoBehaviour
{
    #region Singleton

    public static RoomController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    #endregion

    string id ;
    string hostID;
    int roomID ;
    public bool isHosting;

    [Header("Host UI Info")]
    public TMP_Text hostNameText;
    public TMP_Text hostElo;
    public TMP_Text hostMatchText;
    public GameObject groupHostInfo;

    [Header("User UI Info")]
    public TMP_Text userNameText;
    public TMP_Text userElo;
    public TMP_Text usermatchText;
    public GameObject groupUserInfo;

    [Header("Room Info")]
    public TMP_Text roomIDText;
    public TMP_Text roomNameText;

    [Header("Button Ready and Start")]
    public Button readyButton;
    public Button startButton;
    public GameObject readyStatusOn;
    [Header("====================")]
    public GameObject loading;
    private void Start()
    {
        id = PlayerPrefs.GetString("ID");
        roomID = PlayerPrefs.GetInt("RoomID");

        CheckWhoisHosting(roomID, id);
        GetRoomInfo(roomID);
        InitEventUI();
        ListenPlayerChangeStatus();
    }

    // /////////////////////////////////////////////////////////
    // /////////////////////////////////////////////////////////
    #region Set UI Host And Event
    private void InitEventUI()
    {
        startButton.onClick.AddListener(() => { });
        readyButton.onClick.AddListener(() => {
            readyButton.interactable = false;
            //readyStatusOn.SetActive(!readyStatusOn.activeSelf);
            SetPlayerReady(!readyStatusOn.activeSelf);
        });
    }

    public void SetDataHost(UserPublicInfo info)
    {
        hostNameText.text = info.userName;
        hostElo.text = info.elo.ToString();
        hostMatchText.text = info.match.ToString();
        loading.SetActive(false);
        if(isHosting) PlayerPrefs.SetString("UserName", info.userName);
    }
    public void SetDataPlayer(UserPublicInfo info)
    {
        userNameText.text = info.userName;
        userElo.text = info.elo.ToString();
        usermatchText.text = info.match.ToString();
        groupUserInfo.SetActive(true);
    }

    public void GetRoomInfo(int roomID)
    {
        DatabaseUtils.Instance.GetRoomInfo(roomID);
    }

    public void SetRoomInfo(RoomInfo room)
    {
        roomIDText.text = "No." + room.idRoomName.ToString("0000");
        roomNameText.text = room.name;
    }
    public void  PlayerChangeStatus(bool status)
    {
        readyStatusOn.SetActive(status);
    }
    #endregion
    // /////////////////////////////////////////////////////////
    // /////////////////////////////////////////////////////////

    private void ListenPlayerChangeStatus()
    {
        DatabaseUtils.Instance.SubListenPlayerReady(roomID);
    }



    private async void SetPlayerReady(bool ready)
    {
        await DatabaseUtils.Instance.SetPlayerReady(ready, roomID);
        readyButton.interactable = true;
    }

    public void SetHostingID(string hostID)
    {
        this.hostID = hostID;
    }

    public void PlayerLeavingRoom()
    {
        groupUserInfo.SetActive(false);
        SetPlayerReady(false);
    }

    private void CheckWhoisHosting(int roomid,string myiD )
    {
        DatabaseUtils.Instance.CheckHostingRoom(roomid, myiD);
    }

    public void AfterCheckHosting(bool isHosting)
    {
        this.isHosting = isHosting;

        if (isHosting)
        {
            DatabaseUtils.Instance.LoadDataHost(id);
            DatabaseUtils.Instance.ListenPlayerInRoom(roomID);
            startButton.gameObject.SetActive(true);
        }
        else
        {
            DatabaseUtils.Instance.LoadDataHost(hostID);
            DatabaseUtils.Instance.LoadDataPlayer(id);
            DatabaseUtils.Instance.ListenHostInRoom(roomID);
            readyButton.gameObject.SetActive(true);
        }



        //if (isHosting)  DatabaseUtils.Instance.ListenPlayerInRoom(roomID);
        //else  DatabaseUtils.Instance.ListenHostInRoom(roomID);


    }


}
