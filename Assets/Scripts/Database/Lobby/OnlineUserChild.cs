/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: OnlineUserChild.cs
* Script Author: MinhLe 
* Created On: 8/17/2021 6:11:24 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
public class OnlineUserChild : MonoBehaviour
{
    //UI
    public TMP_Text userID;
    //

    private string id;

    public void Init(UserInfo id)
    {
        this.id = id.userID;
        userID.text = id.userID;
    }
}
