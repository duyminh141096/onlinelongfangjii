/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: CallBackApplicationQuit.cs
* Script Author: MinhLe 
* Created On: 8/17/2021 9:47:35 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallBackApplicationQuit : MonoBehaviour
{
    private void OnApplicationQuit()
    {
        DatabaseUtils.Instance.LeaveTheLobby();
        DatabaseUtils.Instance.UnsubAllListen();
    }
}
