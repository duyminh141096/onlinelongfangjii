/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: LobbyController.cs
* Script Author: MinhLe 
* Created On: 8/17/2021 11:46:05 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class LobbyController : MonoBehaviour
{
    #region Singleton

    public static LobbyController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }

    #endregion


    public bool isCreateRoomSuccess = false;
    public bool  isJoinedLobby = false;

    #region UI Implement
    public string[] randomRoomName = { "Doi con dai cu danh di", "Ga thi dung vao keo nhuc", "Tao thach tui bay dam vo do" };
    public Button createRoomButton;
    public Button popupCreateRoomButton;
    public TMP_InputField roomNameTxt;

    public Button joinRoomButton;

    #endregion

   
    public GameObject popupCreateRoom;
    public GameObject prefabRoom;
    public Transform parentRoom;

    public GameObject prefabUserOnlineLobby;
    public Transform parentUserOnlineLobby;

    private int currentRoomId = 1000;

    [Header("UI User")]

    public TMP_Text userNameText;
    public TMP_Text eloText;
    public TMP_Text matchText;

    public TMP_Text nameOnline;
    public TMP_Text eloOnline;

    public void SetDataUIUserLobby(UserPublicInfo info)
    {
        userNameText.text = info.userName;
        eloText.text = info.elo.ToString();
        matchText.text = info.match.ToString();

        nameOnline.text = info.userName;
        eloOnline.text = info.elo.ToString();
    }

    private void Start()
    {
        DatabaseUtils.Instance.UnsubAllListen();
        int randomAccount = UnityEngine.Random.Range(1, 6);
        PlayerPrefs.SetString("ID", randomAccount.ToString("0000"));
        InitEventUI();
        ConnectToServerEvent();
    }

    private void InitEventUI()
    {
        createRoomButton.onClick.AddListener(() =>
        {
            CreateRoomEvent();
        });
        popupCreateRoomButton.onClick.AddListener(()=> {
            roomNameTxt.text = randomRoomName[Random.Range(0, randomRoomName.Length)];
            popupCreateRoom.SetActive(true);
        });

        joinRoomButton.onClick.AddListener(() => {
            JoinRoomEvent();
        });
    }

    //First get id from database
    //after database tra lai cai id
    //thi create room tu cai id do
    public void CreateRoomEvent()
    {
        GetRoomInLobbySnapShot();
    }

    public void QuickJoinEvent()
    {
        //Get room and find room with player 1/2
        //if not
        //Create new room
    }

    public void JoinRoomEvent()
    {
        Debug.Log("Current ID : " + currentRoomId);

        //check xem room con trong khong
        //neu con trong thi set id in child "player"
        DatabaseUtils.Instance.CheckRoomExistPlayer(currentRoomId);
    }

    public void SetCurrentIdRoomSelected(int id)
    {
        currentRoomId = id;
    }

    public void ConnectToServerEvent() {
        ConnectToServer();
    }

    public void LoadDataLoby()
    {

        GetDataPlayerInLobby(PlayerPrefs.GetString("ID"));
        //Get user online
        GetOnlineUser();
        //Get match exist
        GetRoomInLobby();
    }

    #region USER ONLINE
    public void DestroyListRoom()
    {
        foreach (Transform child in parentRoom)
        {
            Destroy(child.gameObject);
        }
    }

    public void DestroyListUser()
    {
 
        foreach (Transform child in parentUserOnlineLobby)
        {
            Destroy(child.gameObject);
        }
    }
    public void CreateRoomInLobbyUI(RoomInfo roomInfo)
    {
        RoomInfoChild clone = Instantiate(prefabRoom, parentRoom).GetComponent<RoomInfoChild>();
        clone.Init(roomInfo);
    }

    public void CreateUserInLobbyUI(UserInfo id)
    {
        OnlineUserChild clone = Instantiate(prefabUserOnlineLobby, parentUserOnlineLobby).GetComponent<OnlineUserChild>();
        clone.Init(id);
    }

    #endregion

    private void GetDataPlayerInLobby(string id)
    {
        DatabaseUtils.Instance.GetDataPlayerInLobby(id);
    }

    private void GetOnlineUser()
    {
        DatabaseUtils.Instance.GetOnlineUserFromServer();
    }

    private void GetRoomInLobby()
    {
        DatabaseUtils.Instance.SubGetRoomInLobby();
    }

    private void GetRoomInLobbySnapShot()
    {
         DatabaseUtils.Instance.GetRoomInLobbySnapshot();
    }

    public void SetIDToCreateRoom(int id)
    {
        RoomInfo room = new RoomInfo(Matchmode.Normal,
         roomNameTxt.text == "" ? randomRoomName[Random.Range(0, randomRoomName.Length)] : roomNameTxt.text,id);
        CreateRoom(room);
    }

    #region ASYNC REGION


    private async void ConnectToServer()
    {
        await DatabaseUtils.Instance.ConnectToServer(PlayerPrefs.GetString("ID"));

        if (!isJoinedLobby) return;
        LoadDataLoby();
    }


    private async void CreateRoom(RoomInfo room)
    {
        await DatabaseUtils.Instance.CreateRoom(room);

        if (!isCreateRoomSuccess)
        {
            Debug.Log("Create fail");
            return;
        }
        DatabaseUtils.Instance.LeaveTheLobby();
        PlayerPrefs.SetInt("RoomID", room.idRoom);
        LoadRoom();
    }

    public void LoadRoom()
    {
        DatabaseUtils.Instance.UnSubGetRoomInLobby();
        DatabaseUtils.Instance.UnsubGetOnlineUserFromServer();
        SceneManager.LoadScene("Room");
    }

    #endregion


    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.A))
        //    DatabaseUtils.Instance.LeaveTheLobby();
    }
}
