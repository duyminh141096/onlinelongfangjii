/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: RoomInfoChild.cs
* Script Author: MinhLe 
* Created On: 8/17/2021 12:59:32 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class RoomInfoChild : MonoBehaviour
{
    public TMP_Text idRoom;
    public TMP_Text nameRoom;
    public Button myButton;

    public RoomInfo info;

    private void EventButton()
    {
        myButton.onClick.AddListener(() =>
        {
            LobbyController.Instance.SetCurrentIdRoomSelected(info.idRoom);
        });
    }

    public void Init(RoomInfo info)
    {
        this.info = info;
        idRoom.text = info.idRoomName.ToString("0000");
        nameRoom.text = info.name;
        EventButton();
    }
}
