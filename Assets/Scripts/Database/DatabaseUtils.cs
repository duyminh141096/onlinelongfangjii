/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: DatabaseUtils.cs
* Script Author: MinhLe 
* Created On: 8/17/2021 11:19:19 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using System.Threading.Tasks;
using Firebase.Extensions;
using System.Linq;

public class DatabaseUtils : MonoBehaviour
{
    #region Singleton

    public static DatabaseUtils Instance;
    DatabaseReference reference;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        reference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    #endregion
    public Task CreateRoom(RoomInfo room)
    {
        string json = JsonUtility.ToJson(room);
        return reference.Child("Room").Child(room.idRoom.ToString()).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Create Room success ");
                LobbyController.Instance.isCreateRoomSuccess = true;
                return;
            }
            else
            {
                LobbyController.Instance.isCreateRoomSuccess = false;
                Debug.Log("Create Room failed ");
                return;
            }
        });
    }

    public void LeaveTheLobby()
    {
        string id = PlayerPrefs.GetString("ID");
        FirebaseDatabase.DefaultInstance.GetReference("Lobby").Child(id).RemoveValueAsync().ContinueWithOnMainThread(task => {
            if (task.IsFaulted)
            {
                Debug.Log("Failed remove data");
            }else if (task.IsCompleted)
            {
                Debug.Log("Success remove data");
            }
        });
    }

    public void GetRoomInLobbySnapshot()
    {
        FirebaseDatabase.DefaultInstance.GetReference("Room").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {

            }
            else if (task.IsCompleted)
            {
                List<RoomInfo> ListRoom = new List<RoomInfo>();
                DataSnapshot snapshot = task.Result;
              
                foreach (var chilSnapshow in snapshot.Children)
                {
                    RoomInfo room = JsonUtility.FromJson<RoomInfo>(chilSnapshow.GetRawJsonValue());
                    ListRoom.Add(room);
                }

                ListRoom = ListRoom.OrderBy(x => x.idRoom).ToList();
                int newID = 0;
                for (int i = 0; i < ListRoom.Count; i++)
                {
                    if(i != ListRoom[i].idRoom)
                    {
                        newID = i;
                        break;
                    }
                }
                newID = newID == 0 ? ListRoom.Count : newID;

                LobbyController.Instance.SetIDToCreateRoom(newID);
            }
        });
    }

    DatabaseReference RoomRef;
    DatabaseReference OnlineRef;
    DatabaseReference PlayerReadyRef; 
    DatabaseReference ChatRef;
    DatabaseReference HostInRoomRef;
    DatabaseReference ListenPlayerRoomRef;

    public void UnsubAllListen()
    {
        UnSubGetRoomInLobby();
        UnsubGetOnlineUserFromServer();
        UnsubListenChatRoom();
        UnsubListenHostInRoom();
        UnsubListenPlayerReady();
        UnsubListenPlayerInRoom();
    }

    public void SubGetRoomInLobby()
    {
        RoomRef = FirebaseDatabase.DefaultInstance.GetReference("Room");
        RoomRef.ValueChanged += GetRoomInLobby;
    }
    public void UnSubGetRoomInLobby()
    {
        if(RoomRef != null)
        {
            Debug.Log("Unsub Function GetRoomInLobby");
            RoomRef.ValueChanged -= GetRoomInLobby;
            RoomRef = null;
        }
      
    }
    public void GetRoomInLobby(object sender2, ValueChangedEventArgs e2)
    {
        if (e2.DatabaseError != null)
        {
            Debug.LogError(e2.DatabaseError.Message);
        }
        if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
        {
            //Get data and send to LobbyController
            LobbyController.Instance.DestroyListRoom();

            foreach (var childSnapshot in e2.Snapshot.Children)
            {
                RoomInfo roominfo = JsonUtility.FromJson<RoomInfo>(childSnapshot.GetRawJsonValue());
                //LobbyController.Instance.AddUser(iduser);
                LobbyController.Instance.CreateRoomInLobbyUI(roominfo);
            }
        }
    }

    public void GetDataPlayerInLobby(string  id)
    {
        FirebaseDatabase.DefaultInstance.GetReference("User").Child(id).Child("publicInfo").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                //Some thing wrong
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                UserPublicInfo playerData = JsonUtility.FromJson<UserPublicInfo>(snapshot.GetRawJsonValue());

                if (playerData == null)
                {
                    Debug.Log("Can get data player");
                    return;
                }
                LobbyController.Instance.SetDataUIUserLobby(playerData);
            }
        });
    }

    public void GetOnlineUserFromServer()
    {
        OnlineRef = FirebaseDatabase.DefaultInstance.GetReference("Lobby");

        OnlineRef.ValueChanged += HandleGetOnlineUserFromServer;
    }
    public void UnsubGetOnlineUserFromServer()
    {
        if (OnlineRef != null)
        {
            OnlineRef.ValueChanged -= HandleGetOnlineUserFromServer;
            OnlineRef = null;
        }
    }
    private void HandleGetOnlineUserFromServer(object sender2, ValueChangedEventArgs e2)
    {
        if (e2.DatabaseError != null)
        {
            Debug.LogError(e2.DatabaseError.Message);
        }
        if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
        {
            //Get data and send to LobbyController
            LobbyController.Instance.DestroyListUser();

            foreach (var childSnapshot in e2.Snapshot.Children)
            {
                UserInfo userInfo = JsonUtility.FromJson<UserInfo>(childSnapshot.GetRawJsonValue());
                //LobbyController.Instance.AddUser(iduser);
                LobbyController.Instance.CreateUserInLobbyUI(userInfo);
            }

            //
        }
    }

    public Task ConnectToServer(string userID)
    {
        UserInfo userInfo = new UserInfo();
        userInfo.userID = userID;
        string json = JsonUtility.ToJson(userInfo);
        return reference.Child("Lobby").Child(userID).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                LobbyController.Instance.isJoinedLobby = true;
                return;
            }
            else
            {
                LobbyController.Instance.isJoinedLobby = false;
                return;
            }
        });

    }
    public void CheckRoomExistPlayer(int roomid)
    {
        FirebaseDatabase.DefaultInstance.GetReference("Room").Child(roomid.ToString()).Child("player").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {

            }
            else if (task.IsCompleted)
            {
              
                DataSnapshot snapshot = task.Result;
                PlayerWaitingRoom playerData = JsonUtility.FromJson<PlayerWaitingRoom>(snapshot.GetRawJsonValue());

                if(playerData == null)
                {
                    Debug.Log("Can get data player");
                    return;
                }

                if(playerData.id == "None")
                {
                    //can Join
                    string id = PlayerPrefs.GetString("ID");
                    ConnectToRoomEvent(id, roomid);
                }

                Debug.Log("End Check exist Room");
            }
        });
    }

    public async void ConnectToRoomEvent(string id, int roomID)
    {
        await ConnectToRoom(id, roomID);
    }

    public Task ConnectToRoom(string userID, int roomID)
    {
        PlayerWaitingRoom playerData = new PlayerWaitingRoom();
        playerData.id = userID;
        string json = JsonUtility.ToJson(playerData);
        return reference.Child("Room").Child(roomID.ToString()).Child("player").SetRawJsonValueAsync(json).ContinueWithOnMainThread(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Join success, Load scene");
                PlayerPrefs.SetInt("RoomID", roomID);
                LobbyController.Instance.LoadRoom();
                return;
            }
            else
            {
                Debug.Log("Join failed");
                return;
            }
        });

    }
    public Task SendChatContent(string userName, string content)
    {
        int roomid = PlayerPrefs.GetInt("RoomID");
        ChatData chatData = new ChatData();
        chatData.userName = userName;
        chatData.content = content;

        string json = JsonUtility.ToJson(chatData);

        return reference.Child("Room").Child(roomid.ToString()).Child("Chat").Push().SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Chat OK");
                return;
            }
            else
            {
                Debug.Log("Chat failed");
                return;
            }
        });
    }

    public void ListenChatRoom()
    {
        int roomid = PlayerPrefs.GetInt("RoomID");
        ChatRef = FirebaseDatabase.DefaultInstance.GetReference("Room").Child(roomid.ToString()).Child("Chat");

        ChatRef.ValueChanged += HandleListenChatRoom;
    }
    public void UnsubListenChatRoom()
    {
        if (ChatRef != null)
        {
            ChatRef.ValueChanged -= HandleListenChatRoom;
            ChatRef = null;
        }
    }
    private void HandleListenChatRoom(object sender2, ValueChangedEventArgs e2)
    {

        if (e2.DatabaseError != null)
        {
            Debug.LogError(e2.DatabaseError.Message);
        }
        if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
        {
            //Get data and send to LobbyController
            ChatRoomSystem.Instance.DestroyChat();

            foreach (var childSnapshot in e2.Snapshot.Children)
            {
                ChatData chatData = JsonUtility.FromJson<ChatData>(childSnapshot.GetRawJsonValue());
                ChatRoomSystem.Instance.CreateChatContent(chatData.userName, chatData.content);
            }

        }
    }

    public void ListenHostInRoom(int roomID)
    {
        HostInRoomRef = FirebaseDatabase.DefaultInstance.GetReference("Room").Child(roomID.ToString()).Child("host").Child("id");

        HostInRoomRef.ValueChanged += HandleListenHostInRoom;
    }
    public void UnsubListenHostInRoom()
    {
        if (HostInRoomRef != null)
        {
            HostInRoomRef.ValueChanged -= HandleListenHostInRoom;
            HostInRoomRef = null;
        }
    }
    private void HandleListenHostInRoom(object sender2, ValueChangedEventArgs e2)
    {

        if (e2.DatabaseError != null)
        {
            Debug.LogError(e2.DatabaseError.Message);
        }
        if (e2.Snapshot != null)
        {
            //Player change data
            string playerID = e2.Snapshot.Value.ToString();
            if (playerID == null)
            {
                Debug.Log("Null ID data player");
                return;
            }

            if (playerID == "None")
            {

            }
            else
            {
                //Load data user
                LoadDataHost(playerID);
            }
        }
    }

    public void SubListenPlayerReady(int roomID)
    {
        PlayerReadyRef = FirebaseDatabase.DefaultInstance.GetReference("Room").Child(roomID.ToString()).Child("player").Child("readyStatus");
        PlayerReadyRef.ValueChanged += HandlePlayerReady; 
    }
    public void UnsubListenPlayerReady()
    {
        if (PlayerReadyRef != null)
        {
            Debug.Log("Unsub Function UnsubListenPlayerReady");
            PlayerReadyRef.ValueChanged -= HandlePlayerReady;
            PlayerReadyRef = null;
        }
    }
    private void HandlePlayerReady(object sender2, ValueChangedEventArgs e2)
    {
        if (e2.DatabaseError != null)
        {
            Debug.LogError(e2.DatabaseError.Message);
        }
        if (e2.Snapshot != null )
        {
            RoomController.Instance.PlayerChangeStatus(bool.Parse(e2.Snapshot.GetRawJsonValue()));
        }
    }

    public Task SetPlayerReady(bool ready, int roomID)
    {
        return reference.Child("Room").Child(roomID.ToString()).Child("player").Child("readyStatus").SetValueAsync(ready).ContinueWith( task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Change status OK");
                return;
            }
            else
            {
                Debug.Log("Change status failed");
                return;
            }
        });
    }

  
    public void ListenPlayerInRoom(int roomID)
    {
        ListenPlayerRoomRef = FirebaseDatabase.DefaultInstance.GetReference("Room").Child(roomID.ToString()).Child("player").Child("id");
        ListenPlayerRoomRef.ValueChanged += HandleListenPlayerInRoom;
    }
    public void UnsubListenPlayerInRoom()
    {
        if (ListenPlayerRoomRef != null)
        {
            Debug.Log("Unsub Function UnsubListenPlayerReady");
            ListenPlayerRoomRef.ValueChanged -= HandlePlayerReady;
            ListenPlayerRoomRef = null;
        }
    }

    private void HandleListenPlayerInRoom(object sender2, ValueChangedEventArgs e2)
    {
        if (e2.DatabaseError != null)
        {
            Debug.LogError(e2.DatabaseError.Message);
        }
        if (e2.Snapshot != null)
        {
            //Player change data
            string playerID = e2.Snapshot.Value.ToString();
            if (playerID == null)
            {
                Debug.Log("Null ID data player");
                return;
            }

            if (playerID == "None")
            {
                //reset data
                RoomController.Instance.PlayerLeavingRoom();
            }
            else
            {
                //Load data user
                LoadDataPlayer(playerID);
            }
        }
    }

    public void CheckHostingRoom(int roomID, string myID)
    {
        FirebaseDatabase.DefaultInstance.GetReference("Room").Child(roomID.ToString()).Child("host").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log("Fail gi do");
                //Neu fail thi leave room + destroy room.
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                PlayerWaitingRoom playerData = JsonUtility.FromJson<PlayerWaitingRoom>(snapshot.GetRawJsonValue());

                if (playerData == null)
                {
                    Debug.Log("Can get data player CheckHostingRoom");
                    return;
                }
                RoomController.Instance.SetHostingID(playerData.id == myID ? myID : "None");
                RoomController.Instance.AfterCheckHosting(playerData.id == myID);
            }
        });
    }

    public void LoadDataHost(string id)
    {
        FirebaseDatabase.DefaultInstance.GetReference("User").Child(id).Child("publicInfo").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                //Some thing wrong
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                UserPublicInfo playerData = JsonUtility.FromJson<UserPublicInfo>(snapshot.GetRawJsonValue());

                if (playerData == null)
                {
                    Debug.Log("Can get data player");
                    return;
                }

                RoomController.Instance.SetDataHost(playerData);
            }
        });
    }

    public void LoadDataPlayer(string id)
    {
        FirebaseDatabase.DefaultInstance.GetReference("User").Child(id).Child("publicInfo").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                //Some thing wrong
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                UserPublicInfo playerData = JsonUtility.FromJson<UserPublicInfo>(snapshot.GetRawJsonValue());

                if (playerData == null)
                {
                    Debug.Log("Can get data player");
                    return;
                }
                RoomController.Instance.SetDataPlayer(playerData);
                PlayerPrefs.SetString("UserName", playerData.userName);
            }
        });
    }

    public Task PushDataUser(UserInfo user)
    {
        string json = JsonUtility.ToJson(user);
        return reference.Child("User").Child(user.userID.ToString()).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Push success, Load scene");

                return;
            }
            else
            {
                Debug.Log("Push failed");
                return;
            }
        });

    }
    public void GetRoomInfo(int roomID)
    {
        FirebaseDatabase.DefaultInstance.GetReference("Room").Child(roomID.ToString()).GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                //Some thing wrong
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                RoomInfo playerData = JsonUtility.FromJson<RoomInfo>(snapshot.GetRawJsonValue());

                if (playerData == null)
                {
                    Debug.Log("Can get data player");
                    return;
                }
                RoomController.Instance.SetRoomInfo(playerData);
            }
        });
    }

}
