/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: CreateRecord.cs
* Script Author: MinhLe 
* Created On: 10/18/2020 9:33:30 AM*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
public class CreateRecord : MonoBehaviour
{
    private string pathData = "/Record";
    public Match currentMatch;

    [Header("Parent Component")]
    public GameObject parentPieces;
    public GameObject parentPiecesUI;
    [Header("UI")]
    public InputField textName;

    [Header("List GameObject Child")]
    public List<GameObject> Pieces = new List<GameObject>();
    
    private void ChangeMatchName(string name)
    {
        currentMatch.matchName = name;
    }
    public void SubmitSave()
    {
        if (!string.IsNullOrEmpty(textName.text))
        ChangeMatchName(textName.text);
        SaveRecord();
    }

    private void GetAllChild()
    {
        Clear();
        Pieces = GameObjectExtention.GetAllChild(parentPieces);
    }
    private void Clear()
    {
        Pieces.Clear();
    }

    public void RemoveRecord()
    {
        int index = currentMatch.games.Count - 1;
        if (index < 0) return;
        currentMatch.games.RemoveAt(index);
    }
    #region Execute Data
    private void ExecuteData(ref Table table)
    {
        ExecuteDataPieces(ref table);
        ExecuteDataPiecesUI(ref table);
    }
    private void ExecuteDataPieces(ref Table table)
    {
        if (Pieces.Count == 0 || Pieces == null) return;
        //Xu ly
        for (int i = 0; i < Pieces.Count; i++)
        {
            InfoPieces info = new InfoPieces();
            TeamUpList teamUp = Pieces[i].GetComponent<TeamUpList>();
            BasePieces pieces = Pieces[i].GetComponent<BasePieces>();

            if (!teamUp || !pieces) continue;
            info.info = teamUp.Clone();
            info.teamColor = pieces.mColor;
            info.stateRotate = pieces.MStateRotation;
            info.typePiece = pieces.mNamePiece;
            info.kindPiece = pieces.kind;
            info.pos = pieces.transform.localPosition;
            if (pieces.mCurrentCell != null) info.cellID = pieces.mCurrentCell.mId;
            else info.cellID = 141096;
            table.infoPieces.Add(info);
        }
    }

    private void ExecuteDataPiecesUI(ref Table table)
    {
        StorePieces store = parentPiecesUI.GetComponent<StorePieces>();
        if (!store) return;
        for (int i = 0; i < store.storesPiece.Count; i++)
        {
            InfoPiecesUI info = new InfoPiecesUI();
            info.namePiece = store.storesPiece[i].pieceName;
            info.teamColor = store.storesPiece[i].teamColor;
            info.count = store.storesPiece[i].count;
            table.InfoPiecesUI.Add(info);
        }
    }
    #endregion
    public Table SaveTable()
    {
        Debug.Log("Start Save Table");
        Table table = new Table();
        GetAllChild();
        ExecuteData(ref table);
        return table;
    }
    public void CreateFolder()
    {
        //create folder if it's not exist
        string path = Application.persistentDataPath + pathData;
        if (Directory.Exists(path))
        {
            Debug.Log("Exist Path");
        }
        else
        {
            Directory.CreateDirectory(path);
            Debug.Log("Create success , path : " + path);
        }
    }

    public void AddDataMatch(Table table , Turn turn)
    {
        currentMatch.AddData(table, turn);
    }
    public void RefeshMatch()
    {
        currentMatch = new Match();
    }
    #region Save
    public void SaveRecord()
    {
        string json = JsonUtility.ToJson(currentMatch);
        string matchID = "/" + Guid.NewGuid().ToString() + ".json";
        string path = Application.persistentDataPath + pathData + matchID;
        File.WriteAllText(path, json);
        Debug.Log("Save success : " + path);
        //RefeshMatch();
    }
    #endregion
}
