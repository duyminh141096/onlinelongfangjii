using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class InfoMatch
{
    public Match match;
    public string filelink;

    public InfoMatch(Match match, string path)
    {
        this.match = match;
        this.filelink = path;
    }
}
public partial class RecordControl : MonoBehaviour
{
    public void LoadScene()
    {
        SceneManager.LoadScene(1);
    }


    #region Variables
    private string pathData = "/Record";
    ////// Delegate region
    public delegate void EventListen();
    public static EventListen OnDelete;

    ////////////////////
    [Header("For History Panel")]
    public GameObject PrefabsButtonMatch;
    public Transform parentPrefabsButtonMatch;

    public static RecordControl Instance;


    public GameObject MatchHistoryUI;
    public GameObject MatchBoardUI;
    public GameObject StartPanel;
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    private void Start()
    {
        Init();
        LoadRecord();
        SetUpMatch();
    }
 
    #region Delete All Match
    public void DeleteAllMatch()
    {  
        //string[] files = Directory.GetFiles(Application.persistentDataPath + pathData + "/");
        //foreach (string file in files)
        //{
        //    File.Delete(file);
        //}
        //OnDelete?.Invoke();
        //UpdateStatus("Delete done ");
    }
    #endregion

    #region Load
    public void LoadRecord()
    {
        string[] pathfiles = Directory.GetFiles(Application.persistentDataPath + pathData, "*.json");
        ExecuteData(pathfiles);
    }

    private void ExecuteData(string[] pathfiles)
    {
        List<InfoMatch> matchs = new List<InfoMatch>();

        for (int i = 0; i < pathfiles.Length; i++)
        {
            matchs.Add(new InfoMatch(GetMatch(pathfiles[i]), pathfiles[i]));
        }
        matchs = matchs.OrderByDescending(x => Convert.ToDateTime(x.match.created)).ToList();

        GenMatch(matchs);
    }

    private void GenMatch(List<InfoMatch> matchs)
    {
        for (int i = 0; i < matchs.Count; i++)
        {
            GameObject buttonMatch = Instantiate(PrefabsButtonMatch, parentPrefabsButtonMatch);
            buttonMatch.GetComponent<PrefabMatch>().SetMatch(matchs[i]);
        }
    }

    public Match GetMatch(string pathMatch)
    {
        string json = File.ReadAllText(pathMatch);
        Match currentMatch = JsonUtility.FromJson<Match>(json);
        return currentMatch;
    }
    #endregion
    public void SetMatchName(Match currentMatch, string matchName)
    {
        currentMatch.matchName = matchName;
    }
    /// <summary>
    /// this funtion for testing
    /// </summary>
    /// <param name="matchs"></param>
    private void SortMatch(List<Match> matchs)
    {
        foreach (var item in matchs)
        {
            Debug.Log("Match Name : " + item.matchName + "Match Created : " + item.created);
        }
    }
}
