/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: PlayBarControl.cs
* Script Author: MinhLe 
* Created On: 10/16/2020 1:22:57 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayBarControl : MonoBehaviour
{
    [Header("UI")]
    public Toggle TogglePlay;
    public Button ButtonNext;
    public Button ButtonUndo;
    public Button ButtonFirst;
    public Button ButtonEnd;
    public Button PlayButton;
    public Slider SliderMatchTime;

    [Header("Resource")]
    public Sprite playSprite;
    public Sprite stopSprite;

    private bool isLoading = false;

    public void SetValueSlider(float value)
    {
        SliderMatchTime.value = value;
    }
    public void SetDefaultButtonPlay()
    {
        isLoading = true;
        TogglePlay.isOn = false;
        TogglePlay.image.sprite = stopSprite;
        isLoading = false;
    }
    private void Init()
    {
        PlayButton.onClick.AddListener(() => {
            TogglePlay.image.sprite = playSprite;
            RecordControl.Instance.InvokeMethod();
        });

        TogglePlay.onValueChanged.AddListener(x => {
            if (isLoading) return;
            TogglePlay.image.sprite = x ? playSprite : stopSprite;
            RecordControl.OpenPlayBar.ResetSleep();
            if (x)
                RecordControl.Instance.InvokeMethod();
            else
                RecordControl.Instance.CancelInvokeMethod();
        });
        ButtonNext.onClick.AddListener(() => 
        {
            RecordControl.OpenPlayBar.ResetSleep();
            RecordControl.Instance.ButtonNextAction();
        });
        ButtonUndo.onClick.AddListener(() => 
        { 
            RecordControl.OpenPlayBar.ResetSleep();
            RecordControl.Instance.ButtonPreviousAction();
        });
        ButtonFirst.onClick.AddListener(() => 
        { 
            RecordControl.OpenPlayBar.ResetSleep();
            RecordControl.Instance.ButtonFirstAction();
        });
        ButtonEnd.onClick.AddListener(() => 
        { 
            RecordControl.OpenPlayBar.ResetSleep();
            RecordControl.Instance.ButtonLastAction();
        });
    }

    void Start()
    {
        Init();
    }


}
