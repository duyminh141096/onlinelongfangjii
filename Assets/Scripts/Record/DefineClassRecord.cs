/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: DefineClassRecord.cs
* Script Author: MinhLe 
* Created On: 10/13/2020 10:51:20 PM*/
using System;
using System.Collections.Generic;
using UnityEngine;

#region define class

[System.Serializable]
public class Match
{
    public string matchName;
    public string created;
    public string mode;
    public List<Games> games;
    public Match()
    {
        matchName = "Match Challenge # " + DateTime.Now.ToString();
        created = DateTime.Now.ToString();
        mode = "Challenge";
        games = new List<Games>();
    }
    public void RandomGames()
    {
        for (int i = 0; i < UnityEngine.Random.Range(0,50); i++)
        {
            AddData(new Table(), new Turn());
        }
        created = DateTime.Now.ToString();
        mode = "Challenge";
        matchName = "Match #" + UnityEngine.Random.Range(0, 100);
    }
    public void AddData(Table table, Turn turn)
    {
        games.Add(new Games(table, turn));
    }
}
[System.Serializable]
public class Games
{
    public Turn turn;
    public Table table;
    public Games(Table table, Turn turn)
    {
        this.turn = turn;
        this.table = table;
    }
    public void AddData(Table table, Turn turn)
    {
        this.table = table;
        this.turn = turn;
    }
}
[System.Serializable]
public class Turn
{
    public PieceInfos targetPiece;
    public int targetCell;//ID Cell
    public mode mode;
    public int dir;

    public ClickSiteRot clickSiteRot = ClickSiteRot.None;
    public Turn()
    {
        targetPiece = new PieceInfos();
        clickSiteRot = ClickSiteRot.None;
    }
    public void SetPieceInfo(PieceInfos targetPiece)
    {
       this.targetPiece.SetValue(targetPiece);
    }
    public Turn Clone()
    {
        Turn clone = new Turn();
        clone.targetPiece = targetPiece.Clone();
        clone.targetCell = targetCell;
        clone.mode = mode;
        clone.dir = dir;
        clone.clickSiteRot = clickSiteRot;
        return clone;
    }
}
[System.Serializable]
public class Table
{
    //get all object and save in json
    public List<InfoPieces> infoPieces;
    //public List<InfoCellsNormal> infoCellsNormal;
    public List<InfoPiecesUI> InfoPiecesUI;

    public Table()
    {
        infoPieces = new List<InfoPieces>();
       // infoCellsNormal = new List<InfoCellsNormal>();
        InfoPiecesUI = new List<InfoPiecesUI>();
    }
    public void Clear()
    {
        infoPieces.Clear();
        InfoPiecesUI.Clear();
    }
    
}
[System.Serializable]
public class InfoPieces
{
    //Script TeamUp
    public List<PieceInfo> info = new List<PieceInfo>();
    //////////////
    //Script Base Piece
    public bool teamColor;
    public int stateRotate;
    public string typePiece;
    public Kind kindPiece;
    //////////////////
    public int cellID;
    public Vector3 pos;
}
[System.Serializable]
public class InfoCellsNormal
{
    public int cellID;

}
[System.Serializable]
public class InfoPiecesUI
{
    public string namePiece;
    public int count;
    public bool teamColor;
}
#endregion
[System.Serializable]
public class PieceInfos
{
    public string namePiece;
    public bool teamColor;
    public Kind kind;
    public Vector3 pos;
    public void SetValue(BasePieces piece)
    {
        namePiece = piece.mNamePiece;
        teamColor = piece.mColor;
        pos = piece.transform.localPosition;
        kind = piece.kind;
    }

    public PieceInfos Clone()
    {
        PieceInfos clone = new PieceInfos();
        clone.namePiece = namePiece;
        clone.teamColor = teamColor;
        clone.kind = kind;
        clone.pos = pos;
        return clone;
    }

    public void SetValue(PieceInfos targetPiece)
    {
        namePiece = targetPiece.namePiece;
        teamColor = targetPiece.teamColor;
        pos = targetPiece.pos;
        kind = targetPiece.kind;
    }
}
