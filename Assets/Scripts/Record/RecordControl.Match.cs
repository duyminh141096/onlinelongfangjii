/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: RecordControl.Match.cs
* Script Author: MinhLe 
* Created On: 10/16/2020 1:34:26 AM*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class RecordControl
{
    [Header("Control Record")]
    public GameObject PlayBarControlGO;
    public GameObject OpenPlayBarGO;
    public PiecesManager PiecesManager;
    public StorePieces StorePieces;
    [Header("Piece Prefab")]
    public GameObject CellPrefab;
    public Match currentMatch;
    [Header("Transform Pieces UI")]
    public Transform whiteBoard;
    public Transform blackBoard;
    public Transform Board;
    public GameObject parentPieces;
    public GameObject parentPiecesUIWhite;
    public GameObject parentPiecesUIBlack;
    public static int playRecordMode;
    private void Init()
    {
        PlayBarControl = PlayBarControlGO.GetComponent<PlayBarControl>();
        OpenPlayBar = OpenPlayBarGO.GetComponent<OpenPlayBar>();
    }

    public static PlayBarControl PlayBarControl;
    public static OpenPlayBar OpenPlayBar;
    int mainCellID = 0;
    public List<Cell> allCell = new List<Cell>();


    public void SetUpMatch()
    {
        CreatePiecesUI();
        CreateBoard();
    }
    private void CreatePiecesUI()
    {
        PiecesManager.Spawn(true, 3, whiteBoard);
        PiecesManager.Spawn(false, 1, blackBoard);
        StorePieces.UpdateText();
    }
    private void CreateBoard()
    {
        //create Board
        for (int y = 0; y < 9; y++)
        {
            for (int x = 0; x < 9; x++)
            {
                GameObject newCell = Instantiate(CellPrefab, Board);
              
                newCell.name = x.ToString() + ":" + y.ToString();
                mainCellID++;

                //position
                RectTransform rectTransform = newCell.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector2((x * 90) + 45, (y * 90) + 45);

                //setup
                allCell.Add(newCell.GetComponent<Cell>());
                newCell.GetComponent<Cell>().Setup(new Vector2Int(x, y), mainCellID);
            }
        }
    }

    private void DeleteAllPieces()
    {
        List<GameObject> pieces = parentPieces.GetAllChild();
        if (pieces.Count == 0) return;
        for (int i = 0; i < pieces.Count; i++)
        {
            Destroy(pieces[i]);
        }
    }

    public void LoadTable(Table table)
    {
        DeleteAllPieces();

        if (table.infoPieces.Count > 0)
        {
            //create Pieces
            CreatePiecesWithInfo(table.infoPieces);
            //set current pieces
            SetValueStorePieces(table.InfoPiecesUI);
        }
    }

    private void SetValueStorePieces(List<InfoPiecesUI> infoPiecesUI)
    {
        var store = StorePieces.storesPiece;
        if (store.Count == 0 || infoPiecesUI.Count == 0) return;
        for (int i = 0; i < store.Count; i++)
        {
            for (int j = 0; j < infoPiecesUI.Count; j++)
            {
                if(store[i].pieceName ==  infoPiecesUI[j].namePiece && store[i].teamColor == infoPiecesUI[j].teamColor)
                {
                    store[i].count = infoPiecesUI[j].count;
                }
            }
        }
        StorePieces.UpdateText();
    }
    public BasePieces GetPieces()
    {
        Kind kind = currentMatch.games[currentTurn].turn.targetPiece.kind;
        List<GameObject> listPieces = new List<GameObject>();
 
        if(kind == Kind.Normal)
        {
            listPieces = parentPieces.GetAllChild();
        }
        else
        {
            var listWhite = parentPiecesUIWhite.GetAllChild();
            var listBlack = parentPiecesUIBlack.GetAllChild();
            listPieces.AddRange(listWhite);
            listPieces.AddRange(listBlack);
        }
        for (int i = 0; i < listPieces.Count; i++)
        {
            if (CheckPiece(listPieces[i]))
            {
                return listPieces[i].GetComponent<BasePieces>();
            }
        }
        return null;
    }
    private bool CheckPiece(GameObject piece)
    {
        BasePieces basePieces = piece.GetComponent<BasePieces>();
        if (!basePieces) return false;
        PieceInfos pieceInfos = currentMatch.games[currentTurn].turn.targetPiece;
        if (basePieces.mNamePiece == pieceInfos.namePiece && basePieces.mColor == pieceInfos.teamColor && basePieces.transform.localPosition == pieceInfos.pos)
            return true;
        return false;
    }
    public Cell GetCell(int id)
    {
        for (int i = 0; i < allCell.Count; i++)
        {
            if (allCell[i].mId == id) return allCell[i];
        }
        return null;
    }
    private void CreatePiecesWithInfo(List<InfoPieces> infoPieces)
    {
        for (int i = 0; i < infoPieces.Count; i++)
        {
            GameObject piece = PiecesManager.CreateBasePiece(infoPieces[i].teamColor, infoPieces[i].stateRotate, infoPieces[i].typePiece, infoPieces[i].kindPiece);
            //update info teamUp
            TeamUpList teamUp = piece.GetComponent<TeamUpList>();
            if (teamUp) teamUp.AddNewPiecesList(infoPieces[i].info);
            Cell targetCell = GetCell(infoPieces[i].cellID);
            if (targetCell) piece.GetComponent<BasePieces>().PlaceNoVfx(targetCell);
            else piece.transform.localPosition = infoPieces[i].pos;
        }
    }
    int currentTurn = 0;

    public void InvokeMethod()
    {
        InvokeRepeating("ActionMove", 3f,3f);
        playRecordMode = 1;
    }
    public void CancelInvokeMethod()
    {
        CancelInvoke("ActionMove");
        playRecordMode = 0;
    }

    ////////////////////////
    ///Nut play => neu nhan vao thi cancel Invoke hien tai
    ///Unplay => Neu nhan vao thi goi ham Invoke su kien 
    ///Button Undo => cancel invoke hien tai + load table + set lai turn- 1 + invoke method
    ///Button next => tuong tu
    ///Button First => Tuong tu 2 cai tren luon


    public void ButtonNextAction()
    {
        int maxTurn = currentMatch.games.Count - 1;
        CancelInvokeMethod();
        currentTurn = currentTurn +  1 >= maxTurn ? maxTurn : currentTurn + 1;
        LoadTable(currentMatch.games[currentTurn].table);
        UpdateSlider();
        if (playRecordMode == 1) InvokeMethod();
    }

    public void ButtonPreviousAction()
    {
        int minTurn = 0;
        CancelInvokeMethod();
        currentTurn = currentTurn - 1 <= minTurn ? minTurn : currentTurn - 1;
        LoadTable(currentMatch.games[currentTurn].table);
        UpdateSlider(); 
        if(playRecordMode == 1 ) InvokeMethod();
    }

    public void ButtonFirstAction()
    {
        CancelInvokeMethod();
        currentTurn = 0;
        LoadTable(currentMatch.games[currentTurn].table);
        UpdateSlider();
        if (playRecordMode == 1) InvokeMethod();
    }

    public void ButtonLastAction()
    {
        CancelInvokeMethod();
        currentTurn = currentMatch.games.Count - 1;
        LoadTable(currentMatch.games[currentTurn].table);
        UpdateSlider();
        if (playRecordMode == 1) InvokeMethod();
    }
    private void UpdateSlider()
    {
        float maxTurn = currentMatch.games.Count;
        float indexTurn = currentTurn;
        PlayBarControl.SetValueSlider(indexTurn / maxTurn);
    }
    public void ActionMove()
    {
        int maxTurn = currentMatch.games.Count -1;
        if (currentTurn < 0) return ;
        if (currentTurn > maxTurn) return;
        BasePieces targetPiece = GetPieces();
        Cell targetCell = GetCell(currentMatch.games[currentTurn].turn.targetCell);
        Turn turn = currentMatch.games[currentTurn].turn;
        switch (turn.mode)
        {
            case mode.MoveSingle:
                StartCoroutine(MoveSingleBot(targetPiece, targetCell, turn.dir));
                break;
            case mode.MoveSingleinTeam:
                StartCoroutine(MoveSingleinteamBot(targetPiece, targetCell, turn.dir));
                break;
            case mode.MoveTeam:
                StartCoroutine(MoveTeamBot(targetPiece, targetCell, turn.dir,turn));
                break;
            case mode.MoveSingleandKill:
                // data.piece.MoveOnlyAndKill(data.cell);
                StartCoroutine(MoveOnlyAndKillBot(targetPiece, targetCell, turn.dir));
                break;
            case mode.MoveTeamandKill:
                StartCoroutine(MoveTeamAndKillBot(targetPiece, targetCell, turn.dir, turn));
                break;
            case mode.RotationPiece:
                StartCoroutine(RotationPiece(targetPiece, targetCell, turn.dir, turn));
                break;
            case mode.TeamUp:
                StartCoroutine(TeamUpBot(targetPiece, targetCell, turn.dir));
                break;
            case mode.MoveFromStoreToCell:
                StartCoroutine(MoveFromStoretoCellBot(targetPiece, targetCell, turn.dir));
                break;
            case mode.TeamUpFormStoreToCell:
                StartCoroutine(TeamupFromStoretoCellBot(targetPiece, targetCell, turn.dir));
                break;
            case mode.Setupincell:
                StartCoroutine(Setupincell(targetPiece, targetCell));
                break;
            case mode.SetupTeamUp:
                StartCoroutine(SetupTeamUp(targetPiece, targetCell));
                break;
           // case mode.RotationAllPieceRight:
            //    StartCoroutine(RotationPieceAllPieceRight(targetPiece, targetCell, turn.dir));
                break;
           // case mode.RotationAllPieceLeft:
           //     StartCoroutine(RotationPieceAllPieceLeft(targetPiece, targetCell, turn.dir));
                break;
        }

        StartCoroutine(EndAction());
    }

    private IEnumerator EndAction()
    {
        yield return new WaitForSeconds(1f);
        currentTurn++;
        UpdateSlider();
        if (currentTurn >= currentMatch.games.Count) CancelInvokeMethod();//max
    }

    public void EventOutMatch()
    {
        CancelInvokeMethod();
        DeleteAllPieces();
    }

    public void SetMatch(Match match)
    {
        currentMatch = match;
        MatchHistoryUI.SetActive(false);
        currentTurn = 0;
        LoadTable(currentMatch.games[currentTurn].table);
        UpdateSlider();
        MatchBoardUI.SetActive(true);
        StartPanel.SetActive(true);
        PlayBarControl.SetDefaultButtonPlay();
        //PlayMatch
    }

}
