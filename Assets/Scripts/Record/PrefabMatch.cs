/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: PrefabMatch.cs
* Script Author: MinhLe 
* Created On: 10/13/2020 10:53:01 PM*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PrefabMatch : MonoBehaviour
{
    public Match match;
    public Button click;

    public string fileLink;


    [Header("UI Element")]
    public Text MatchName;
    public Text MatchNameClone;

    public Text Moved;
    public Text MovedClone;

    public Text Mode;
    public Text ModeClone;

    public Text DayCreated;
    public Text DayCreatedClone;

    public Button DeleteButton;
    private void Start()
    {
        click.onClick.AddListener(() => PlayMatch());
        RecordControl.OnDelete += DeleteMySelft;
        DeleteButton.onClick.AddListener(() =>DeleteEventButton());
    }

    private void DeleteEventButton()
    {
        if (File.Exists(fileLink))
        {
            File.Delete(fileLink);
        }
        DeleteMySelft();
    }

    public void DeleteMySelft()
    {
        Destroy(this.gameObject);
    }
    private void OnDestroy()
    {
        RecordControl.OnDelete -= DeleteMySelft;
    }

    private void PlayMatch()
    {
        if (match.games.Count == 0) return;
        //show pop up and set Match
        RecordControl.Instance.SetMatch(match);
    }

    public void SetMatch(InfoMatch match)
    {
        this.match = match.match;
        fileLink = match.filelink;
        FillData();
    }
    public void FillData()
    {
        FillInput(match.matchName, MatchName, MatchNameClone);
        FillInput(match.mode, Mode, ModeClone);
        FillInputDay();
        FillInputMove();
    }

    private void FillInputDay()
    {
       DateTime dateTime =  Convert.ToDateTime(match.created);
        string dayTimeConvert = dateTime.ToString("dd/MM/yyyy hh:mm tt");
        DayCreated.text = dayTimeConvert;
        DayCreatedClone.text = dayTimeConvert;
    }

    private void FillInput(string value , Text main , Text clone)
    {
        string Name = UpperFirstCase(value);
        main.text = Name;
        clone.text = Name;
    }
    private void FillInputMove()
    {
        string count = match.games.Count.ToString();
        Moved.text = count + " Move";
        MovedClone.text = count + " Move";
    }
    private string UpperFirstCase(string value)
    {
        string name = value;
        if (!string.IsNullOrEmpty(name))
        {
            name = char.ToUpper(name[0]) + name.Substring(1);
        }
        return name;
    }
}
