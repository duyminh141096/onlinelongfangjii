/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: OpenPlayBar.cs
* Script Author: MinhLe 
* Created On: 10/16/2020 12:43:45 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

public class OpenPlayBar :MonoBehaviour,IPointerDownHandler
{
    public GameObject playbar;
    public float deltaTime;
    public float sleepTime;
    public void OnPointerDown(PointerEventData eventData)
    {
        PlayBarActive();
    }
    private void PlayBarActive()
    {
        if (playbar.activeSelf)
        {
            CancelInvoke();
            playbar.transform.DOLocalMoveY(-650, deltaTime).OnComplete(EventComplete);
        }
        else
        {
            EventComplete();
            playbar.transform.DOLocalMoveY(-450, deltaTime);
            Invoke("PlayBarActive", sleepTime);
        }
    }

    public void ResetSleep()
    {
        CancelInvoke();
        Invoke("PlayBarActive", sleepTime);
    }

    void EventComplete()
    {
        playbar.SetActive(!playbar.activeSelf);
    }
}
