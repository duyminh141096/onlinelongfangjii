/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: RecordControl.Match.cs
* Script Author: MinhLe 
* Created On: 10/16/2020 1:34:26 AM*/
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class RecordControl
{
    public IEnumerator MoveSingleinteamBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        if (targetCell.mCurrentPiece == null)
        {
            GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

            PiecesManager.rotation = false;

            if (newPiece.GetComponent<BasePieces>() != null)
            {
                newPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                //newPiece.GetComponent<BasePieces>().Move(targetCell, false, false);
                MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, false);
                
            }
            basePieces.mListPiece.RemovePieceList();

            if (basePieces.mListPiece.GetListNameCount() > 1)
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
                basePieces.mListPiece.RemovePieceList();
                newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(basePieces.mListPiece.GetPieceNamesList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
            }
            else
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
                
            }
            basePieces.gameObject.SetActive(false);
            string namePiece = newPiece.GetComponent<BasePieces>().mNamePiece;
            //check Rotation
            if (namePiece != "Vuong" && namePiece != "Long" && namePiece != "Phuong")
            {
                if (newPiece.GetComponent<BasePieces>().MStateRotation != newDir)
                {
                    //rotation
                    yield return new WaitForSeconds(0.3f);
                    CheckRotation(newDir, newPiece.GetComponent<BasePieces>());
                }
            }
            Destroy(basePieces.gameObject);
           
        }
    }
    public IEnumerator MoveTeamBot(BasePieces basePieces, Cell targetCell, int newDir, Turn turn)
    {
        if (targetCell.mCurrentPiece == null)
        {
            PiecesManager.rotation = false;

            basePieces.mCurrentCell.mCurrentPiece = null;
            basePieces.mCurrentCell = targetCell;
            basePieces.mCurrentCell.mCurrentPiece = basePieces;

            basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
            SoundController.Instance.PlayVfxClip(2);
            yield return new WaitForSeconds(0.3f);

            basePieces.mTargetCell = null;
            basePieces.mIsPlace = true;
            basePieces.ClearCells();
            if (basePieces.mNamePiece != "Vuong" && basePieces.mNamePiece != "Long" && basePieces.mNamePiece != "Phuong")
            {
                if (basePieces.MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, basePieces, turn);
                }
            }
        }
    }
    public IEnumerator MoveOnlyAndKillBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        if (targetCell.mCurrentPiece != null)
        {
            BasePieces targetPiece = targetCell.mCurrentPiece;
            GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
            PiecesManager.rotation = false;

            if (newPiece.GetComponent<BasePieces>() != null)
            {
                newPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, true);
                
            }

            basePieces.mListPiece.RemovePieceList();

            if (basePieces.mListPiece.GetListNameCount() > 1)
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());
                basePieces.mListPiece.RemovePieceList();
                newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(basePieces.mListPiece.GetPieceNamesList());
                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }

            }
            else
            {
                GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.mListPiece.GetPieceListRotation(), basePieces.mListPiece.GetPieceList());

                if (newOrderPiece.GetComponent<BasePieces>() != null)
                {
                    newOrderPiece.GetComponent<BasePieces>().Place(basePieces.mCurrentCell);
                }
            
            }
            basePieces.gameObject.SetActive(false);
            string namePiece = newPiece.GetComponent<BasePieces>().mNamePiece;
            targetPiece.Kill(targetPiece.mListPiece.GetPieceNamesList(), null);
            if (namePiece != "Vuong" && namePiece != "Long" && namePiece != "Phuong")
            {
                if (newPiece.GetComponent<BasePieces>().MStateRotation != newDir)
                {
                    //rotation
                    yield return new WaitForSeconds(0.3f);
                    CheckRotation(newDir, newPiece.GetComponent<BasePieces>());
                }
              
            }
            Destroy(basePieces.gameObject);
        }
    }
    public IEnumerator MoveTeamAndKillBot(BasePieces basePieces, Cell targetCell, int newDir, Turn turn)
    {
        if (targetCell.mCurrentPiece != null)
        {
            BasePieces targetPiece = targetCell.mCurrentPiece;
          
            targetPiece.Kill(targetPiece.mListPiece.GetPieceNamesList(), null);
            PiecesManager.rotation = false;
            //basePieces.mCurrentCell.mCurrentPiece = null;
            basePieces.mCurrentCell = targetCell;
            basePieces.mCurrentCell.mCurrentPiece = basePieces;
            basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
            SoundController.Instance.PlayVfxClip(2);
            yield return new WaitForSeconds(0.3f);
   
            basePieces.mTargetCell = null;
            basePieces.ClearCells();

            if (basePieces.mNamePiece != "Vuong" && basePieces.mNamePiece != "Long" && basePieces.mNamePiece != "Phuong")
            {
                if (basePieces.MStateRotation != newDir)
                {
                    //rotation
                    CheckRotation(newDir, basePieces, turn);
                }
            }
        }
    }
    public IEnumerator TeamUpBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        PiecesManager.rotation = false;
        basePieces.TeamUp(targetCell.mCurrentPiece.mListPiece.GetPieceNamesList());//teamup
        if (basePieces.mCurrentCell != null)
            basePieces.mCurrentCell.mCurrentPiece = null;
        //switch cells
        BasePieces destroyPieces = targetCell.mCurrentPiece;
        //Destroy(targetCell.mCurrentPiece.gameObject);
        basePieces.mCurrentCell = targetCell;
        basePieces.mCurrentCell.mCurrentPiece = basePieces;
        basePieces.transform.SetParent(PiecesManager.transform);
        basePieces.transform.DOMove(targetCell.transform.position, 0.3f);
        SoundController.Instance.PlayVfxClip(2);
        yield return new WaitForSeconds(0.3f);
        Destroy(destroyPieces.gameObject);
        basePieces.mIsPlace = true;
        basePieces.ClearCells();

        string NamePiece = basePieces.mNamePiece;
        if (NamePiece != "Long" && NamePiece != "Phuong" && NamePiece != "Vuong")
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
            }
        }
    }
    private IEnumerator RotationPiece(BasePieces targetPiece, Cell targetCell, int dir, Turn turn)
    {
        yield return new WaitForSeconds(0.1f);
        string NamePiece = targetPiece.mNamePiece;
        if (NamePiece != "Long" && NamePiece != "Phuong" && NamePiece != "Vuong")
        {
            if (targetPiece.MStateRotation != dir)
            {
                //rotation
                CheckRotation(dir, targetPiece, turn);
            }
        }
    }
    private IEnumerator RotationPieceAllPieceLeft(BasePieces targetPiece, Cell targetCell, int dir)
    {
        yield return new WaitForSeconds(0.1f);
        string NamePiece = targetPiece.mNamePiece;
        if (NamePiece != "Long" && NamePiece != "Phuong" && NamePiece != "Vuong")
        {
            if (targetPiece.MStateRotation != dir)
            {
                //rotation
                CheckRotationAllPieceLeft(dir, targetPiece);
            }
        }
    }
    private IEnumerator RotationPieceAllPieceRight(BasePieces targetPiece, Cell targetCell, int dir)
    {
        yield return new WaitForSeconds(0.1f);
        string NamePiece = targetPiece.mNamePiece;
        if (NamePiece != "Long" && NamePiece != "Phuong" && NamePiece != "Vuong")
        {
            if (targetPiece.MStateRotation != dir)
            {
                //rotation
                CheckRotationAllPieceRight(dir, targetPiece);
            }
        }
    }
    public IEnumerator MoveFromStoretoCellBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        PiecesManager.rotation = false;
        StorePieces.Instance.ShowSelectCell(basePieces.transform, false);
        GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.MStateRotation, basePieces.mNamePiece);
        newPiece.GetComponent<BasePieces>().isDropPiece = true;
        //gameController.SelectedPiece = newPiece.GetComponent<BasePieces>();
        newPiece.transform.position = basePieces.transform.position;
        basePieces.ClearCells();
        //gameController.SelectedCellColor(false);
        BasePieces Pieces = newPiece.GetComponent<BasePieces>();
        // newPiece.GetComponent<BasePieces>().Move(targetCell, false, false);
        MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, false, false);
        yield return new WaitForSeconds(0.3f);
        StorePieces.Instance.Remove(basePieces.mNamePiece, basePieces.mColor);
        // gameController.ClearListHightLightCell();
        string NamePiece = Pieces.mNamePiece;
        if (NamePiece != "Long" && NamePiece != "Phuong" && NamePiece != "Vuong")
        {
            if (Pieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, Pieces);
            }
        }
    }
    public IEnumerator TeamupFromStoretoCellBot(BasePieces basePieces, Cell targetCell, int newDir)
    {
        //create new piece
        StorePieces.Instance.ShowSelectCell(basePieces.transform, false);
        GameObject newPiece = PiecesManager.instance.CreateBasePiece(basePieces.mColor, basePieces.MStateRotation, basePieces.mNamePiece);
        newPiece.GetComponent<BasePieces>().isDropPiece = true;
  
        newPiece.transform.position = basePieces.transform.position;
        newPiece.GetComponent<BasePieces>().TeamUp(targetCell.mCurrentPiece.mListPiece.GetPieceNamesList());//teamup

        GameObject destroyObject = targetCell.mCurrentPiece.gameObject;
        MoveBot(newPiece.GetComponent<BasePieces>(), targetCell, true, false);
        yield return new WaitForSeconds(0.3f);

        Destroy(destroyObject);
        
        StorePieces.Instance.Remove(basePieces.mNamePiece, basePieces.mColor);//remove from store
        basePieces.ClearCells();
   
        BasePieces Pieces = newPiece.GetComponent<BasePieces>();

        string NamePiece = Pieces.mNamePiece;
        if (NamePiece != "Long" && NamePiece != "Phuong" && NamePiece != "Vuong")
        {
            if (Pieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, Pieces);
          
            }
  
        }
    }
    public IEnumerator MoveSingleBot(BasePieces basePieces, Cell targetcell, int newDir)
    {
        MoveBot(basePieces, targetcell, false, false);
        SoundController.Instance.PlayVfxClip(2);
        yield return new WaitForSeconds(0.3f);
        string NamePiece = basePieces.mNamePiece;
        if (NamePiece != "Long" && NamePiece != "Phuong" && NamePiece != "Vuong")
        {
            if (basePieces.MStateRotation != newDir)
            {
                //rotation
                CheckRotation(newDir, basePieces);
            }
        }
    }
    public IEnumerator SetupTeamUp(BasePieces targetPiece, Cell targetCell)
    {
        targetPiece.TeamUp(targetCell.mCurrentPiece.mListPiece.GetPieceNamesList());//teamup
        if (targetPiece.mCurrentCell != null)
            targetPiece.mCurrentCell.mCurrentPiece = null;
        //switch cells
        GameObject destroyObject = targetCell.mCurrentPiece.gameObject;
        
        targetPiece.mCurrentCell = targetCell;
        targetPiece.mCurrentCell.mCurrentPiece = targetPiece;

        targetPiece.transform.DOMove(targetCell.transform.position, 0.29f);
        yield return new WaitForSeconds(0.3f);
        Destroy(destroyObject);
        targetPiece.Place(targetCell);

        targetPiece.mIsPlace = true;

    }

    public IEnumerator Setupincell(BasePieces targetPiece, Cell targetCell)
    {
        targetPiece.transform.DOMove(targetCell.transform.position, 0.29f);
        yield return new WaitForSeconds(0.3f);
        targetPiece.Place(targetCell);
    }
    public bool CheckRotationAllPieceLeft(int targetDir,BasePieces basePieces)
    {
        switch (basePieces.MStateRotation)
        {
            case 1:
                basePieces.MStateRotation = 4;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation, true, true);
                //mLeftButton.onClick.RemoveAllListeners();
                break;
            case 2:
                basePieces.MStateRotation = 1;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation, true, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
            case 3:
                basePieces.MStateRotation = 2;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation, true, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
            case 4:
                basePieces.MStateRotation = 3;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation, true, true);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
        }
        return false;
    }
    public bool CheckRotationAllPieceRight(int targetDir, BasePieces basePieces)
    {
        switch (basePieces.MStateRotation)
        {
            case 1:
                basePieces.MStateRotation = 2;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation, false, true);
                break;
            case 2:
                basePieces.MStateRotation = 3;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation, false, true);
                break;
            case 3:
                basePieces.MStateRotation = 4;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation, false, true);
                break;
            case 4:
                basePieces.MStateRotation = 1;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation, false, true);
                break;

        }
        return false;
    }

    public bool CheckRotation(int targetDir, BasePieces basePieces , Turn turn = null)
    {
        if (turn != null)
        {
            if (turn.clickSiteRot != ClickSiteRot.None)
            {
                bool left = turn.clickSiteRot == ClickSiteRot.Left ? true : false;
                if (left)
                {
                    switch (basePieces.MStateRotation)
                    {
                        case 1:
                            basePieces.MStateRotation = 4;
                            basePieces.GetandSetStateRotaion(basePieces.MStateRotation, true, true);
                            //mLeftButton.onClick.RemoveAllListeners();
                            return true;
                        case 2:
                            basePieces.MStateRotation = 1;
                            basePieces.GetandSetStateRotaion(basePieces.MStateRotation, true, true);
                            // mLeftButton.onClick.RemoveAllListeners();
                            return true;
                        case 3:
                            basePieces.MStateRotation = 2;
                            basePieces.GetandSetStateRotaion(basePieces.MStateRotation, true, true);
                            // mLeftButton.onClick.RemoveAllListeners();
                            return true;
                        case 4:
                            basePieces.MStateRotation = 3;
                            basePieces.GetandSetStateRotaion(basePieces.MStateRotation, true, true);
                            // mLeftButton.onClick.RemoveAllListeners();
                            return true;
                    }
                }
                else
                {
                    switch (basePieces.MStateRotation)
                    {
                        case 1:
                            basePieces.MStateRotation = 2;
                            basePieces.GetandSetStateRotaion(basePieces.MStateRotation, false, true);
                            return true;
                        case 2:
                            basePieces.MStateRotation = 3;
                            basePieces.GetandSetStateRotaion(basePieces.MStateRotation, false, true);
                            return true;
                        case 3:
                            basePieces.MStateRotation = 4;
                            basePieces.GetandSetStateRotaion(basePieces.MStateRotation, false, true);
                            return true;
                        case 4:
                            basePieces.MStateRotation = 1;
                            basePieces.GetandSetStateRotaion(basePieces.MStateRotation, false, true);
                            return true;
                    }
                }
            }
            else
            {
                return RotPiece(targetDir, basePieces);
            }
        }
        else
        {
            return RotPiece(targetDir, basePieces);
        }
        return false;
    }

    private bool RotPiece (int targetDir, BasePieces basePieces)
    {
        switch (basePieces.MStateRotation)
        {
            case 1:

                basePieces.MStateRotation = targetDir;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                return true;

            case 2:

                basePieces.MStateRotation = targetDir;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                return true;

            case 3:

                basePieces.MStateRotation = targetDir;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                return true;

            case 4:

                basePieces.MStateRotation = targetDir;
                basePieces.GetandSetStateRotaion(basePieces.MStateRotation);
                return true;

            default:
                return false;
        }
    }
    public void MoveBot(BasePieces basePieces, Cell targetCell, bool isTeamup, bool isKill)
    {
        SoundController.Instance.PlayVfxClip(2);
        if (basePieces.mCurrentCell != null)
            basePieces.mCurrentCell.mCurrentPiece = null;
        //switch cells
        basePieces.mCurrentCell = targetCell;
        basePieces.mCurrentCell.mCurrentPiece = basePieces;
        basePieces.transform.SetParent(PiecesManager.transform);
        basePieces.transform.DOMove(targetCell.transform.position, 0.3f);

        basePieces.mIsPlace = true;
        basePieces.ClearCells();
    }//SUA LAI THANH MOVESINGLEBOT USING MOVE BOT
}
