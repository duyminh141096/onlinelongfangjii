/*Project Name : LK
* Script Name: FakePiece.cs
* Script Author: MinhLe 
* Created On: 1/4/2021 1:55:54 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class FakePiece : MonoBehaviour
{
    public Data data;
    private Color greenColor = new Color32(58, 241, 56, 255);
    private Color redColor=  new Color32(255, 176, 93, 255);
    public void DestroyYourSelf()
    {
        Destroy(this.gameObject);
    }

    public FakePiece StartFunc()
    {
        transform.localScale = Vector3.zero;
        Anim();
        return this;
    }
    public FakePiece AddColor(bool teamColor)
    {
        SetColor(teamColor ? redColor : greenColor);
        return this;
    }
    public FakePiece ChangeRotateAndShow(int state,bool teamColor)
    {
        SetRotate(state, teamColor);
        return this;
    }
    private void Anim()
    {
        transform.DOScale(1f, 0.3f);
    }
    private void SetColor(Color color)
    {
        data.Image.color = color;
    }
    private void SetRotate(int state, bool teamColor)
    {
        switch (state)
        {
            case 1://up
                this.transform.localEulerAngles = teamColor ? new Vector3(0, 0, 180)  : new Vector3(0, 0, 0);
                break;
            case 2:
                this.transform.localEulerAngles = teamColor ? new Vector3(0, 0, 90) :  new Vector3(0, 0, -90);
                break;
            case 3:
                this.transform.localEulerAngles = teamColor ? new Vector3(0, 0, 0) : new Vector3(0, 0, 180);
                break;
            case 4:
                this.transform.localEulerAngles = teamColor ? new Vector3(0, 0, -90) : new Vector3(0, 0, 90);
                break;
        }
      
    }
    [System.Serializable]
    public class Data
    {
        public Image Image;
        public Text intState;
    }
}
