/*Project Name : LK
* Script Name: PieceDetails.cs
* Script Author: MinhLe 
* Created On: 1/4/2021 11:24:02 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PieceDetails : MonoBehaviour
{
    public DetailsPieceUI ui;
    public DetailsData data;

    public static PieceDetails Instance;
    private void Awake()
    {
        Instance = this;
    }

    public void ReceiveRequest(GameObject piece, bool active)
    {
        BasePieces basePiece = piece.GetComponent<BasePieces>();
        TeamUpList teamUpList = piece.GetComponent<TeamUpList>();
        ClearData(basePiece.mColor);
        if(!active) return;
        if (PlayerPrefs.GetInt("InfoPiece") == 0) return;
            CreatePiece(basePiece.mColor, teamUpList.Clone());
    }
    public float offset;
    void CreatePiece(bool teamColor, List<PieceInfo> pieces)
    {
        for (int i = 0; i < pieces.Count; i++)
        {
            int index = pieces.Count - i - 1 ;
            GameObject parent = ui.pieceDetails.Find(x => x.nameObject == pieces[i].nPieceName).gameObject;

            FakePiece fakePiece = Instantiate(parent, teamColor ? ui.rightPool : ui.leftPool)
             .GetComponent<FakePiece>()
             .AddColor(teamColor)
             .ChangeRotateAndShow(pieces[i].mPieceRotation, teamColor)
             .StartFunc();

            fakePiece.transform.localPosition = Vector3.up * index * offset;
        }
    }
    void ClearData(bool teamColor)
    {
        DestroyAllchild(teamColor ? ui.rightPool.gameObject : ui.leftPool.gameObject);
    }
    void DestroyAllchild(GameObject parent)
    {
        var array = parent.GetComponentsInChildren<FakePiece>();
        if (array.Length == 0) return;
        foreach (var item in array)
        {
            item.DestroyYourSelf();
        }
    }

}



[System.Serializable]
public class PieceDetail
{
    public string nameObject;
    public GameObject gameObject;
}
[System.Serializable]
public class DetailsPieceUI
{

    public List<PieceDetail> pieceDetails = new List<PieceDetail>();

    public Transform rightPool;
    public Transform leftPool;
}
[System.Serializable]
public class DetailsData
{

}
