/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: LanguageController.cs
* Script Author: MinhLe 
* Created On: 10/19/2020 9:20:43 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class language { 
    public string content;
    public int sizeFont;
}
public class LanguageController : MonoBehaviour
{

    public delegate void ChangeLanguage();
    public static ChangeLanguage OnChangeLanguage;

    public Dropdown LanguageDropdown;

    public Toggle showinfopiece;

    private void Start()
    {
        OnChangeLanguage?.Invoke();
        if (LanguageDropdown)
        {
            LanguageDropdown.value = PlayerPrefs.GetInt("Language");
            showinfopiece.isOn = PlayerPrefs.GetInt("InfoPiece") == 1 ? true : false;
            Init();
        }
    }

    public void Init()
    {
        LanguageDropdown.onValueChanged.AddListener(x => {
            
            if(x == 0) //English
            {
                PlayerPrefs.SetInt("Language", 0);
                OnChangeLanguage?.Invoke();
            }
            else
            {
                PlayerPrefs.SetInt("Language", 1);
                OnChangeLanguage?.Invoke();
            }
        });
        showinfopiece.onValueChanged.AddListener(x => {
            PlayerPrefs.SetInt("InfoPiece", x ? 1 : 0);
        });
    }
}
