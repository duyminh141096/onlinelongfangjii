/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: ContentText.cs
* Script Author: MinhLe 
* Created On: 10/19/2020 9:18:26 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ContentText : MonoBehaviour
{
    public Text contentInput;
    public language Vietnamese;
    public language English;
    private void Awake()
    {
        LanguageController.OnChangeLanguage += UpdateLanguage;
        contentInput = this.GetComponent<Text>();
    }

    private void OnDestroy()
    {
        LanguageController.OnChangeLanguage -= UpdateLanguage;
    }
    private void Start()
    {
        UpdateLanguage();
    }

    public void UpdateLanguage()
    {
        int language = PlayerPrefs.GetInt("Language");
        if (language == 0)//english
        {
            string text =  English.content.Replace("***", "\n"); 
            contentInput.text = text;
            contentInput.fontSize = English.sizeFont;
        }
        else if (language == 1)//vietnamese
        {
            string text = Vietnamese.content.Replace("***", "\n");
            contentInput.text = text;
            contentInput.fontSize = Vietnamese.sizeFont;
        }
    }
}
